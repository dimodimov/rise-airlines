# RISE-Airlines


RISE-Airlines is a console application to read, validate, and work with sequences of Airports, Airlines, Flights, Aircrafts, Reservations and Routes.

## Usage

Upon running the application, type 'help' to get a list of all commands with instructions how to use them.

## Testing

This project uses xUnit as the testing framework for unit tests. Tests follow the naming convention MethodName_StateUnderTest_ExpectedBehavior.

For more information about xUnit. To run the tests use the Test Explorer, or navigate to the root directory of the project in your terminal and run the following command:

```bash
dotnet test
```

Or for full details and test coverage:

```bash
dotnet test --no-restore --verbosity normal --logger "trx;LogFileName=test_results.xml" /p:CollectCoverage=true /p:CoverletOutputFormat=cobertura /p:CoverletOutput=./TestResults/ /p:Threshold=50 /p:ThresholdType=line /p:ThresholdStat=total
