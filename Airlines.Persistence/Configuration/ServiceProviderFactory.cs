﻿using Airlines.Persistence.DB;
using Airlines.Persistence.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;


namespace Airlines.Persistence.Configuration;
public class ServiceProviderFactory
{
    private static IServiceProvider? _serviceProvider;

    public static IServiceProvider GetServiceProvider()
    {
        if (_serviceProvider == null)
        {
            // Create service collection
            var services = new ServiceCollection();

            // Add services
            _ = services.AddScoped<IFlightRepository, FlightRepository>();
            _ = services.AddScoped<IAirlineRepository, AirlineRepository>();
            _ = services.AddScoped<IAirportRepository, AirportRepository>();

            _ = services.AddDbContext<RiseContext>(options =>
                options.UseSqlServer(ConfigurationManager.GetConnectionString("Local")));

            // Build service provider
            _serviceProvider = services.BuildServiceProvider();
        }

        return _serviceProvider;
    }
}
