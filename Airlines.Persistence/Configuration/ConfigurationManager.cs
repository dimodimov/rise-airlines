﻿using Airlines.Persistence.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Airlines.Persistence.Configuration;
public class ConfigurationManager
{
    private static readonly IConfigurationRoot _configuration;

    static ConfigurationManager()
    {
        // Build configuration once
        var currentDirectory = Directory.GetCurrentDirectory();
        var projectDirectory = Path.Combine(currentDirectory, @"..\..\..\..\Airlines.Persistence");

        _configuration = new ConfigurationBuilder()
                .SetBasePath(projectDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
    }

    public static string? GetConnectionString(string name)
        => _configuration.GetConnectionString(name);

    public static IFlightRepository GetFlightRepository()
    {
        var serviceProvider = ServiceProviderFactory.GetServiceProvider();

        var repository = serviceProvider.GetRequiredService<IFlightRepository>();

        return repository;
    }

    public static IAirportRepository GetAirportRepository()
    {
        var serviceProvider = ServiceProviderFactory.GetServiceProvider();

        var repository = serviceProvider.GetRequiredService<IAirportRepository>();

        return repository;
    }

    public static IAirlineRepository GetAirlineRepository()
    {
        var serviceProvider = ServiceProviderFactory.GetServiceProvider();

        var repository = serviceProvider.GetRequiredService<IAirlineRepository>();

        return repository;
    }
}
