﻿using Airlines.Persistence.Entities;

namespace Airlines.Persistence.Repository;
public interface IAirlineRepository
{
    Task<List<Airline>> GetAirlinesAsync();
    Task<int> GetAirlinesCountAsync();
    Task<List<Airline>> GetAirlinesByPageAsync(int itemsToSkip, int pageSize);
    Task<(int itemCount, List<Airline> airlines)> SearchAirlinesByPageAsync(string searchString, int itemsToSkip, int pageSize);
    Task<List<Airline>> GetAirlinesByFilterAsync(string filter, string value);
    Task<Airline?> GetAirlineByIdAsync(int id);
    Task<Airline?> GetAirlineByNameAsync(string name);
    Task<Airline?> AddAirlineAsync(Airline airline);
    Task<bool> UpdateAirlineFleetSizeAsync(int id, int fleetSize);
    Task<Airline?> UpdateAirlineAsync(Airline airline);
    Task<Airline?> DeleteAirlineAsync(int id);
}
