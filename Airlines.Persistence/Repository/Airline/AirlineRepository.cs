﻿using Airlines.Persistence.Entities;
using Airlines.Persistence.DB;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.Repository;
public class AirlineRepository(RiseContext context) : IAirlineRepository
{
    private readonly RiseContext _context = context;

    public async Task<List<Airline>> GetAirlinesAsync() => await _context.Airlines.ToListAsync();

    public async Task<int> GetAirlinesCountAsync() => await _context.Airlines.CountAsync();

    public async Task<List<Airline>> GetAirlinesByPageAsync(int itemsToSkip, int pageSize)
        => await _context.Airlines.Skip(itemsToSkip).Take(pageSize).ToListAsync();

    public async Task<(int itemCount, List<Airline> airlines)> SearchAirlinesByPageAsync(string searchString, int itemsToSkip, int pageSize)
    {
        var query = _context.Airlines.Where(airline =>
            airline.Name.Contains(searchString));

        var totalItems = await query.CountAsync();
        var result = await query.Skip(itemsToSkip).Take(pageSize).ToListAsync();

        return (totalItems, result);
    }

    public async Task<List<Airline>> GetAirlinesByFilterAsync(string filter, string value)
        => await _context.Airlines.Where(airline => EF.Property<string>(airline, filter) == value).ToListAsync();

    public async Task<Airline?> GetAirlineByIdAsync(int id)
        => await _context.Airlines.FirstOrDefaultAsync(airline => airline.Id == id);

    public async Task<Airline?> GetAirlineByNameAsync(string name)
        => await _context.Airlines.FirstOrDefaultAsync(airline => airline.Name == name);

    public async Task<Airline?> AddAirlineAsync(Airline airline)
    {
        _ = await _context.AddAsync(airline);
        var result = await _context.SaveChangesAsync();

        if (result > 0)
        {
            return airline;
        }

        return null;
    }

    public async Task<bool> UpdateAirlineFleetSizeAsync(int id, int fleetSize)
    {
        var airline = await _context.Airlines.FirstOrDefaultAsync(airline => airline.Id == id);

        if (airline != null)
        {
            airline.FleetSize = fleetSize;
        }

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<Airline?> UpdateAirlineAsync(Airline airline)
    {
        _ = _context.Update(airline);
        var result = await _context.SaveChangesAsync();

        if (result > 0)
        {
            return airline;
        }

        return null;
    }

    public async Task<Airline?> DeleteAirlineAsync(int id)
    {
        var airline = await _context.Airlines.FirstOrDefaultAsync(airline => airline.Id == id);

        if (airline != null)
        {
            _ = _context.Remove(airline);
        }

        var result = await _context.SaveChangesAsync();

        if (result > 0)
        {
            return airline;
        }

        return null;
    }
}
