﻿using Airlines.Persistence.Entities;

namespace Airlines.Persistence.Repository;
public interface IAirportRepository
{
    Task<List<Airport>> GetAirportsAsync();
    Task<int> GetAirportsCountAsync();
    Task<List<Airport>> GetAirportsByPageAsync(int itemsToSkip, int pageSize);
    Task<(int itemCount, List<Airport> airports)> SearchAirportsByPageAsync(string searchString, int itemsToSkip, int pageSize);
    Task<List<Airport>> GetAirportsByFilterAsync(string filter, string value);
    Task<Airport?> GetAirportByCodeAsync(string code);
    Task<Airport?> GetAirportByNameAsync(string name);
    Task<Airport?> AddAirportAsync(Airport airport);
    Task<bool> UpdateAirportRunwaysCountAsync(string code, int runways);
    Task<Airport?> UpdateAirportAsync(Airport airport);
    Task<Airport?> DeleteAirportAsync(string code);
}
