﻿using Airlines.Persistence.Entities;
using Airlines.Persistence.DB;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.Repository;
public class AirportRepository(RiseContext context) : IAirportRepository
{
    private readonly RiseContext _context = context;

    public async Task<List<Airport>> GetAirportsAsync() => await _context.Airports.ToListAsync();

    public async Task<int> GetAirportsCountAsync() => await _context.Airports.CountAsync();

    public async Task<List<Airport>> GetAirportsByPageAsync(int itemsToSkip, int pageSize)
        => await _context.Airports.Skip(itemsToSkip).Take(pageSize).ToListAsync();

    public async Task<(int itemCount, List<Airport> airports)> SearchAirportsByPageAsync(string searchString, int itemsToSkip, int pageSize)
    {
        var query = _context.Airports.Where(airport =>
            airport.Name.Contains(searchString) || airport.Code.Contains(searchString));

        var totalItems = await query.CountAsync();
        var result = await query.Skip(itemsToSkip).Take(pageSize).ToListAsync();

        return (totalItems, result);
    }

    public async Task<List<Airport>> GetAirportsByFilterAsync(string filter, string value)
        => await _context.Airports.Where(airport => EF.Property<string>(airport, filter) == value).ToListAsync();

    public async Task<Airport?> GetAirportByCodeAsync(string code)
        => await _context.Airports.FirstOrDefaultAsync(airport => airport.Code == code);

    public async Task<Airport?> GetAirportByNameAsync(string name)
        => await _context.Airports.FirstOrDefaultAsync(airport => airport.Name == name);

    public async Task<Airport?> AddAirportAsync(Airport airport)
    {
        _ = await _context.AddAsync(airport);
        var result = await _context.SaveChangesAsync();

        if (result > 0)
        {
            return airport;
        }

        return null;
    }

    public async Task<bool> UpdateAirportRunwaysCountAsync(string code, int runways)
    {
        var airline = await _context.Airports.FirstOrDefaultAsync(airport => airport.Code == code);

        if (airline != null)
        {
            airline.RunwaysCount = runways;
        }

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<Airport?> UpdateAirportAsync(Airport airport)
    {
        _ = _context.Update(airport);
        var result = await _context.SaveChangesAsync();

        if (result > 0)
        {
            return airport;
        }

        return null;
    }

    public async Task<Airport?> DeleteAirportAsync(string code)
    {
        var airport = await _context.Airports.FirstOrDefaultAsync(airport => airport.Code == code);

        if (airport != null)
        {
            _ = _context.Remove(airport);
        }

        var result = await _context.SaveChangesAsync();

        if (result > 0)
        {
            return airport;
        }

        return null;
    }
}
