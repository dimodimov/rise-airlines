﻿using Airlines.Persistence.Entities;
using Airlines.Persistence.DB;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.Repository;
public class FlightRepository(RiseContext context) : IFlightRepository
{
    private readonly RiseContext _context = context;

    public async Task<List<Flight>> GetFlightsAsync() => await _context.Flights.ToListAsync();

    public async Task<int> GetFlightCountAsync() => await _context.Flights.CountAsync();

    public async Task<(int itemCount, List<Flight> flights)> SearchAndFiterFlightsAsync(string searchString, DateTime today, DateTime endDate, int itemsToSkip, int pageSize)
    {
        var query = _context.Flights.Where(flight =>
            (flight.FlightNumber.Contains(searchString)
            || flight.FromAirport.Contains(searchString)
            || flight.ToAirport.Contains(searchString))
            && flight.DepartureDateTime.Date >= today.Date
            && flight.DepartureDateTime.Date <= endDate.Date);

        var totalItems = await query.CountAsync();
        var result = await query.Skip(itemsToSkip).Take(pageSize).ToListAsync();

        return (totalItems, result);
    }

    public async Task<(int itemCount, List<Flight> flights)> SearchFlightsAsync(string searchString, int itemsToSkip, int pageSize)
    {
        var query = _context.Flights.Where(flight =>
            flight.FlightNumber.Contains(searchString)
            || flight.FromAirport.Contains(searchString)
            || flight.ToAirport.Contains(searchString));

        var totalItems = await query.CountAsync();
        var result = await query.Skip(itemsToSkip).Take(pageSize).ToListAsync();

        return (totalItems, result);
    }

    public async Task<(int itemCount, List<Flight> flights)> GetFlightsByDateRangeAsync(DateTime today, DateTime endDate, int itemsToSkip, int pageSize)
    {
        var query = _context.Flights.Where(f => f.DepartureDateTime.Date >= today && f.DepartureDateTime.Date <= endDate);
        var totalItems = await query.CountAsync();

        var result = await query.Skip(itemsToSkip).Take(pageSize).ToListAsync();

        return (totalItems, result);
    }

    public async Task<List<Flight>> GetFlightsByPageAsync(int pagesToSkip, int totalItems)
        => await _context.Flights.Skip(pagesToSkip).Take(totalItems).ToListAsync();

    public async Task<List<Flight>> GetFlightsByFilterAsync(string filter, string value)
        => await _context.Flights.Where(flight => EF.Property<string>(flight, filter) == value).ToListAsync();

    public async Task<List<Flight>> GetFlightsByFilterAsync(string filter, int value)
        => await _context.Flights.Where(flight => EF.Property<int>(flight, filter) == value).ToListAsync();

    public async Task<List<Flight>> GetFlightsByFilterAsync(string filter, DateTime value)
        => await _context.Flights.Where(flight => EF.Property<DateTime>(flight, filter) == value).ToListAsync();

    public async Task<Flight?> GetFlightByIdAsync(int id)
        => await _context.Flights.FirstOrDefaultAsync(flight => flight.Id == id);

    public async Task<Flight?> GetFlightByNumberAsync(string flightNumber)
        => await _context.Flights.FirstOrDefaultAsync(flight => flight.FlightNumber == flightNumber);

    public async Task<Flight?> AddFlightAsync(Flight flight)
    {
        _ = await _context.AddAsync(flight);
        var result = await _context.SaveChangesAsync();

        if (result > 0)
        {
            return flight;
        }

        return null;
    }

    public async Task<Flight?> UpdateFlightAsync(Flight flight)
    {
        _ = _context.Update(flight);
        var result = await _context.SaveChangesAsync();

        if (result > 0)
        {
            return flight;
        }

        return null;
    }

    public async Task<bool> UpdateFlightDepartureAndArrivalTimeAsync(string flightNumber, DateTime departureTime, DateTime arrivalTime)
    {
        var flight = await _context.Flights.FirstOrDefaultAsync(flight => flight.FlightNumber == flightNumber);

        if (flight != null)
        {
            flight.DepartureDateTime = departureTime;
            flight.ArrivalDateTime = arrivalTime;
        }

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<Flight?> DeleteFlightAsync(int id)
    {
        var flight = await _context.Flights.FirstOrDefaultAsync(flight => flight.Id == id);

        if (flight != null)
        {
            _ = _context.Remove(flight);
        }

        var result = await _context.SaveChangesAsync();

        if (result > 0)
        {
            return flight;
        }

        return null;
    }
}
