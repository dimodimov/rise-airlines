﻿using Airlines.Persistence.Entities;

namespace Airlines.Persistence.Repository;
public interface IFlightRepository
{
    Task<List<Flight>> GetFlightsAsync();
    Task<int> GetFlightCountAsync();
    Task<(int itemCount, List<Flight> flights)> SearchFlightsAsync(string searchString, int itemsToSkip, int pageSize);
    Task<(int itemCount, List<Flight> flights)> GetFlightsByDateRangeAsync(DateTime today, DateTime endDate, int itemsToSkip, int pageSize);
    Task<List<Flight>> GetFlightsByPageAsync(int pagesToSkip, int totalItems);
    Task<(int itemCount, List<Flight> flights)> SearchAndFiterFlightsAsync(string searchString, DateTime today, DateTime endDate, int itemsToSkip, int pageSize);
    Task<List<Flight>> GetFlightsByFilterAsync(string filter, string value);
    Task<List<Flight>> GetFlightsByFilterAsync(string filter, int value);
    Task<List<Flight>> GetFlightsByFilterAsync(string filter, DateTime value);
    Task<Flight?> GetFlightByIdAsync(int id);
    Task<Flight?> GetFlightByNumberAsync(string flightNumber);
    Task<Flight?> AddFlightAsync(Flight flight);
    Task<Flight?> UpdateFlightAsync(Flight flight);
    Task<bool> UpdateFlightDepartureAndArrivalTimeAsync(string flightNumber, DateTime departureTime, DateTime arrivalTime);
    Task<Flight?> DeleteFlightAsync(int id);
}
