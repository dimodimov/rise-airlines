ALTER TABLE dbo.Flights
ADD CONSTRAINT CHK_Flights_DepartureTime_Not_Past
CHECK (DepartureDateTime >= GETDATE());

ALTER TABLE dbo.Flights
ADD CONSTRAINT CHK_Flights_ArrivalTime_Not_Past
CHECK (ArrivalDateTime >= GETDATE());

ALTER TABLE dbo.Flights
ADD CONSTRAINT CHK_Flights_ArrivalTime_After_Departure
CHECK (ArrivalDateTime > DepartureDateTime);