SELECT TOP 1 WITH TIES
	FlightNumber, 
	FromAirport,
	ToAirport,
	DATEDIFF(MINUTE, Flights.DepartureDateTime, Flights.ArrivalDateTime) AS FlightDurationMinutes
FROM dbo.Flights    
ORDER BY FlightDurationMinutes DESC;
