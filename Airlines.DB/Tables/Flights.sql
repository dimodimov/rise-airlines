﻿CREATE TABLE Flights (
    ID INT PRIMARY KEY IDENTITY(1,1),
    FlightNumber VARCHAR(6) NOT NULL,
    FromAirport VARCHAR(3) NOT NULL,
    ToAirport VARCHAR(3) NOT NULL,
    DepartureDateTime DATETIME NOT NULL,
    ArrivalDateTime DATETIME NOT NULL
);