﻿CREATE TABLE Airlines (
    Id INT PRIMARY KEY IDENTITY(1,1),
    Name VARCHAR(255) NOT NULL,
    Founded DATE NOT NULL,
    FleetSize INT NOT NULL,
    Description VARCHAR(255)
);