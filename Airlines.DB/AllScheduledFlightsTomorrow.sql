SELECT 	f.id AS FlightID,
		f.FlightNumber,
		f.FromAirport,
		f.ToAirport,
		da.Name AS DepartureAirportName,
		da.Country AS DepartureAirportCountry,
		da.City AS DepartureAirportCity,
		da.Code AS DepartureAirportCode,
		da.RunwaysCount AS DepartureAirportRunwaysCount,
		da.Founded	AS DepartureAirportFounded,
		aa.Name AS ArrivalAirportName,
		aa.Country AS ArrivalAirportCountry,
		aa.City AS ArrivalAirportCity,
		aa.Code AS ArrivalAirportCode,
		aa.RunwaysCount AS ArrivalAirportRunwaysCount,
		aa.Founded	AS ArrivalAirportFounded
FROM dbo.Flights AS F
	JOIN dbo.Airports AS da 
		ON DA.Code = f.FromAirport
	JOIN dbo.Airports AS aa 
		ON AA.Code = f.ToAirport
WHERE CONVERT(DATE, DepartureDateTime) = DATEADD(DAY, 1, CONVERT(DATE, GETDATE()));