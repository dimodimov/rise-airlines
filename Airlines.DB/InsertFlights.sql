﻿INSERT INTO Flights (FlightNumber, FromAirport, ToAirport, DepartureDateTime, ArrivalDateTime)
VALUES ('FL123', 'JFK', 'LAX', '2024-04-12 12:00', '2024-04-13 15:00'),
       ('FL456', 'LAX', 'ORD', '2024-04-13 15:30', '2024-04-14 18:30');
