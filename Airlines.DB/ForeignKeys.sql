ALTER TABLE dbo.Flights
ADD CONSTRAINT FK_Flights_FromAirport
FOREIGN KEY (FromAirport) REFERENCES dbo.Airports(Code);

ALTER TABLE dbo.Flights
ADD CONSTRAINT FK_Flights_ToAirport
FOREIGN KEY (ToAirport) REFERENCES dbo.Airports(Code);