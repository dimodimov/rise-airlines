CREATE NONCLUSTERED INDEX IX_Airports_Code  
ON dbo.Airports(Code)

CREATE NONCLUSTERED INDEX IX_Airports_Name  
ON dbo.Airports(Name)

CREATE NONCLUSTERED INDEX IX_Airlines_Name 
ON dbo.Airlines(Name);

CREATE NONCLUSTERED INDEX IX_Flights_FromAirport 
ON dbo.Flights(FromAirport);

CREATE NONCLUSTERED INDEX IX_Flights_ToAirport 
ON dbo.Flights(ToAirport);