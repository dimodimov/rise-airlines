﻿using Airlines.Business.DTOs;
using Airlines.Business.Services.FlightService;
using Microsoft.AspNetCore.Mvc;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Airlines.API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class FlightsController(IFlightService flightService) : Controller
{
    private readonly IFlightService _flightService = flightService;

    [HttpGet]
    public async Task<ActionResult<List<FlightDTO>>> GetAll()
    {
        var (flights, error) = await _flightService.GetFlightsAsync();

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (flights?.Count == 0)
        {
            return NoContent();
        }

        return Ok(flights);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<FlightDTO>> GetOne(int id)
    {
        var (flight, error) = await _flightService.GetFlightByIdAsync(id);

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (flight == null)
        {
            return NotFound();
        }

        return Ok(flight);
    }

    [HttpPost]
    public async Task<ActionResult<FlightDTO>> Create([FromBody] FlightDTO flightDTO)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var (result, error) = await _flightService.AddFlightAsync(flightDTO);

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (result == null)
        {
            return BadRequest();
        }

        return CreatedAtAction("Create", result);
    }

    [HttpPut]
    public async Task<ActionResult<FlightDTO>> Update([FromBody] FlightDTO flightDTO)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var (result, error) = await _flightService.UpdateFlightAsync(flightDTO);

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (result == null)
        {
            return BadRequest();
        }

        return Ok(result);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<FlightDTO>> Delete(int id)
    {
        var (result, error) = await _flightService.DeleteFlightAsync(id);

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (result == null)
        {
            return NotFound();
        }

        return Ok(result);
    }
}
