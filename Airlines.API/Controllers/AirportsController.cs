﻿using Airlines.Business.DTOs;
using Airlines.Business.Services.AirportService;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AirportsController(IAirportService airportService) : Controller
{
    private readonly IAirportService _airportService = airportService;

    [HttpGet]
    public async Task<ActionResult<List<AirportDTO>>> GetAll()
    {
        var (airports, error) = await _airportService.GetAirportsAsync();

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (airports?.Count == 0)
        {
            return NoContent();
        }

        return Ok(airports);
    }

    [HttpGet("{code}")]
    public async Task<ActionResult<AirportDTO>> GetOne(string code)
    {
        var (airport, error) = await _airportService.GetAirportByCodeAsync(code);

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (airport == null)
        {
            return NotFound();
        }

        return Ok(airport);
    }

    [HttpPost]
    public async Task<ActionResult<AirportDTO>> Create([FromBody] AirportDTO airportDTO)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var (result, error) = await _airportService.AddAirportAsync(airportDTO);

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (result == null)
        {
            return BadRequest();
        }

        return CreatedAtAction("Create", result);
    }

    [HttpPut]
    public async Task<ActionResult<AirportDTO>> Update([FromBody] AirportDTO airportDTO)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var (result, error) = await _airportService.UpdateAirportAsync(airportDTO);

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (result == null)
        {
            return BadRequest();
        }

        return Ok(result);
    }

    [HttpDelete("{code}")]
    public async Task<ActionResult<AirportDTO>> Delete(string code)
    {
        var (result, error) = await _airportService.DeleteAirportAsync(code);

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (result == null)
        {
            return NotFound();
        }

        return Ok(result);
    }
}
