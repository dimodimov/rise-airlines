﻿using Airlines.Business.DTOs;
using Airlines.Business.Services.AirlineService;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AirlinesController(IAirlineService airlineService) : ControllerBase
{
    private readonly IAirlineService _airlineService = airlineService;

    [HttpGet]
    public async Task<ActionResult<List<AirlineDTO>>> GetAll()
    {
        var (airlines, error) = await _airlineService.GetAirlinesAsync();

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (airlines?.Count == 0)
        {
            return NoContent();
        }

        return Ok(airlines);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<AirlineDTO>> GetOne(int id)
    {
        var (airline, error) = await _airlineService.GetAirlineByIdAsync(id);

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (airline == null)
        {
            return NotFound();
        }

        return Ok(airline);
    }

    [HttpPost]
    public async Task<ActionResult<AirlineDTO>> Create([FromBody] AirlineDTO airlineDTO)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var (result, error) = await _airlineService.AddAirlineAsync(airlineDTO);

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (result == null)
        {
            return BadRequest();
        }

        return CreatedAtAction("Create", result);
    }

    [HttpPut]
    public async Task<ActionResult<AirlineDTO>> Update([FromBody] AirlineDTO airlineDTO)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var (result, error) = await _airlineService.UpdateAirlineAsync(airlineDTO);

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (result == null)
        {
            return BadRequest();
        }

        return Ok(result);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<AirlineDTO>> Delete(int id)
    {
        var (result, error) = await _airlineService.DeleteAirlineAsync(id);

        if (error)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }

        if (result == null)
        {
            return NotFound();
        }

        return Ok(result);
    }
}
