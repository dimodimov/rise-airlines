using Microsoft.EntityFrameworkCore;
using Airlines.Persistence.DB;
using Airlines.Persistence.Repository;
using Airlines.Business.Services.AirlineService;
using Airlines.Business.Services.AirportService;
using Airlines.Business.Services.FlightService;

namespace Airlines.API;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.

        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        var connectionString = builder.Configuration.GetConnectionString("Local");
        builder.Services.AddDbContext<RiseContext>(options => options.UseSqlServer(connectionString));

        builder.Services.AddScoped<IAirlineRepository, AirlineRepository>();
        builder.Services.AddScoped<IAirlineService, AirlineService>();

        builder.Services.AddScoped<IAirportRepository, AirportRepository>();
        builder.Services.AddScoped<IAirportService, AirportService>();

        builder.Services.AddScoped<IFlightRepository, FlightRepository>();
        builder.Services.AddScoped<IFlightService, FlightService>();

        const string AllowAnyOrigin = "_allowAnyOriginPolicy";

        builder.Services.AddCors(options =>
        {
            options.AddPolicy(name: AllowAnyOrigin,
                              policy =>
                              {
                                  policy.AllowAnyHeader()
                                  .AllowAnyOrigin()
                                  .AllowAnyMethod();
                              });
        });

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        app.UseAuthorization();


        app.MapControllers();

        app.UseCors(AllowAnyOrigin);

        app.Run();
    }
}
