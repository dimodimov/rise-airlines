﻿//using Airlines.Business.DTOs;
//using Airlines.Business.Services.AirlineService;
//using Airlines.Business.Services.AirportService;
//using Airlines.Business.Services.FlightService;
//using Airlines.Persistence.Configuration;

//namespace Airlines.Console;
//public static class DemoServices
//{
//    public static async Task DemoFlightServices()
//    {
//        var flightService = new FlightService(ConfigurationManager.GetFlightRepository());
//        var flights = await flightService.GetFlightsAsync();

//        foreach (var flight in flights)
//        {
//            System.Console.WriteLine($"Flight {flight.FlightNumber} from {flight.FromAirport} to {flight.ToAirport} departs at {flight.DepartureDateTime} and arrives at {flight.ArrivalDateTime}");
//        }

//        var flightByFilter = await flightService.GetFlightsByFilterAsync("Id", 13);

//        foreach (var flight in flightByFilter)
//        {
//            System.Console.WriteLine($"Flight {flight.FlightNumber} from {flight.FromAirport} to {flight.ToAirport} departs at {flight.DepartureDateTime} and arrives at {flight.ArrivalDateTime}");
//        }

//        var flightByFilterTwo = await flightService.GetFlightsByFilterAsync("FromAirport", "JFK");

//        foreach (var flight in flightByFilterTwo)
//        {
//            System.Console.WriteLine($"Flight {flight.FlightNumber} from {flight.FromAirport} to {flight.ToAirport} departs at {flight.DepartureDateTime} and arrives at {flight.ArrivalDateTime}");
//        }

//        var flightByFilterThree = await flightService.GetFlightsByFilterAsync("DepartureDateTime", DateTime.Parse("4/15/2024 2:00:00 PM"));

//        foreach (var flight in flightByFilterThree)
//        {
//            System.Console.WriteLine($"Flight {flight.FlightNumber} from {flight.FromAirport} to {flight.ToAirport} departs at {flight.DepartureDateTime} and arrives at {flight.ArrivalDateTime}");
//        }

//        var flightById = await flightService.GetFlightByIdAsync(13);
//        System.Console.WriteLine($"Flight {flightById?.FlightNumber} from {flightById?.FromAirport} to {flightById?.ToAirport} departs at {flightById?.DepartureDateTime} and arrives at {flightById?.ArrivalDateTime}");

//        var flightByName = await flightService.GetFlightByNumberAsync("FL123");
//        System.Console.WriteLine($"Flight {flightByName?.FlightNumber} from {flightByName?.FromAirport} to {flightByName?.ToAirport} departs at {flightByName?.DepartureDateTime} and arrives at {flightByName?.ArrivalDateTime}");

//        var flightToAdd = new FlightDTO
//        {
//            FlightNumber = "FL666",
//            FromAirport = "JFK",
//            ToAirport = "LAX",
//            DepartureDateTime = DateTime.Parse("5/15/2024 2:00:00 PM"),
//            ArrivalDateTime = DateTime.Parse("5/16/2024 4:00:00 PM")
//        };

//        var addedFlight = await flightService.AddFlightAsync(flightToAdd);

//        if (addedFlight != null)
//        {
//            System.Console.WriteLine("Flight added");
//            var newFlight = await flightService.GetFlightByNumberAsync("FL666");
//            System.Console.WriteLine($"Flight {newFlight?.FlightNumber} from {newFlight?.FromAirport} to {newFlight?.ToAirport} departs at {newFlight?.DepartureDateTime} and arrives at {newFlight?.ArrivalDateTime}");
//        }

//        var flightUpdate = await flightService.UpdateFlightDepartureAndArrivalTimeAsync("FL666", DateTime.Parse("5/15/2024 3:00:00 PM"), DateTime.Parse("5/16/2024 5:00:00 PM"));

//        if (flightUpdate)
//        {
//            System.Console.WriteLine("Flight updated");
//            var newFlight = await flightService.GetFlightByNumberAsync("FL666");
//            System.Console.WriteLine($"Flight {newFlight?.FlightNumber} from {newFlight?.FromAirport} to {newFlight?.ToAirport} departs at {newFlight?.DepartureDateTime} and arrives at {newFlight?.ArrivalDateTime}");
//        }

//        var deletedFlight = await flightService.DeleteFlightAsync(42);

//        if (deletedFlight != null)
//        {
//            System.Console.WriteLine("Flight deleted");
//        }
//    }

//    public static async Task DemoAirportServices()
//    {
//        var airportService = new AirportService(ConfigurationManager.GetAirportRepository());

//        var airports = await airportService.GetAirportsAsync();

//        foreach (var airport in airports)
//        {
//            System.Console.WriteLine($"Airport {airport.Name} with {airport.Code} found.");
//        }

//        var airportsByFilter = await airportService.GetAirportsByFilterAsync("Code", "JFK");

//        foreach (var airport in airportsByFilter)
//        {
//            System.Console.WriteLine($"Airport {airport.Name} with {airport.Code} found.");
//        }

//        var airportByCode = await airportService.GetAirportByCodeAsync("LAX");
//        System.Console.WriteLine($"Airport {airportByCode?.Name} with {airportByCode?.Code} found.");

//        var airportToAdd = new AirportDTO
//        {
//            Name = "SOF",
//            Country = "Bulgaria",
//            City = "Sofia",
//            Code = "SOF",
//            RunwaysCount = 2,
//            Founded = DateOnly.MinValue
//        };

//        var addedAirport = await airportService.AddAirportAsync(airportToAdd);

//        if (addedAirport != null)
//        {
//            System.Console.WriteLine("Airport added");
//            var newAirport = await airportService.GetAirportByCodeAsync("SOF");
//            System.Console.WriteLine($"Airport {newAirport?.Name} with {newAirport?.Code} found.");
//        }

//        var airportUpdate = await airportService.UpdateAirportRunwaysCountAsync("SOF", 22);

//        if (airportUpdate)
//        {
//            System.Console.WriteLine("Airport updated");
//            var updatedAirport = await airportService.GetAirportByCodeAsync("SOF");
//            System.Console.WriteLine($"Airport {updatedAirport?.Name} with {updatedAirport?.Code} found.");
//        }

//        var deletedAirport = await airportService.DeleteAirportAsync("SOF");

//        if (deletedAirport != null)
//        {
//            System.Console.WriteLine("Airport deleted");
//        }
//    }

//    public static async Task DemoAirlineServices()
//    {
//        var airlineService = new AirlineService(ConfigurationManager.GetAirlineRepository());

//        var airlines = await airlineService.GetAirlinesAsync();

//        foreach (var airline in airlines)
//        {
//            System.Console.WriteLine($"Airline {airline.Name} with fleet size {airline.FleetSize} found.");
//        }

//        var airlinesByFilter = await airlineService.GetAirlinesByFilterAsync("Name", "United Airlines");

//        foreach (var airline in airlinesByFilter)
//        {
//            System.Console.WriteLine($"Airline {airline.Name} with fleet size {airline.FleetSize} found.");
//        }

//        var airlineById = await airlineService.GetAirlineByIdAsync(2);
//        System.Console.WriteLine($"Airport {airlineById?.Name} found.");

//        var airlineToAdd = new AirlineDTO
//        {
//            Name = "Sofia Air",
//            Founded = DateOnly.MinValue,
//            FleetSize = 23,
//            Description = "Sofia Air is a Bulgarian airline"
//        };

//        var addedAirline = await airlineService.AddAirlineAsync(airlineToAdd);

//        if (addedAirline != null)
//        {
//            System.Console.WriteLine("Airline added");
//            var newAirline = await airlineService.GetAirlineByNameAsync("Sofia Air");
//            System.Console.WriteLine($"Airline {newAirline?.Name} found.");
//        }

//        var airlineToUpdate = await airlineService.UpdateAirlineFleetSizeAsync(4, 300);

//        if (airlineToUpdate)
//        {
//            System.Console.WriteLine("Airline updated");
//            var updatedAirline = await airlineService.GetAirlineByNameAsync("Sofia Air");
//            System.Console.WriteLine($"Airline {updatedAirline?.Name} fleet size updated to {updatedAirline?.FleetSize}.");
//        }

//        var deletedAirline = await airlineService.DeleteAirlineAsync(4);

//        if (deletedAirline != null)
//        {
//            System.Console.WriteLine("Airline deleted");
//        }
//    }
//}
