﻿
using Airlines.Business.Models.Aircraft;
using Airlines.Business.Models.Airline;
using Airlines.Business.Models.Airport;
using Airlines.Business.Models.Flight;
using Airlines.Business.Models.Reservation;
using Airlines.Console.Models.Commands;

namespace Airlines.Console.Factories;
public static class CommandFactory
{
    public static ICommand CreateCommand(CommandParser.CommandType commandType,
        string[] parameters,
        AirportManager airportManager,
        AirlineManager airlineManager,
        FlightManager flightManager,
        AircraftManager aircraftManager,
        ReservationManager reservationManager)
    {
        var airports = airportManager.GetAirports().Select(el => el.Identifier).ToList();
        var airlines = airlineManager.GetAirlines().Values.Select(el => el.Name).ToList();
        var flights = flightManager.GetFlights().Select(el => el.Identifier).ToList();

#pragma warning disable IDE0072 // Add missing cases
        return commandType switch
        {
            CommandParser.CommandType.Add => AddCommand.Create(parameters, airportManager, airlineManager, flightManager),
            CommandParser.CommandType.Sort => SortCommand.Create(parameters, airports, airlines, flights),
            CommandParser.CommandType.Print => PrintListsCommand.Create(airports, airlines, flights),
            CommandParser.CommandType.Search => SearchCommand.Create(parameters, airportManager, airlineManager, flightManager),
            CommandParser.CommandType.Exist => AirlineExistCommand.Create(parameters, airlineManager),
            CommandParser.CommandType.List => ListCommand.Create(parameters, airportManager),
            CommandParser.CommandType.Route => RouteCommand.Create(parameters, flightManager, airportManager),
            CommandParser.CommandType.Reserve => ReserveCommand.Create(parameters, flightManager, aircraftManager, reservationManager),
            CommandParser.CommandType.Help => HelpCommand.Create(),
            _ => throw new NotSupportedException("Unsupported command."),
        };
#pragma warning restore IDE0072 // Add missing cases
    }
}
