﻿using Airlines.Console.Models;
using Airlines.Business.Models.Airport;
using Airlines.Business.Models.Airline;
using Airlines.Business.Models.Flight;
using Airlines.Business.Models.Aircraft;
using Airlines.Business.Models.Reservation;
using Airlines.Business.Exceptions;
namespace Airlines.Console;

public class Program
{
    private static void Main()
    {
        var airportManager = new AirportManager();
        var airlineManager = new AirlineManager();
        var aircraftManager = new AircraftManager();
        var flightManager = new FlightManager();
        var reservationManager = new ReservationManager();
        var executionManager = new ExecutionManager();

        try
        {
            //await DemoServices.DemoFlightServices();
            //await DemoServices.DemoAirportServices();
            //await DemoServices.DemoAirlineServices();

            airportManager.LoadRepository("../../../../Airlines.Persistence.Basic/Data/airports.txt");
            airportManager.AirportGraph.LoadAirports(airportManager);
            airlineManager.LoadRepository("../../../../Airlines.Persistence.Basic/Data/airlines.txt");
            flightManager.LoadRepository("../../../../Airlines.Persistence.Basic/Data/flights.txt");
            airportManager.AirportGraph.LoadFlights(flightManager);
            aircraftManager.LoadRepository("../../../../Airlines.Persistence.Basic/Data/aircrafts.txt");
            flightManager.LoadRouteTreeData("../../../../Airlines.Persistence.Basic/Data/routes.txt");
        }
        catch (InvalidAirportException exception)
        {
            System.Console.WriteLine($"Invalid Airport: {exception.Message}");
        }
        catch (InvalidFlightCodeException exception)
        {
            System.Console.WriteLine($"Invalid Flight code: {exception.Message}");
        }
        catch (InvalidAirlineNameException exception)
        {
            System.Console.WriteLine($"Invalid Airline name: {exception.Message}");
        }
        catch (InvalidAircraftParameterException exception)
        {
            System.Console.WriteLine($"Invalid Aircraft parameters: {exception.Message}");
        }
        catch (DuplicateEntryException exception)
        {
            System.Console.WriteLine($"Duplicate entry: {exception.Message}");
        }
        catch (InvalidRouteException exception)
        {
            System.Console.WriteLine($"Invalid Route: {exception.Message}");
        }
        catch (InvalidReservationParameterException exception)
        {
            System.Console.WriteLine($"Invalid Reservation parameters: {exception.Message}");
        }
        catch (Exception exception)
        {
            System.Console.WriteLine(exception.Message);
        }

        ConsoleManager.StartProgram(airportManager,
                                airlineManager,
                                flightManager,
                                aircraftManager,
                                reservationManager,
                                executionManager
                                );
    }
}
