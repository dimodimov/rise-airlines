﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Aircraft;
using Airlines.Business.Models.Airline;
using Airlines.Business.Models.Airport;
using Airlines.Business.Models.Flight;
using Airlines.Business.Models.Reservation;
using Airlines.Console.Factories;
using Airlines.Console.Models.Commands;

namespace Airlines.Console.Models;

public static class ConsoleManager
{
    public static void StartProgram
        (
        AirportManager airportManager,
        AirlineManager airlineManager,
        FlightManager flightManager,
        AircraftManager aircraftManager,
        ReservationManager reservationManager,
        ExecutionManager executionManager
        )
    {
        System.Console.WriteLine("Type 'help' for a full list of the available commands.");

        while (true)
        {
            var input = System.Console.ReadLine()!.Trim();
            try
            {
                var (commandType, parameters) = CommandParser.Parse(input);

                if (commandType == CommandParser.CommandType.Exit)
                {
                    return;
                }

                if (commandType == CommandParser.CommandType.Batch)
                {
                    executionManager.ExecuteBatchCommand(parameters);
                }
                else
                {
                    var command = CommandFactory.CreateCommand(commandType,
                        parameters,
                        airportManager,
                        airlineManager,
                        flightManager,
                        aircraftManager,
                        reservationManager);

                    if (executionManager.Mode == ExecutionManager.ExecutionMode.Batch)
                    {
                        executionManager.AddToBatch(command);
                    }
                    else
                    {
                        var result = command?.Execute();
                        System.Console.WriteLine(result);
                    }
                }
            }
            catch (InvalidInputException exception)
            {
                System.Console.WriteLine($"Invalid input error: {exception.Message}");
            }
            catch (Exception exception)
            {
                System.Console.WriteLine(exception.Message);
            }
        }
    }
}
