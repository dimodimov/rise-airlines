﻿using Airlines.Business.Exceptions;
using Airlines.Console.Models.Commands;

namespace Airlines.Console.Models;
public class ExecutionManager
{
    public enum ExecutionMode
    {
        Immediate,
        Batch
    }

    private enum BatchParameter
    {
        Start,
        Run,
        Cancel
    }

    public ExecutionMode Mode = ExecutionMode.Immediate;
    public readonly List<ICommand> BatchQueue = [];

    private static BatchParameter ParseParameters(string[] parameters)
    {
        if (!Enum.TryParse(parameters[0], true, out BatchParameter batchParameter) || parameters.Length == 0)
        {
            throw new InvalidInputException("Invalid batch command.");
        }

        return batchParameter;
    }

    public void ExecuteBatchCommand(string[] parameters)
    {
        var batchCommand = ParseParameters(parameters);

        switch (batchCommand)
        {
            case BatchParameter.Start:
                StartBatchMode();
                break;
            case BatchParameter.Run:
                RunBatch();
                break;
            case BatchParameter.Cancel:
                CancelBatch();
                break;
            default:
                break;
        }
    }

    public void StartBatchMode()
    {
        if (Mode == ExecutionMode.Batch)
        {
            System.Console.WriteLine("Batch mode is currently active.");
        }
        else
        {
            Mode = ExecutionMode.Batch;
            System.Console.WriteLine("Batch mode started.");
        }
    }

    public void RunBatch()
    {
        if (Mode == ExecutionMode.Immediate)
        {
            System.Console.WriteLine("Batch mode is not active.");
        }
        else
        {
            Mode = ExecutionMode.Immediate;

            foreach (var command in BatchQueue)
            {
                System.Console.WriteLine(command.Execute());
            }

            BatchQueue.Clear();
            System.Console.WriteLine("Batch execution complete.");
        }
    }

    public void CancelBatch()
    {
        if (Mode == ExecutionMode.Immediate)
        {
            System.Console.WriteLine("Batch mode is not active.");
        }
        else
        {
            Mode = ExecutionMode.Immediate;
            BatchQueue.Clear();
            System.Console.WriteLine("Batch canceled.");
        }
    }

    public void AddToBatch(ICommand command)
    {
        BatchQueue.Add(command);
        System.Console.WriteLine("Command added to batch queue.");
    }
}
