﻿using System.Text;

namespace Airlines.Console.Models.Commands;

public class PrintListsCommand : ICommand
{
    private readonly List<string> _airports;
    private readonly List<string> _airlines;
    private readonly List<string> _flights;

    private PrintListsCommand(List<string> airports, List<string> airlines, List<string> flights)
    {
        _airports = airports;
        _airlines = airlines;
        _flights = flights;
    }

    public string? Execute()
    {
        var airportsString = string.Join(", ", _airports);
        var airlinesString = string.Join(", ", _airlines);
        var flightsString = string.Join(", ", _flights);

        var sb = new StringBuilder();

        _ = sb.AppendLine("Final Data:");
        _ = sb.AppendLine($"Airports: {airportsString}");
        _ = sb.AppendLine($"Airlines: {airlinesString}");
        _ = sb.AppendLine($"Flights: {flightsString}");

        return sb.ToString();
    }

    public static PrintListsCommand Create(List<string> airports, List<string> airlines, List<string> flights)
        => new(airports, airlines, flights);
}
