﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Aircraft;
using Airlines.Business.Models.Flight;
using Airlines.Business.Models.Reservation;

namespace Airlines.Console.Models.Commands;

public class ReserveCommand : ICommand
{
    private enum ReserveCommands
    {
        Cargo,
        Ticket,
    }

    private readonly string[] _parameters;
    private readonly FlightManager _flightManager;
    private readonly AircraftManager _aircraftManager;
    private readonly ReservationManager _reservationManager;

    private ReserveCommand(string[] parameters, FlightManager flightManager, AircraftManager aircraftManager, ReservationManager reservationManager)
    {
        _parameters = parameters;
        _flightManager = flightManager;
        _aircraftManager = aircraftManager;
        _reservationManager = reservationManager;
    }

    public string? Execute()
    {
        var reserveParams = _parameters[1].Split(',');
        return Enum.Parse<ReserveCommands>(_parameters[0], true) switch
        {
            ReserveCommands.Cargo => _reservationManager.ReserveCargo(
                                reserveParams[0],
                                int.Parse(reserveParams[1]),
                                double.Parse(reserveParams[2]),
                                _flightManager,
                                _aircraftManager
                                ),
            ReserveCommands.Ticket => _reservationManager.ReserveTicket(
                                reserveParams[0],
                                int.Parse(reserveParams[1]),
                                int.Parse(reserveParams[2]),
                                int.Parse(reserveParams[3]),
                                _flightManager,
                                _aircraftManager),
            _ => throw new InvalidInputException("Invalid reserve command."),
        };
    }

    public static ReserveCommand Create(string[] parameters, FlightManager flightManager, AircraftManager aircraftManager, ReservationManager reservationManager)
    {
        if (parameters == null || parameters.Length < 2)
        {
            throw new InvalidInputException("Invalid parameters for 'reserve' command.");
        }

        if (!Enum.TryParse(parameters[0], true, out ReserveCommands _))
        {
            throw new InvalidInputException("Invalid reserve command.");
        }

        return new ReserveCommand(parameters, flightManager, aircraftManager, reservationManager);
    }
}
