﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Airline;
using Airlines.Business.Models.Airport;
using Airlines.Business.Models.Flight;
using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Console.Models.Commands;

public class SearchCommand : ICommand
{
    private readonly AirportManager _airportManager;
    private readonly AirlineManager _airlineManager;
    private readonly FlightManager _flightManager;
    private readonly string[] _parameters;

    private SearchCommand(string[] parameters, AirportManager airportManager, AirlineManager airlineManager, FlightManager flightManager)
    {
        _parameters = parameters;
        _airportManager = airportManager;
        _airlineManager = airlineManager;
        _flightManager = flightManager;
    }

    public string Execute()
    {
        var airports = _airportManager.GetAirports().ToList();
        var airlines = _airlineManager.GetAirlines().Values.ToList();
        var flights = _flightManager.GetFlights().ToList();

        var searchTerm = string.Join(" ", _parameters);
        return SearchAllLists(airports, airlines, flights, searchTerm);
    }

    public static string SearchAllLists(List<IAirport> airports, List<IAirline> airlines, List<IFlight> flights, string searchTerm)
    {
        string? result;

        if (airports.Count > 0)
        {
            result = airports.FirstOrDefault(el => el.Identifier == searchTerm)?.Identifier;

            if (!string.IsNullOrWhiteSpace(result))
            {
                return $"Airport found: {result}.";
            }
        }

        if (airlines.Count > 0)
        {
            result = airlines.FirstOrDefault(el => el.Name == searchTerm)?.Name;

            if (!string.IsNullOrWhiteSpace(result))
            {
                return $"Airline found: {result}.";
            }
        }

        if (flights.Count > 0)
        {
            result = flights.FirstOrDefault(el => el.Identifier == searchTerm)?.Identifier;

            if (!string.IsNullOrWhiteSpace(result))
            {
                return $"Flight found: {result}.";
            }
        }

        return $"{searchTerm} not found.";
    }

    public static SearchCommand Create(string[] parameters, AirportManager airportManager, AirlineManager airlineManager, FlightManager flightManager)
    {
        if (parameters == null || parameters.Length < 1)
        {
            throw new InvalidInputException("Invalid search command format. Input your query after the search command.");
        }

        return new SearchCommand(parameters, airportManager, airlineManager, flightManager);
    }
}
