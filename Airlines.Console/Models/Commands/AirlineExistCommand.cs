﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Airline;

namespace Airlines.Console.Models.Commands;

public class AirlineExistCommand : ICommand
{
    private readonly string[] _parameters;
    private readonly AirlineManager _airlineManager;

    private AirlineExistCommand(string[] parameters, AirlineManager airlineManager)
    {
        _parameters = parameters;
        _airlineManager = airlineManager;
    }

    public string? Execute()
    {
        var airlineName = string.Join(" ", _parameters);
        return _airlineManager.AirlineExists(airlineName).ToString();
    }

    public static AirlineExistCommand Create(string[] parameters, AirlineManager airlineManager)
    {
        if (parameters == null || parameters.Length < 1)
        {
            throw new InvalidInputException("Invalid parameter for 'exist' command.");
        }

        return new AirlineExistCommand(parameters, airlineManager);
    }
}
