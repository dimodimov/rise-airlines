﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Airport;
using Airlines.Business.Models.Flight;
using Airlines.Business.Models.Strategies.RouteSearchStrategy;
using System.Text;

namespace Airlines.Console.Models.Commands;

public class RouteCommand : ICommand
{
    private enum RouteCommands
    {
        New,
        Add,
        Remove,
        Print,
        Search,
        Find
    }

    private enum Strategy
    {
        Cheap,
        Short,
        Stops
    }

    private readonly string[] _parameters;
    private readonly string[] _secondaryParams;
    private readonly FlightManager _flightManager;
    private readonly AirportManager _airportManager;


    private RouteCommand(string[] parameters, string[] secondaryParams, FlightManager flightManager, AirportManager airportManager)
    {
        _parameters = parameters;
        _flightManager = flightManager;
        _airportManager = airportManager;
        _secondaryParams = secondaryParams;
    }

    public string? Execute()
    {
        var routeCommand = Enum.Parse<RouteCommands>(_parameters[0], true);
        var result = routeCommand switch
        {
            RouteCommands.New => _flightManager.AddRoute(),
            RouteCommands.Add => _flightManager.Route?.AddFlight(_parameters[1], _flightManager),
            RouteCommands.Remove => _flightManager.Route?.RemoveLastFlight(),
            RouteCommands.Print => AllRoutes(_flightManager),
            RouteCommands.Search => HandleSearch(_secondaryParams[0], _secondaryParams[1], _secondaryParams[2], _airportManager),
            RouteCommands.Find => _airportManager.AirportGraph.IsConnected(_secondaryParams[0], _secondaryParams[1]).ToString(),
            _ => throw new InvalidInputException("Invalid route command."),
        };

        return result;
    }

    private static string AllRoutes(FlightManager flightManager)
    {
        if (!flightManager.Route!.RouteList.Any())
        {
            return "Route empty.";
        }

        var sb = new StringBuilder();

        _ = sb.AppendLine("Route Details:");
        foreach (var flight in flightManager.Route.RouteList)
        {
            _ = sb.AppendLine($"Flight {flight.Identifier}, Departure Airport: {flight.DepartureAirport}, Arrival Airport: {flight.ArrivalAirport}.");
        }

        return sb.ToString();
    }

    public static RouteCommand Create(string[] parameters, FlightManager flightManager, AirportManager airportManager)
    {
        if (parameters == null)
        {
            throw new InvalidInputException("Invalid parameters for 'route' command.");
        }

        if (!Enum.TryParse(parameters[0], true, out RouteCommands _))
        {
            throw new InvalidInputException("Invalid route command.");
        }

        string[]? secondaryParams = [];

        if (parameters.Length > 1 && parameters[1].Contains(','))
        {
            secondaryParams = parameters[1].Split(',');

            if (secondaryParams.Length != 3 || !Enum.TryParse(secondaryParams[2], true, out Strategy _))
            {
                throw new InvalidInputException("The search and find commands must contain 3 parameters for departure, arrival and search strategy 'cheap','short' or 'stops'.");
            }
        }

        return new RouteCommand(parameters, secondaryParams, flightManager, airportManager);
    }

    private static string HandleSearch(string destinationAirport, string arrivalAirport, string strategy, AirportManager airportManager)
    {
        var parseStrategy = Enum.Parse<Strategy>(strategy, true);
        switch (parseStrategy)
        {
            case Strategy.Cheap:
                airportManager.AirportGraph.SetStrategy(new ShortestPathByPrice());
                break;
            case Strategy.Short:
                airportManager.AirportGraph.SetStrategy(new ShortestPathByDuration());
                break;
            case Strategy.Stops:
                airportManager.AirportGraph.SetStrategy(new ShortestPathFewestStops());
                break;
            default:
                throw new InvalidInputException("Invalid strategy type for add command.");
        }

        var result = airportManager.AirportGraph.GetShortestPath(destinationAirport, arrivalAirport);

        return result;
    }
}
