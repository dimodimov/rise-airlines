﻿using Airlines.Business.Exceptions;
using Airlines.Business.Utilities;

namespace Airlines.Console.Models.Commands;
public class SortCommand : ICommand
{
    private enum SortTarget
    {
        Airports,
        Airlines,
        Flights
    }

    private enum SortOrder
    {
        Ascending,
        Descending
    }

    private readonly List<string> _airports;
    private readonly List<string> _airlines;
    private readonly List<string> _flights;
    private readonly string[] _parameters;

    private SortCommand(string[] parameters, List<string> airports, List<string> airlines, List<string> flights)
    {
        _parameters = parameters;
        _airports = airports;
        _airlines = airlines;
        _flights = flights;
    }

    public string? Execute()
    {
        var sortOrder = SortOrder.Ascending;

        if (_parameters.Length == 2 && !Enum.TryParse(_parameters[1], true, out sortOrder))
        {
            throw new InvalidInputException("Invalid sorting order.");
        }

        switch (Enum.Parse<SortTarget>(_parameters[0], true))
        {
            case SortTarget.Airports:
                _airports.BubbleSort(sortOrder.ToString());
                return $"Airports Sorted {string.Join(", ", _airports)}";
            case SortTarget.Airlines:
                _airlines.SelectionSort(sortOrder.ToString());
                return $"Airlines Sorted {string.Join(", ", _airlines)}";
            case SortTarget.Flights:
                _flights.SelectionSort(sortOrder.ToString());
                return $"Flights Sorted {string.Join(", ", _flights)}";
            default:
                return "Invalid list type for sort command.";
        }
    }

    public static SortCommand Create(string[] parameters, List<string> airports, List<string> airlines, List<string> flights)
    {
        if (parameters == null || parameters.Length == 0)
        {
            throw new InvalidInputException("Invalid format of sort command.");
        }

        if (!Enum.TryParse(parameters[0], true, out SortTarget _))
        {
            throw new InvalidInputException("Invalid list type for sort command.");
        }

        return new SortCommand(parameters, airports, airlines, flights);
    }
}
