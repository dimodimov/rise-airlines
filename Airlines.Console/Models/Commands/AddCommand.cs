﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Airline;
using Airlines.Business.Models.Airport;
using Airlines.Business.Models.Flight;

namespace Airlines.Console.Models.Commands;

public class AddCommand : ICommand
{
    private enum CollectionType
    {
        Airport,
        Airline,
        Flight
    }

    private readonly string[] _parameters;
    private readonly AirportManager _airportManager;
    private readonly AirlineManager _airlineManager;
    private readonly FlightManager _flightManager;

    private AddCommand(string[] parameters, AirportManager airportManager, AirlineManager airlineManager, FlightManager flightManager)
    {
        _parameters = parameters;
        _airportManager = airportManager;
        _airlineManager = airlineManager;
        _flightManager = flightManager;
    }
    private string? HandleAirport()
    {
        var airportParams = string.Join(" ", _parameters.Skip(1)).Split(',');
        var airport = new Airport(airportParams[0], airportParams[1], airportParams[2], airportParams[3]);
        var result = _airportManager.AddAirport(airport);
        return result;
    }

    private string? HandleAirline()
    {
        var airlineName = string.Join(" ", _parameters.Skip(1));
        var airline = new Airline(airlineName);
        var result = _airlineManager.AddAirline(airline);
        return result;
    }

    private string? HandleFlight()
    {
        var flightParams = string.Join(" ", _parameters.Skip(1)).Split(',');
        var flight = new Flight(flightParams[0], flightParams[1], flightParams[2], decimal.Parse(flightParams[3]), double.Parse(flightParams[4]), flightParams[5]);
        var result = _flightManager.AddFlight(flight);
        return result;
    }

    public string? Execute()
    {
        var listType = Enum.Parse<CollectionType>(_parameters[0], true);

        var result = listType switch
        {
            CollectionType.Airport => HandleAirport(),
            CollectionType.Airline => HandleAirline(),
            CollectionType.Flight => HandleFlight(),
            _ => throw new InvalidInputException("Invalid list type for add command."),
        };

        return result;
    }

    public static AddCommand Create(string[] parameters, AirportManager airportManager, AirlineManager airlineManager, FlightManager flightManager)
    {
        if (parameters == null || parameters.Length < 2)
        {
            throw new InvalidInputException("Invalid parameters for 'add' command.");
        }

        if (!Enum.TryParse(parameters[0], true, out CollectionType _) || parameters.Length < 2)
        {
            throw new InvalidInputException("Invalid list type or command parameters for the 'add' command. Valid format add <list> <entity name>.");
        }

        return new AddCommand(parameters, airportManager, airlineManager, flightManager);
    }
}
