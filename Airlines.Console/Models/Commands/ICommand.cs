﻿
namespace Airlines.Console.Models.Commands;
public interface ICommand
{
    string? Execute();
}
