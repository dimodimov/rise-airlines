﻿using System.Text;

namespace Airlines.Console.Models.Commands;

public class HelpCommand : ICommand
{
    private HelpCommand() { }
    public string Execute()
    {
        var sb = new StringBuilder();

        _ = sb.AppendLine("Command list:");
        _ = sb.AppendLine("add airport <identifier>,<name>,<city>,<country>: Adds a new airport to the list. Separate params with ',' without spaces.");
        _ = sb.AppendLine("add airline <name>: Adds a new airline to the list");
        _ = sb.AppendLine("add flight  <name>,<departure airport>,<arrival airport>,<price>,<duration>,<aircraft>: Adds a new flight to the list");
        _ = sb.AppendLine("sort <list> <optional: sort order>: Sorts a list (e.g. sort airports ascending, sort flights descending). If sort order is not specified, the list will be sorted in ascending order.");
        _ = sb.AppendLine("search <search term>: Search for a term in all lists. Sort all lists before searching!");
        _ = sb.AppendLine("print: Print Airports, Airlines, and Flights");
        _ = sb.AppendLine("exist <airline name>: Returns if an airline exists or not.");
        _ = sb.AppendLine("list <name of city/country>,<type of city/country>: Displays all airports in a city or country.");
        _ = sb.AppendLine("route new: Creates a new route.");
        _ = sb.AppendLine("route add <existing flight identifier>: Adds a new route for an existing flight that logically connects to the route.");
        _ = sb.AppendLine("route remove: Removes the last route.");
        _ = sb.AppendLine("route search <departureAirport>,<arrivalAirport>: Finds the shortest route in the airport graph.");
        _ = sb.AppendLine("route find <departureAirport>,<arrivalAirport>: Checks if a route exists.");
        _ = sb.AppendLine("route print: Prints the routes.");
        _ = sb.AppendLine("reserve cargo <flight identifier>,<cargo weight>,<cargo volume>: Reserves cargo for the specified existing flight.");
        _ = sb.AppendLine("reserve ticket <flight identifier>,<seats>,<small baggage count>,<large baggage count>: Reserves a ticket for the specified existing flight.");
        _ = sb.AppendLine("batch start: Activates batch mode.");
        _ = sb.AppendLine("batch run: Runs commands from the batch queue.");
        _ = sb.AppendLine("batch cancel: Cancels batch mode and clears the batch queue.");
        _ = sb.AppendLine("help: Display available commands.");
        _ = sb.AppendLine("exit: Exit the program.");

        return sb.ToString();
    }

    public static HelpCommand Create() => new();
}
