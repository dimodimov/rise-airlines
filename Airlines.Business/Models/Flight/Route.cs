﻿using Airlines.Business.Exceptions;
using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Business.Models.Flight;
public class Route
{
    private readonly LinkedList<IFlight> _route;

    public Route() => _route = new LinkedList<IFlight>();

    public IEnumerable<IFlight> RouteList => _route;

    internal string? AddFlight(string flightIdentifier, FlightManager flightManager)
    {
        var flight = flightManager.GetFlightById(flightIdentifier)
            ?? throw new InvalidFlightCodeException($"{flightIdentifier} not found. Add the flight before you add it to a route!");

        if (_route.Count > 0 && _route.Last!.Value.ArrivalAirport != flight.DepartureAirport)
        {
            throw new InvalidRouteException("Error: New flight does not connect logically to the route.");
        }

        _ = _route.AddLast(flight);
        return $"Flight {flight.Identifier} added to the route.";
    }

    internal string? RemoveLastFlight()
    {
        if (_route.Count == 0)
        {
            throw new InvalidRouteException("Error: Route is empty.");
        }

        var removedFlight = _route.Last!.Value;
        _route.RemoveLast();
        return $"Flight {removedFlight.Identifier} removed from the route.";
    }
}
