﻿using Airlines.Business.Exceptions;
using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Business.Models.Flight;
public class FlightRouteTree
{
    private string _root;

#pragma warning disable CS8618

    public FlightRouteTree(string root) => Root = root;

#pragma warning disable CS8618

    public string Root
    {
        get => _root;
        set
        {
            Airport.Airport.ValidateIdentifier(value);
            _root = value;
        }
    }

    public List<FlightRouteTreeNode> Children { get; } = [];

    internal void AddChild(IFlight value)
    {
        if (value.DepartureAirport == value.ArrivalAirport)
        {
            throw new InvalidRouteException("Flight route has the same airport for both arrival and departure.");
        }

        Children.Add(new FlightRouteTreeNode(value));
    }

    internal FlightRouteTreeNode? FindRoute(string arrivalAirport)
    {
        foreach (var child in Children)
        {
            var result = child.FindRoute(arrivalAirport);

            if (result != null)
            {
                return result;
            }
        }

        return null;
    }

    internal string? GetFlightRoute(string arrivalAirport)
    {
        var route = new List<string>();
        _ = GetFlightRouteRecursive(arrivalAirport, route);

        if (route.Count == 0)
        {
            return null;
        }

        return string.Join("->", route);
    }

    private bool GetFlightRouteRecursive(string arrivalAirport, List<string> route)
    {
        route.Add(_root);
        foreach (var child in Children)
        {
            if (child.GetFlightRouteRecursive(_root, arrivalAirport, route))
            {
                return true;
            }
        }

        route.RemoveAt(route.Count - 1);

        return false;
    }
}
