﻿using Airlines.Business.Exceptions;
using Airlines.Business.Utilities;
using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Business.Models.Flight;
public class Flight : IFlight
{
    private string _identifier;
    private string _departureAirport;
    private string _arrivalAirport;
    private decimal _price;
    private double _duration;
    private string _aircraftModel;

#pragma warning disable CS8618

    // All setters have a Null check, the warning appears to be wrong.
    public Flight(
        string identifier,
        string departureAirport,
        string arrivalAirport,
        decimal price,
        double duration,
        string aircraftModel
        )

#pragma warning restore CS8618 
    {
        Identifier = identifier;
        DepartureAirport = departureAirport;
        ArrivalAirport = arrivalAirport;
        Price = price;
        Duration = duration;
        AircraftModel = aircraftModel;
    }

    public string Identifier
    {
        get => _identifier;

        set
        {
            if (string.IsNullOrWhiteSpace(value) || !DataValidator.IsAlphanumeric(value))
            {
                throw new InvalidFlightCodeException($"The flight identifier is mandatory and it must have only alphanumeric characters.");
            }

            _identifier = value;
        }
    }

    public string DepartureAirport
    {
        get => _departureAirport;

        set
        {
            Airport.Airport.ValidateIdentifier(value);
            _departureAirport = value;
        }
    }

    public string ArrivalAirport
    {
        get => _arrivalAirport;

        set
        {
            Airport.Airport.ValidateIdentifier(value);
            _arrivalAirport = value;
        }
    }

    public string AircraftModel
    {
        get => _aircraftModel;
        set
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new InvalidAircraftParameterException($"The aircraft model is mandatory.");
            }

            _aircraftModel = value;
        }
    }

    public decimal Price
    {
        get => _price;
        set
        {
            if (value <= 0)
            {
                throw new InvalidInputException("The price can't be 0 or negative.");
            }

            _price = value;
        }
    }

    public double Duration
    {
        get => _duration;
        set
        {
            if (value <= 0)
            {
                throw new InvalidInputException("The duration can't be 0 or negative.");
            }

            _duration = value;
        }
    }
}
