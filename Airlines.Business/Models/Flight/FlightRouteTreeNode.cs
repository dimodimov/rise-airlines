﻿using Airlines.Business.Exceptions;
using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Business.Models.Flight;

public class FlightRouteTreeNode
{
    private IFlight _value;

#pragma warning disable CS8618

    public FlightRouteTreeNode(IFlight value) => Value = value;

#pragma warning restore CS8618

    public IFlight Value
    {
        get => _value;
        set => _value = value ?? throw new InvalidInputException("Cannot insert a null value.");
    }

    public List<FlightRouteTreeNode> Children { get; } = [];

    internal void AddChild(IFlight value)
    {
        if (value.DepartureAirport == value.ArrivalAirport)
        {
            throw new InvalidRouteException("Flight route has the same airport for both arrival and departure.");
        }

        Children.Add(new FlightRouteTreeNode(value));
    }

    internal FlightRouteTreeNode? FindRoute(string arrivalAirport)
    {
        if (_value.ArrivalAirport == arrivalAirport)
        {
            return this;
        }

        foreach (var child in Children)
        {
            var result = child.FindRoute(arrivalAirport);

            if (result != null)
            {
                return result;
            }
        }

        return null;
    }

    public bool GetFlightRouteRecursive(string currentAirport, string arrivalAirport, List<string> route)
    {
        if (Value.DepartureAirport == currentAirport)
        {
            route.Add(Value.ArrivalAirport);

            if (Value.ArrivalAirport == arrivalAirport)
            {
                return true;
            }

            foreach (var child in Children)
            {
                if (child.GetFlightRouteRecursive(Value.ArrivalAirport, arrivalAirport, route))
                {
                    return true;
                }
            }

            route.RemoveAt(route.Count - 1);
        }

        return false;
    }
}
