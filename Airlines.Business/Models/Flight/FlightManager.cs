﻿using Airlines.Business.Exceptions;
using Airlines.Persistence.Basic;
using Airlines.Persistence.Basic.EntityInterfaces;
using Airlines.Persistence.Basic.Repositories;

namespace Airlines.Business.Models.Flight;
public class FlightManager
{
    private readonly FlightRepository _flightRepository = new();

    public Route? Route { get; set; }

    public FlightRouteTree? RouteTree { get; set; }

    public IReadOnlyList<IFlight> GetFlights() => _flightRepository.Get();

    internal string? AddFlight(IFlight flight)
    {
        if (_flightRepository.Exists(flight))
        {
            throw new DuplicateEntryException("The flight is already in the database.");
        }

        _flightRepository.Add(flight);
        return $"{flight.Identifier} added successfully.";
    }

    internal string AddRoute()
    {
        Route = new Route();
        return "New route added.";
    }

    public IFlight? GetFlightById(string identifier)
        => _flightRepository.GetById(identifier);

    private static List<string[]> GetRouteData(string filePath) => FileReader.StreamReader(filePath);

    public void LoadRouteTreeData(string filePath)
    {
        var routeData = GetRouteData(filePath);
        var root = new FlightRouteTree(routeData[0][0]);
        RouteTree = root;

        for (var i = 1; i < routeData.Count; i++)
        {
            var flight = _flightRepository.GetById(routeData[i][0])
                ?? throw new InvalidFlightCodeException("The flight does not exist in the database.");

            if (flight.DepartureAirport == root.Root)
            {
                root.AddChild(flight);

            }
            else
            {
                var flightNode = root.FindRoute(flight.DepartureAirport);

                if (flightNode != null)
                {

                    if (flightNode.Children.Any(node => node.Value == flight))
                    {
                        throw new InvalidRouteException("The flight is already in the route.");
                    }

                    flightNode.AddChild(flight);
                }
            }
        }
    }

    private static IEnumerable<IFlight> GetTransformedData(string filePath)
        => FlightRepository.GetData(filePath)
            .Select(flight => new Flight(flight[0], flight[1], flight[2], decimal.Parse(flight[3]), double.Parse(flight[4]), flight[5]));

    public void LoadRepository(string filePath) => _flightRepository.LoadData(GetTransformedData(filePath));
}
