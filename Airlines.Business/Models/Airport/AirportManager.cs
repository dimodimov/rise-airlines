﻿using Airlines.Business.Exceptions;
using Airlines.Persistence.Basic.EntityInterfaces;
using Airlines.Persistence.Basic.Repositories;

namespace Airlines.Business.Models.Airport;

public class AirportManager
{
    private readonly AirportRepository _airportRepository = new();
    public AirportManager() => AirportGraph = new AirportGraph();

    public AirportGraph AirportGraph { get; }

    internal string ListAirportsBy(string name, string type)
    {
        if (type.Equals("country", StringComparison.CurrentCultureIgnoreCase))
        {
            var result = _airportRepository.GetAirportsByCountry(name);
            return result != null ?
                string.Join(", ", result.Select(airport => airport.Identifier)) :
                $"No airports in found in {type} {name}.";
        }

        if (type.Equals("city", StringComparison.CurrentCultureIgnoreCase))
        {
            var result = _airportRepository.GetAirportsByCity(name);

            return result != null ?
                string.Join(", ", result.Select(airport => airport.Identifier)) :
                $"No airports in found in {type} {name}.";
        }

        return "Invalid type. Please select city or country.";
    }

    internal string? AddAirport(Airport airport)
    {
        if (_airportRepository.Exists(airport))
        {
            throw new DuplicateEntryException("The airport is already in the database.");
        }

        _airportRepository.Add(airport);

        return $"Airport {airport.Identifier} added successfully!";
    }

    public IEnumerable<IAirport> GetAirports() => _airportRepository.Get();

    private static IEnumerable<IAirport> GetTransformedData(string filePath)
        => AirportRepository.GetData(filePath)
        .Select(airport => new Airport(airport[0], airport[1], airport[2], airport[3]));

    public void LoadRepository(string filePath) => _airportRepository.LoadData(GetTransformedData(filePath));
}
