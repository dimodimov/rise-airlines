﻿using Airlines.Business.Exceptions;
using Airlines.Business.Utilities;
using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Business.Models.Airport;
public class Airport : IAirport
{
#pragma warning disable IDE1006 // Naming Styles
    private const int AIRPORT_IDENTIFIER_MIN_LENGTH = 2;
    private const int AIRPORT_IDENTIFIER_MAX_LENGTH = 4;
#pragma warning restore IDE1006 // Naming Styles

    private string _identifier;
    private string _name;
    private string _city;
    private string _country;

#pragma warning disable CS8618
    // All setters have a Null check, the warning appears to be wrong.

    public Airport(string identifier, string name, string city, string country)

#pragma warning restore CS8618 
    {
        Identifier = identifier;
        Name = name;
        City = city;
        Country = country;
    }

    public string Identifier
    {
        get => _identifier;

        set
        {
            ValidateIdentifier(value);
            _identifier = value;
        }
    }

    public string Name
    {
        get => _name;

        set
        {
            if (string.IsNullOrWhiteSpace(value) || !DataValidator.IsLettersOrSpaceOnly(value))
            {
                throw new InvalidAirportException("The airport name must have only letters or spaces.");
            }

            _name = value;
        }
    }

    public string City
    {
        get => _city;

        set
        {
            if (string.IsNullOrWhiteSpace(value) || !DataValidator.IsLettersOrSpaceOnly(value))
            {
                throw new InvalidAirportException("The airport city must have only letters or spaces.");
            }

            _city = value;
        }
    }

    public string Country
    {
        get => _country;

        set
        {
            if (string.IsNullOrWhiteSpace(value) || !DataValidator.IsLettersOrSpaceOnly(value))
            {
                throw new InvalidAirportException("The airport country must have only letters or spaces.");
            }

            _country = value;
        }
    }

    internal static bool IsValidIdentifier(string value)
    {
        if (value.Length is < AIRPORT_IDENTIFIER_MIN_LENGTH or > AIRPORT_IDENTIFIER_MAX_LENGTH)
        {
            return false;
        }

        return DataValidator.IsAlphanumeric(value);
    }

    internal static void ValidateIdentifier(string value)
    {
        if (string.IsNullOrWhiteSpace(value) || !IsValidIdentifier(value))
        {
            throw new InvalidAirportException($"The airport identifier must have between {AIRPORT_IDENTIFIER_MIN_LENGTH} and {AIRPORT_IDENTIFIER_MAX_LENGTH} alphanumeric characters.");
        }
    }
}
