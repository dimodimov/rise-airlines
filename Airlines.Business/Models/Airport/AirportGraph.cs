﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Flight;
using Airlines.Business.Models.Strategies.RouteSearchStrategy;
using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Business.Models.Airport;
public class AirportGraph
{
    internal readonly Dictionary<string, List<IFlight>> _adjacencyList;
    private IRouteSearchStrategy _shortestPathStrategy = new ShortestPathFewestStops();

    public AirportGraph() => _adjacencyList = [];


    // Already validated by AirportManager
    public void AddAirport(string airport)
    {
        if (!_adjacencyList.ContainsKey(airport))
        {
            _adjacencyList[airport] = [];
        }
    }

    // Already validated by FlightManager
    private void AddFlight(IFlight flight)
    {
        if (!_adjacencyList[flight.DepartureAirport].Contains(flight))
        {
            _adjacencyList[flight.DepartureAirport].Add(flight);
        }
    }

    internal bool IsConnected(string departureAirport, string arrivalAirport)
    {
        if (!_adjacencyList.ContainsKey(departureAirport) || !_adjacencyList.ContainsKey(arrivalAirport))
        {
            return false;
        }

        if (departureAirport == arrivalAirport)
        {
            throw new InvalidRouteException("The departure airport matches the arrival airport.");
        }

        var queue = new Queue<string>();
        var visited = new HashSet<string>();

        queue.Enqueue(departureAirport);
        _ = visited.Add(departureAirport);

        while (queue.Count > 0)
        {
            var currentAirport = queue.Dequeue();

            if (currentAirport == arrivalAirport)
            {
                return true;
            }

            foreach (var flight in _adjacencyList[currentAirport])
            {
                var neighbor = flight.ArrivalAirport;
                if (!visited.Contains(neighbor))
                {
                    queue.Enqueue(neighbor);
                    _ = visited.Add(neighbor);
                }
            }
        }

        return false;
    }

    internal void SetStrategy(IRouteSearchStrategy strategy) => _shortestPathStrategy = strategy;

    internal string GetShortestPath(string departureAirport, string arrivalAirport)
        => _shortestPathStrategy.FindRoute(departureAirport, arrivalAirport, this);

    internal void LoadAirports(AirportManager airportManager)
    {
        foreach (var airport in airportManager.GetAirports())
        {
            AddAirport(airport.Identifier);
        }
    }

    internal void LoadFlights(FlightManager flightManager)
    {
        foreach (var flight in flightManager.GetFlights())
        {
            AddFlight(flight);
        }
    }
}