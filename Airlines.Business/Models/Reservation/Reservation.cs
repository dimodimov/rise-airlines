﻿using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Business.Models.Reservation;
public abstract class Reservation(string flightIdentifier) : IReservation
{
    public string FlightIdentifier { get; set; } = flightIdentifier;
}
