﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Flight;
using Airlines.Business.Models.Aircraft;
using Airlines.Persistence.Basic.Repositories;
using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Business.Models.Reservation;
public class ReservationManager
{
#pragma warning disable IDE1006 // Naming Styles
    private const int SMALL_BAGGAGE_WEIGHT = 15;
    private const int LARGE_BAGGAGE_WEIGHT = 30;
    private const double SMALL_BAGGAGE_VOLUME = 0.045;
    private const double LARGE_BAGGAGE_VOLUME = 0.090;
#pragma warning restore IDE1006 // Naming Styles

    private readonly ReservationRepository _reservationRepository = new();

    internal static void ValidateCargo(int aircraftCargoWeight, double aircraftCargoVolume, int cargoWeight, double cargoVolume)
    {
        if (aircraftCargoWeight < cargoWeight)
        {
            throw new InvalidReservationParameterException("The cargo exceeds the aircraft limit.");
        }

        if (aircraftCargoVolume < cargoVolume)
        {
            throw new InvalidReservationParameterException("The cargo volume exceeds the aircraft limit.");
        }
    }

    internal static void ValidateSeats(int aircraftSeats, int seats)
    {
        if (aircraftSeats < seats)
        {
            throw new InvalidReservationParameterException($"Not enough seats. {aircraftSeats} are left.");
        }
    }

    public IReadOnlyList<ICargoReservation> GetCargoReservationList()
        => _reservationRepository.GetCargoReservationList();
    public IReadOnlyList<ITicketReservation> GetTicketReservationList()
        => _reservationRepository.GetTicketReservationList();

    internal string? ReserveCargo(
        string flightIdentifier,
        int cargoWeight,
        double cargoVolume,
        FlightManager flightManager,
        AircraftManager aircraftManager
        )
    {
        var flight = flightManager.GetFlightById(flightIdentifier)
            ?? throw new InvalidFlightCodeException($"Flight with id {flightIdentifier} doesn't exist.");

        var aircraft = aircraftManager.GetCargoAircraftByModel(flight.AircraftModel)
            ?? throw new InvalidAircraftParameterException($"Aircraft with model {flight.AircraftModel} doesn't exist.");

        ValidateCargo(aircraft.CargoWeight, aircraft.CargoVolume, cargoWeight, cargoVolume);

        aircraft.CargoWeight -= cargoWeight;
        aircraft.CargoVolume -= cargoVolume;

        var reservation = new CargoReservation(flightIdentifier, cargoWeight, cargoVolume);
        _reservationRepository.Add(reservation);
        return $"Reservation for {flightIdentifier} submitted.";
    }

    internal string? ReserveTicket(
        string flightIdentifier,
        int seats,
        int smallBaggageCount,
        int largeBaggageCount,
        FlightManager flightManager,
        AircraftManager aircraftManager
        )
    {
        var flight = flightManager.GetFlightById(flightIdentifier)
             ?? throw new InvalidFlightCodeException($"Flight with id {flightIdentifier} doesn't exist.");

        var aircraft = aircraftManager.GetPassengerAircraftByModel(flight.AircraftModel)
            ?? throw new InvalidAircraftParameterException($"Aircraft with model {flight.AircraftModel} doesn't exist.");

        var cargoWeight = (smallBaggageCount * SMALL_BAGGAGE_WEIGHT)
            + (largeBaggageCount * LARGE_BAGGAGE_WEIGHT);

        var cargoVolume = (smallBaggageCount * SMALL_BAGGAGE_VOLUME)
            + (largeBaggageCount * LARGE_BAGGAGE_VOLUME);

        ValidateCargo(aircraft.CargoWeight, aircraft.CargoVolume, cargoWeight, cargoVolume);
        ValidateSeats(aircraft.Seats, seats);

        aircraft.Seats -= seats;
        aircraft.CargoWeight -= cargoWeight;
        aircraft.CargoVolume -= cargoVolume;

        var reservation = new TicketReservation(flightIdentifier, seats, smallBaggageCount, largeBaggageCount);
        _reservationRepository.Add(reservation);
        return $"Reservation for {flightIdentifier} submitted.";
    }
}
