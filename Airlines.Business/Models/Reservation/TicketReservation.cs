﻿using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Business.Models.Reservation;
public class TicketReservation(
    string flightIdentifier,
    int seats, int smallBaggageCount,
    int largeBaggageCount
    )
    : Reservation(flightIdentifier), ITicketReservation
{
    public int Seats { get; set; } = seats;
    public int SmallBaggageCount { get; set; } = smallBaggageCount;
    public int LargeBaggageCount { get; set; } = largeBaggageCount;
}
