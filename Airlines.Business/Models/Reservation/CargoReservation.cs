﻿using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Business.Models.Reservation;
public class CargoReservation(string flightIdentifier, int cargoWeight, double cargoVolume)
    : Reservation(flightIdentifier), ICargoReservation
{
    public int CargoWeight { get; set; } = cargoWeight;
    public double CargoVolume { get; set; } = cargoVolume;
}
