﻿using Airlines.Business.Exceptions;
using Airlines.Persistence.Basic.EntityInterfaces;
using Airlines.Persistence.Basic.Repositories;

namespace Airlines.Business.Models.Airline;

public class AirlineManager
{
    private readonly AirlineRepository _airlineRepository = new();

    internal string? AddAirline(IAirline airline)
    {
        if (_airlineRepository.Exists(airline.Name))
        {
            throw new DuplicateEntryException("The airline is already in the database.");
        }

        _airlineRepository.Add(airline);
        return $"Airline {airline.Name} added successfully.";
    }

    public IReadOnlyDictionary<string, IAirline> GetAirlines() => _airlineRepository.Get();

    public bool AirlineExists(string name) => _airlineRepository.Exists(name);

    private static IEnumerable<IAirline> GetTransformedData(string filePath)
        => AirlineRepository.GetData(filePath).Select(airline => new Airline(airline[0]));

    public void LoadRepository(string filePath) => _airlineRepository.LoadData(GetTransformedData(filePath));
}
