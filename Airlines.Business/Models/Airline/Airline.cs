﻿using Airlines.Persistence.Basic.EntityInterfaces;
using Airlines.Business.Exceptions;

namespace Airlines.Business.Models.Airline;
public class Airline : IAirline
{
#pragma warning disable IDE1006 // Naming Styles
    private const int AIRLINE_NAME_MAX_LENGTH = 5;
#pragma warning restore IDE1006 // Naming Styles

    private string _name;

#pragma warning disable CS8618
    // There is a null check in the getter, this warning appears to be wrong.
    // No way to create an instance without a name.

    public Airline(string name) => Name = name;

#pragma warning restore CS8618

    public string Name
    {
        get => _name;

        set
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new InvalidAirlineNameException("The airline name can't be empty.");
            }

            if (!IsValidAirline(value))
            {
                throw new InvalidAirlineNameException($"The airline can't be longer than {AIRLINE_NAME_MAX_LENGTH} characters.");
            }

            _name = value;
        }
    }

    internal static bool IsValidAirline(string input)
        => input.Length <= AIRLINE_NAME_MAX_LENGTH;
}
