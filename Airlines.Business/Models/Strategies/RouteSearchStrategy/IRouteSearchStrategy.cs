﻿using Airlines.Business.Models.Airport;

namespace Airlines.Business.Models.Strategies.RouteSearchStrategy;
public interface IRouteSearchStrategy
{
    public string FindRoute(string departureAirport, string arrivalAirport, AirportGraph graph);
}
