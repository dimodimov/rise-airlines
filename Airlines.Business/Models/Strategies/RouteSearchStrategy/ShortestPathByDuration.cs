﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Airport;

namespace Airlines.Business.Models.Strategies.RouteSearchStrategy;
public class ShortestPathByDuration : IRouteSearchStrategy
{
    public string FindRoute(string departureAirport, string arrivalAirport, AirportGraph graph)
    {
        if (!graph.IsConnected(departureAirport, arrivalAirport))
        {
            throw new InvalidRouteException("There are no connecting flights to this route.");
        }

        if (departureAirport == arrivalAirport)
        {
            throw new InvalidRouteException("The departure airport matches the arrival airport.");
        }

        var parentMap = new Dictionary<string, (string?, double)>();
        var distances = new Dictionary<string, double>();

        foreach (var adjAirport in graph._adjacencyList.Keys)
        {
            parentMap[adjAirport] = (null, double.PositiveInfinity);
            distances[adjAirport] = double.PositiveInfinity;
        }

        distances[departureAirport] = 0;
        var minHeap = new SortedSet<(double, string)>
        {
            (0, departureAirport)
        };

        while (minHeap.Count > 0)
        {
            var (currentDistance, currentAirport) = minHeap.Min;
            _ = minHeap.Remove(minHeap.Min);

            foreach (var flight in graph._adjacencyList[currentAirport])
            {
                var neighbor = flight.ArrivalAirport;
                var newDistance = currentDistance + flight.Duration;

                if (newDistance < distances[neighbor])
                {
                    _ = minHeap.Remove((distances[neighbor], neighbor));
                    distances[neighbor] = newDistance;
                    parentMap[neighbor] = (currentAirport, flight.Duration);
                    _ = minHeap.Add((newDistance, neighbor));
                }
            }
        }

        var shortestPath = new List<string>();
        var airport = arrivalAirport;
        while (airport != null)
        {
            shortestPath.Insert(0, airport);
            airport = parentMap[airport].Item1;
        }

        return string.Join("->", shortestPath);
    }
}
