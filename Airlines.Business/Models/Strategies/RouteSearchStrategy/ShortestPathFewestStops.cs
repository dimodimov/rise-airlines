﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Airport;

namespace Airlines.Business.Models.Strategies.RouteSearchStrategy;
public class ShortestPathFewestStops : IRouteSearchStrategy
{
    public string FindRoute(string departureAirport, string arrivalAirport, AirportGraph graph)
    {
        if (!graph.IsConnected(departureAirport, arrivalAirport))
        {
            throw new InvalidRouteException("There are no connecting flights to this route.");
        }

        if (departureAirport == arrivalAirport)
        {
            throw new InvalidRouteException("The departure airport matches the arrival airport.");
        }

        var parentMap = new Dictionary<string, string>();
        var queue = new Queue<string>();

        queue.Enqueue(departureAirport);

#pragma warning disable CS8625 // Cannot convert null literal to non-nullable reference type.
        parentMap[departureAirport] = null;
#pragma warning restore CS8625 // Cannot convert null literal to non-nullable reference type.

        while (queue.Count > 0)
        {
            var currentAirport = queue.Dequeue();

            if (currentAirport == arrivalAirport)
            {
                break;
            }

            foreach (var flight in graph._adjacencyList[currentAirport])
            {
                var neighbor = flight.ArrivalAirport;
                if (!parentMap.ContainsKey(neighbor))
                {
                    queue.Enqueue(neighbor);
                    parentMap[neighbor] = currentAirport;
                }
            }
        }

        var shortestPath = new List<string>();
        var airport = arrivalAirport;
        while (airport != null)
        {
            shortestPath.Insert(0, airport);
            airport = parentMap[airport];
        }

        return string.Join("->", shortestPath);
    }
}
