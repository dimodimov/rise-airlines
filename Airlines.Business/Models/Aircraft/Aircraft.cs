﻿using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Business.Models.Aircraft;
public abstract class Aircraft(string model) : IAircraft
{
    public string Model { get; set; } = model;
}
