﻿using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Business.Models.Aircraft;
public class PrivateAircraft(string model, int seats) : Aircraft(model), IPrivateAircraft
{
    public int Seats { get; set; } = seats;
}
