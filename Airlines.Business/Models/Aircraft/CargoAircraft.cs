﻿using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Business.Models.Aircraft;
public class CargoAircraft(string model, int cargoWeight, double cargoVolume) : Aircraft(model), ICargoAircraft
{
    public int CargoWeight { get; set; } = cargoWeight;
    public double CargoVolume { get; set; } = cargoVolume;
}
