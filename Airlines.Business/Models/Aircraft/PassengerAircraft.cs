﻿using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Business.Models.Aircraft;
public class PassengerAircraft(
    string model,
    int cargoWeight,
    double cargoVolume,
    int seats
    ) : Aircraft(model), IPassengerAircraft
{
    public int CargoWeight { get; set; } = cargoWeight;
    public double CargoVolume { get; set; } = cargoVolume;
    public int Seats { get; set; } = seats;
}
