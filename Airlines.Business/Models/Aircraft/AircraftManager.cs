﻿using Airlines.Persistence.Basic.EntityInterfaces;
using Airlines.Persistence.Basic.Repositories;

namespace Airlines.Business.Models.Aircraft;
public class AircraftManager
{
    private readonly AircraftRepository _aircraftRepository = new();

    internal void AddAircraft(string model,
    string cargoWeight,
    string cargoVolume,
    string seats)
    {
        if (cargoWeight != "-" && cargoVolume != "-" && seats != "-")
        {
            var passengerAircraft = new PassengerAircraft(model, int.Parse(cargoWeight), double.Parse(cargoVolume), int.Parse(seats));
            _aircraftRepository.Add(passengerAircraft);
            return;
        }

        if (cargoWeight != "-" && cargoVolume != "-" && seats == "-")
        {
            var cargoAircraft = new CargoAircraft(model, int.Parse(cargoWeight), double.Parse(cargoVolume));
            _aircraftRepository.Add(cargoAircraft);
            return;
        }

        if (cargoWeight == "-" && cargoVolume == "-" && seats != "-")
        {
            var privateAircraft = new PrivateAircraft(model, int.Parse(seats));
            _aircraftRepository.Add(privateAircraft);
            return;
        }
    }

    public ICargoAircraft? GetCargoAircraftByModel(string model)
        => _aircraftRepository.GetCargoAircraftByModel(model);

    public IPassengerAircraft? GetPassengerAircraftByModel(string model)
        => _aircraftRepository.GetPassengerAircraftByModel(model);

    public IPrivateAircraft? GetPrivateAircraftByModel(string model)
    => _aircraftRepository.GetPrivateAircraftByModel(model);

    public void LoadRepository(string filePath)
    {
        var aircraftData = AircraftRepository.GetData(filePath);
        foreach (var aircraft in aircraftData)
        {
            AddAircraft(aircraft[0], aircraft[1], aircraft[2], aircraft[3]);
        }
    }
}
