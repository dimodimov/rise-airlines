﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.DTOs;
public class AirportDTO
{
    [Required(ErrorMessage = "Airport name is required.")]
    public required string Name { get; set; }

    [Required(ErrorMessage = "Airport country is required.")]
    public required string Country { get; set; }

    [Required(ErrorMessage = "Airport city is required.")]
    public required string City { get; set; }

    [Required(ErrorMessage = "Airport code is required.")]
    [StringLength(3, ErrorMessage = "Airport code must be exactly 3 letters.", MinimumLength = 3)]
    public required string Code { get; set; }

    [Required(ErrorMessage = "Airport runways field is required.")]
    [Range(0, int.MaxValue, ErrorMessage = "Runways Count must be a non-negative number.")]
    public required int RunwaysCount { get; set; }

    [Required(ErrorMessage = "Airport founded date is required.")]
    public required DateOnly Founded { get; set; }
}
