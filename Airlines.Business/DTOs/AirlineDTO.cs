﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.DTOs;
public class AirlineDTO
{
    public int? Id { get; set; }
    [Required(ErrorMessage = "Name field is required.")]
    public required string Name { get; set; }

    [Required(ErrorMessage = "Founded field is required.")]
    public required DateOnly Founded { get; set; }

    [Required(ErrorMessage = "Fleet Size field is required.")]
    [Range(0, int.MaxValue, ErrorMessage = "Fleet Size must be a non-negative number.")]
    public required int FleetSize { get; set; }

    [Required(ErrorMessage = "Description field is required.")]
    [StringLength(100, ErrorMessage = "Description cannot exceed 100 characters.")]
    public required string Description { get; set; }
}
