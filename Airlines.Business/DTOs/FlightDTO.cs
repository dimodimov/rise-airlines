﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.DTOs;
public class FlightDTO
{
    public int? Id { get; set; }

    [Required(ErrorMessage = "Flight number is required.")]
    public required string FlightNumber { get; set; }

    [Required(ErrorMessage = "From airport is required.")]
    [StringLength(3, ErrorMessage = "From airport code must be exactly 3 letters.", MinimumLength = 3)]
    public required string FromAirport { get; set; }

    [Required(ErrorMessage = "To airport is required.")]
    [StringLength(3, ErrorMessage = "To airport code must be exactly 3 letters.", MinimumLength = 3)]
    public required string ToAirport { get; set; }

    [Required(ErrorMessage = "Departure date and time is required.")]
    public required DateTime DepartureDateTime { get; set; }

    [Required(ErrorMessage = "Arrival date and time is required.")]
    public required DateTime ArrivalDateTime { get; set; }
}
