﻿
namespace Airlines.Business.Exceptions;
public class InvalidInputException(string message) : Exception(message)
{
}
