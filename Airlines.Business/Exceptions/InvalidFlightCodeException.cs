﻿
namespace Airlines.Business.Exceptions;
public class InvalidFlightCodeException(string message) : Exception(message)
{
}
