﻿
namespace Airlines.Business.Exceptions;
public class InvalidAirlineNameException(string message) : Exception(message)
{
}
