﻿namespace Airlines.Business.Exceptions;
public class DuplicateEntryException(string message) : Exception(message)
{
}
