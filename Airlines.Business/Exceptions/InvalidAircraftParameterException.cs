﻿namespace Airlines.Business.Exceptions;
public class InvalidAircraftParameterException(string message) : Exception(message)
{
}
