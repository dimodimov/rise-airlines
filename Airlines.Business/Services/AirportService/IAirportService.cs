﻿using Airlines.Business.DTOs;

namespace Airlines.Business.Services.AirportService;
public interface IAirportService
{
    Task<(List<AirportDTO>? airports, bool error)> GetAirportsAsync();
    Task<int> GetAirportsCountAsync();
    Task<List<AirportDTO>> GetAirportsByPageAsync(int itemsToSkip, int pageSize);
    Task<(int itemCount, List<AirportDTO> airports)> SearchAirportsByPageAsync(string searchString, int itemsToSkip, int pageSize);
    Task<List<AirportDTO>> GetAirportsByFilterAsync(string filter, string value);
    Task<(AirportDTO? airport, bool error)> GetAirportByCodeAsync(string code);
    Task<AirportDTO?> GetAirportByNameAsync(string name);
    Task<(AirportDTO? airport, bool error)> AddAirportAsync(AirportDTO airport);
    Task<bool> UpdateAirportRunwaysCountAsync(string code, int runways);
    Task<(AirportDTO? airport, bool error)> UpdateAirportAsync(AirportDTO airport);
    Task<(AirportDTO? airport, bool error)> DeleteAirportAsync(string code);
}
