﻿using Airlines.Business.DTOs;
using Airlines.Business.Exceptions;
using Airlines.Persistence.Repository;

namespace Airlines.Business.Services.AirportService;
public class AirportService(IAirportRepository airportRepository) : IAirportService
{
#pragma warning disable IDE1006 // Naming Styles
    private const int AIRPORT_CODE_LENGTH = 3;
#pragma warning restore IDE1006 // Naming Styles

    private readonly AutoMapper _mapper = new();
    private readonly IAirportRepository _airportRepository = airportRepository;

    public async Task<(List<AirportDTO>? airports, bool error)> GetAirportsAsync()
    {
        try
        {
            var airports = await _airportRepository.GetAirportsAsync();
            return (airports.Select(_mapper.MapAirport).ToList(), false);
        }
        catch (Exception)
        {
            return (null, true);
        }
    }

    public async Task<int> GetAirportsCountAsync() => await _airportRepository.GetAirportsCountAsync();

    public async Task<List<AirportDTO>> GetAirportsByPageAsync(int itemsToSkip, int pageSize)
    {
        var airports = await _airportRepository.GetAirportsByPageAsync(itemsToSkip, pageSize);
        return airports.Select(_mapper.MapAirport).ToList();
    }

    public async Task<(int itemCount, List<AirportDTO> airports)> SearchAirportsByPageAsync(string searchString, int itemsToSkip, int pageSize)
    {
        var (itemCount, airports) = await _airportRepository.SearchAirportsByPageAsync(searchString, itemsToSkip, pageSize);
        var result = (itemCount, airports.Select(_mapper.MapAirport).ToList());

        return result;
    }

    public async Task<List<AirportDTO>> GetAirportsByFilterAsync(string filter, string value)
    {
        if (filter is not "Name" and not "Country" and not "City" and not "Code")
            throw new ArgumentException("Invalid filter. Choose from Name, Country City and Code.");
        var airports = await _airportRepository.GetAirportsByFilterAsync(filter, value);

        return airports.Select(_mapper.MapAirport).ToList();
    }

    public async Task<(AirportDTO? airport, bool error)> GetAirportByCodeAsync(string code)
    {
        try
        {
            var airport = await _airportRepository.GetAirportByCodeAsync(code);
            return airport != null ? (_mapper.MapAirport(airport), false) : (null, false);
        }
        catch (Exception)
        {
            return (null, true);
        }
    }

    public async Task<AirportDTO?> GetAirportByNameAsync(string name)
    {
        var airport = await _airportRepository.GetAirportByNameAsync(name);
        return airport != null ? _mapper.MapAirport(airport) : null;
    }

    public async Task<(AirportDTO? airport, bool error)> AddAirportAsync(AirportDTO airport)
    {
        try
        {
            if (airport.Code.Length != AIRPORT_CODE_LENGTH || airport.RunwaysCount < 0)
            {
                return (null, false);
            }

            var result = await _airportRepository.AddAirportAsync(_mapper.MapAirport(airport));

            if (result != null)
            {
                return (_mapper.MapAirport(result), false);
            }

            return (null, false);
        }
        catch (Exception)
        {
            return (null, true);
        }
    }

    public async Task<bool> UpdateAirportRunwaysCountAsync(string code, int runways)
    {
        if (code.Length != AIRPORT_CODE_LENGTH)
            throw new InvalidAirportException($"The airport code must be exactly {AIRPORT_CODE_LENGTH} letters long.");

        if (runways < 0)
            throw new ArgumentException("Runways can't be negative.");

        return await _airportRepository.UpdateAirportRunwaysCountAsync(code, runways);
    }

    public async Task<(AirportDTO? airport, bool error)> UpdateAirportAsync(AirportDTO airport)
    {
        try
        {
            if (airport.Code.Length != AIRPORT_CODE_LENGTH || airport.RunwaysCount < 0)
            {
                return (null, false);
            }

            var result = await _airportRepository.UpdateAirportAsync(_mapper.MapAirport(airport));

            if (result != null)
            {
                return (airport, false);
            }

            return (null, false);
        }
        catch (Exception)
        {
            return (null, true);
        }

    }

    public async Task<(AirportDTO? airport, bool error)> DeleteAirportAsync(string code)
    {
        try
        {
            var result = await _airportRepository.DeleteAirportAsync(code);

            if (result != null)
            {
                return (_mapper.MapAirport(result), false);
            }

            return (null, false);
        }
        catch (Exception)
        {
            return (null, true);
        }
    }
}
