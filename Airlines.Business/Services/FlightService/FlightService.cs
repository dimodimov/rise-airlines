﻿using Airlines.Business.DTOs;
using Airlines.Business.Utilities;
using Airlines.Persistence.Repository;

namespace Airlines.Business.Services.FlightService;
public class FlightService(IFlightRepository flightRepository) : IFlightService
{
    private readonly AutoMapper _mapper = new();
    private readonly IFlightRepository _flightRepository = flightRepository;

    public async Task<(List<FlightDTO>? flights, bool error)> GetFlightsAsync()
    {
        try
        {
            var flights = await _flightRepository.GetFlightsAsync();

            return (flights.Select(_mapper.MapFlight).ToList(), false);
        }
        catch (Exception)
        {
            return (null, true);
        }
    }

    public async Task<int> GetFlightCountAsync()
    {
        var flights = await _flightRepository.GetFlightCountAsync();

        return flights;
    }

    public async Task<(int itemCount, List<FlightDTO> flights)> SearchFlightsAsync(string searchString, int itemsToSkip, int pageSize)
    {
        var (itemCount, flights) = await _flightRepository.SearchFlightsAsync(searchString, itemsToSkip, pageSize);
        var mappedFlights = flights.Select(_mapper.MapFlight).ToList();
        var result = (itemCount, mappedFlights);

        return result;
    }

    public async Task<(int itemCount, List<FlightDTO> flights)> SearchAndFilterFlightsAsync(string searchString, DateTime today, DateTime endDate, int itemsToSkip, int pageSize)
    {
        var (itemCount, flights) = await _flightRepository.SearchAndFiterFlightsAsync(searchString, today, endDate, itemsToSkip, pageSize);
        var mappedFlights = flights.Select(_mapper.MapFlight).ToList();
        var result = (itemCount, mappedFlights);

        return result;
    }

    public async Task<(int itemCount, List<FlightDTO> flights)> GetFlightsByDateRangeAsync(DateTime today, DateTime endDate, int itemsToSkip, int pageSize)
    {
        var (itemCount, flights) = await _flightRepository.GetFlightsByDateRangeAsync(today, endDate, itemsToSkip, pageSize);

        var mappedFlights = flights.Select(_mapper.MapFlight).ToList();
        var result = (itemCount, mappedFlights);

        return result;
    }

    public async Task<List<FlightDTO>> GetFlightsByPageAsync(int itemsToSkip, int itesmPerPage)
    {
        var flights = await _flightRepository.GetFlightsByPageAsync(itemsToSkip, itesmPerPage);

        return flights.Select(_mapper.MapFlight).ToList();
    }

    public async Task<List<FlightDTO>> GetFlightsByFilterAsync(string filter, string value)
    {
        if (filter is not "FlightNumber" and not "FromAirport" and not "ToAirport")
            throw new ArgumentException("Invalid filter. Choose from FlightNumber, FromAirport and ToAirport.");
        var flights = await _flightRepository.GetFlightsByFilterAsync(filter, value);

        return flights.Select(_mapper.MapFlight).ToList();
    }

    public async Task<List<FlightDTO>> GetFlightsByFilterAsync(string filter, int value)
    {
        if (filter != "Id")
            throw new ArgumentException("Invalid filter.");

        var flights = await _flightRepository.GetFlightsByFilterAsync(filter, value);

        return flights.Select(_mapper.MapFlight).ToList();
    }

    public async Task<List<FlightDTO>> GetFlightsByFilterAsync(string filter, DateTime value)
    {
        if (filter is not "DepartureDateTime" and not "ArrivalDateTime")
            throw new ArgumentException("Invalid filter. Choose from DepartureDateTime and ArrivalDateTime.");

        var flights = await _flightRepository.GetFlightsByFilterAsync(filter, value);

        return flights.Select(_mapper.MapFlight).ToList();
    }

    public async Task<(FlightDTO? flight, bool error)> GetFlightByIdAsync(int id)
    {
        try
        {
            var flight = await _flightRepository.GetFlightByIdAsync(id);

            return flight != null ? (_mapper.MapFlight(flight), false) : (null, false);
        }
        catch (Exception)
        {
            return (null, true);
        }
    }

    public async Task<FlightDTO?> GetFlightByNumberAsync(string flightNumber)
    {
        var flight = await _flightRepository.GetFlightByNumberAsync(flightNumber);

        return flight != null ? _mapper.MapFlight(flight) : null;
    }

    public async Task<(FlightDTO? flight, bool error)> AddFlightAsync(FlightDTO flightDTO)
    {
        try
        {
            if (!DataValidator.IsNotInPastDateTime(flightDTO.DepartureDateTime))
            {
                return (null, false);
            }

            if (!DataValidator.IsNotInPastDateTime(flightDTO.ArrivalDateTime))
            {
                return (null, false);
            }

            if (!DataValidator.ArrivalIsAfterDeparture(flightDTO.DepartureDateTime, flightDTO.ArrivalDateTime))
            {
                return (null, false);
            }

            var flight = _mapper.MapFlight(flightDTO);
            var result = await _flightRepository.AddFlightAsync(flight);

            if (result != null)
            {
                return (_mapper.MapFlight(result), false);
            }

            return (null, false);
        }
        catch (Exception)
        {
            return (null, true);
        }
    }

    public async Task<(FlightDTO? flight, bool error)> UpdateFlightAsync(FlightDTO flightDTO)
    {
        try
        {
            if (!DataValidator.IsNotInPastDateTime(flightDTO.DepartureDateTime))
            {
                return (null, false);
            }

            if (!DataValidator.IsNotInPastDateTime(flightDTO.ArrivalDateTime))
            {
                return (null, false);
            }

            if (!DataValidator.ArrivalIsAfterDeparture(flightDTO.DepartureDateTime, flightDTO.ArrivalDateTime))
            {
                return (null, false);
            }

            var flight = _mapper.MapFlight(flightDTO);
            var result = await _flightRepository.UpdateFlightAsync(flight);

            if (result != null)
            {
                return (flightDTO, false);
            }

            return (null, false);
        }
        catch (Exception)
        {
            return (null, true);
        }
    }

    public async Task<bool> UpdateFlightDepartureAndArrivalTimeAsync(string flightNumber, DateTime departureTime, DateTime arrivalTime)
    {
        if (departureTime < DateTime.Now)
            throw new ArgumentException("Departure time cannot be in the past.");

        if (arrivalTime < DateTime.Now)
            throw new ArgumentException("Arrival time cannot be in the past.");

        if (arrivalTime < departureTime)
            throw new ArgumentException("Arrival time cannot be before departure time.");

        return await _flightRepository.UpdateFlightDepartureAndArrivalTimeAsync(flightNumber, departureTime, arrivalTime);
    }

    public async Task<(FlightDTO? flight, bool error)> DeleteFlightAsync(int id)
    {
        try
        {
            var result = await _flightRepository.DeleteFlightAsync(id);

            if (result != null)
            {
                return (_mapper.MapFlight(result), false);
            }

            return (null, false);
        }
        catch (Exception)
        {
            return (null, true);
        }
    }
}
