﻿using Airlines.Business.DTOs;

namespace Airlines.Business.Services.FlightService;
public interface IFlightService
{
    Task<(List<FlightDTO>? flights, bool error)> GetFlightsAsync();
    Task<int> GetFlightCountAsync();
    Task<(int itemCount, List<FlightDTO> flights)> SearchFlightsAsync(string searchString, int itemsToSkip, int pageSize);
    Task<(int itemCount, List<FlightDTO> flights)> GetFlightsByDateRangeAsync(DateTime today, DateTime endDate, int itemsToSkip, int pageSize);
    Task<(int itemCount, List<FlightDTO> flights)> SearchAndFilterFlightsAsync(string searchString, DateTime today, DateTime endDate, int itemsToSkip, int pageSize);
    Task<List<FlightDTO>> GetFlightsByPageAsync(int itemsToSkip, int itesmPerPage);
    Task<List<FlightDTO>> GetFlightsByFilterAsync(string filter, string value);
    Task<List<FlightDTO>> GetFlightsByFilterAsync(string filter, int value);
    Task<List<FlightDTO>> GetFlightsByFilterAsync(string filter, DateTime value);
    Task<(FlightDTO? flight, bool error)> GetFlightByIdAsync(int id);
    Task<FlightDTO?> GetFlightByNumberAsync(string flightNumber);
    Task<(FlightDTO? flight, bool error)> AddFlightAsync(FlightDTO flightDTO);
    Task<(FlightDTO? flight, bool error)> UpdateFlightAsync(FlightDTO flightDTO);
    Task<bool> UpdateFlightDepartureAndArrivalTimeAsync(string flightNumber, DateTime departureTime, DateTime arrivalTime);
    Task<(FlightDTO? flight, bool error)> DeleteFlightAsync(int id);
}
