﻿using Airlines.Business.DTOs;

namespace Airlines.Business.Services.AirlineService;
public interface IAirlineService
{
    Task<(List<AirlineDTO>? airlines, bool error)> GetAirlinesAsync();
    Task<int> GetAirlinesCountAsync();
    Task<List<AirlineDTO>> GetAirlinesByPageAsync(int itemsToSkip, int pageSize);
    Task<(int itemCount, List<AirlineDTO> airlines)> SearchAirlinesByPageAsync(string searchString, int itemsToSkip, int pageSize);
    Task<List<AirlineDTO>> GetAirlinesByFilterAsync(string filter, string value);
    Task<(AirlineDTO? airline, bool error)> GetAirlineByIdAsync(int id);
    Task<AirlineDTO?> GetAirlineByNameAsync(string name);
    Task<(AirlineDTO? airline, bool error)> AddAirlineAsync(AirlineDTO airline);
    Task<bool> UpdateAirlineFleetSizeAsync(int id, int fleetSize);
    Task<(AirlineDTO? airline, bool error)> UpdateAirlineAsync(AirlineDTO airline);
    Task<(AirlineDTO? airline, bool error)> DeleteAirlineAsync(int id);
}
