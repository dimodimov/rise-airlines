﻿using Airlines.Business.DTOs;
using Airlines.Business.Exceptions;
using Airlines.Persistence.Repository;

namespace Airlines.Business.Services.AirlineService;
public class AirlineService(IAirlineRepository airlineRepository) : IAirlineService
{
    private readonly AutoMapper _mapper = new();
    private readonly IAirlineRepository _airlineRepository = airlineRepository;

    public async Task<(List<AirlineDTO>? airlines, bool error)> GetAirlinesAsync()
    {
        try
        {
            var airlines = await _airlineRepository.GetAirlinesAsync();

            return (airlines.Select(_mapper.MapAirline).ToList(), false);
        }
        catch (Exception)
        {
            return (null, true);
        }
    }

    public async Task<int> GetAirlinesCountAsync() => await _airlineRepository.GetAirlinesCountAsync();

    public async Task<List<AirlineDTO>> GetAirlinesByPageAsync(int itemsToSkip, int pageSize)
    {
        var airlines = await _airlineRepository.GetAirlinesByPageAsync(itemsToSkip, pageSize);
        return airlines.Select(_mapper.MapAirline).ToList();
    }

    public async Task<(int itemCount, List<AirlineDTO> airlines)> SearchAirlinesByPageAsync(string searchString, int itemsToSkip, int pageSize)
    {
        var (itemCount, airlines) = await _airlineRepository.SearchAirlinesByPageAsync(searchString, itemsToSkip, pageSize);
        var result = (itemCount, airlines.Select(_mapper.MapAirline).ToList());

        return result;
    }

    public async Task<List<AirlineDTO>> GetAirlinesByFilterAsync(string filter, string value)
    {
        if (filter is not "Name" and not "Description")
            throw new InvalidInputException("Valid filters are Name and Description");
        var airlines = await _airlineRepository.GetAirlinesByFilterAsync(filter, value);

        return airlines.Select(_mapper.MapAirline).ToList();
    }

    public async Task<(AirlineDTO? airline, bool error)> GetAirlineByIdAsync(int id)
    {
        try
        {
            var airline = await _airlineRepository.GetAirlineByIdAsync(id);

            return airline != null ? (_mapper.MapAirline(airline), false) : (null, false);
        }
        catch (Exception)
        {
            return (null, true);
        }
    }

    public async Task<AirlineDTO?> GetAirlineByNameAsync(string name)
    {
        var airline = await _airlineRepository.GetAirlineByNameAsync(name);
        return airline != null ? _mapper.MapAirline(airline) : null;
    }

    public async Task<(AirlineDTO? airline, bool error)> AddAirlineAsync(AirlineDTO airline)
    {
        try
        {
            if (airline.FleetSize < 0)
            {
                return (null, false);
            }

            var result = await _airlineRepository.AddAirlineAsync(_mapper.MapAirline(airline));

            if (result != null)
            {
                return (_mapper.MapAirline(result), false);
            }

            return (null, false);
        }
        catch (Exception)
        {
            return (null, true);
        }
    }

    public async Task<bool> UpdateAirlineFleetSizeAsync(int id, int fleetSize)
    {
        if (fleetSize < 0)
            throw new ArgumentException("FleetSize can't be negative");

        return await _airlineRepository.UpdateAirlineFleetSizeAsync(id, fleetSize);
    }

    public async Task<(AirlineDTO? airline, bool error)> UpdateAirlineAsync(AirlineDTO airline)
    {
        try
        {
            if (airline.FleetSize < 0)
            {
                return (null, false);
            }

            var result = await _airlineRepository.UpdateAirlineAsync(_mapper.MapAirline(airline));

            if (result != null)
            {
                return (airline, false);
            }

            return (null, false);
        }
        catch (Exception)
        {
            return (null, true);
        }
    }

    public async Task<(AirlineDTO? airline, bool error)> DeleteAirlineAsync(int id)
    {
        try
        {
            var result = await _airlineRepository.DeleteAirlineAsync(id);

            if (result != null)
            {
                return (_mapper.MapAirline(result), false);
            }

            return (null, false);
        }
        catch (Exception)
        {
            return (null, true);
        }
    }
}
