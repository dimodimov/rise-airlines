﻿using AutoMapper;
using Airlines.Persistence.Entities;
using Airlines.Business.DTOs;

namespace Airlines.Business;
public class AutoMapper
{
    private readonly Mapper _mapper;
    public AutoMapper()
    {
        var config = new MapperConfiguration(cfg =>
        {
            _ = cfg.CreateMap<Airline, AirlineDTO>();
            _ = cfg.CreateMap<AirlineDTO, Airline>();
            _ = cfg.CreateMap<Airport, AirportDTO>();
            _ = cfg.CreateMap<AirportDTO, Airport>();
            _ = cfg.CreateMap<Flight, FlightDTO>();
            _ = cfg.CreateMap<FlightDTO, Flight>();
        });

        _mapper = new Mapper(config);
    }

    public Airline MapAirline(AirlineDTO airlineDTO) => _mapper.Map<Airline>(airlineDTO);
    public AirlineDTO MapAirline(Airline airline) => _mapper.Map<AirlineDTO>(airline);
    public Airport MapAirport(AirportDTO airportDTO) => _mapper.Map<Airport>(airportDTO);
    public AirportDTO MapAirport(Airport airport) => _mapper.Map<AirportDTO>(airport);
    public Flight MapFlight(FlightDTO flightDTO) => _mapper.Map<Flight>(flightDTO);
    public FlightDTO MapFlight(Flight flight) => _mapper.Map<FlightDTO>(flight);
}
