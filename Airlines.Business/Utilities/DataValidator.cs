﻿// Ignore Spelling: Validator

namespace Airlines.Business.Utilities;
public static class DataValidator
{
    internal static bool IsAlphanumeric(string input)
    {
        foreach (var c in input)
        {
            if (!char.IsLetterOrDigit(c))
            {
                return false;
            }
        }

        return true;
    }

    internal static bool IsLettersOnly(string input)
    {
        foreach (var c in input)
        {
            if (!char.IsLetter(c))
            {
                return false;
            }
        }

        return true;
    }

    internal static bool IsLettersOrSpaceOnly(string input)
    {
        foreach (var c in input)
        {
            if (!char.IsLetter(c) && c != ' ')
            {
                return false;
            }
        }

        return true;
    }

    internal static bool IsNotInPastDateTime(DateTime time)
    {
        if (time < DateTime.Now)
        {
            return false;
        }

        return true;
    }

    internal static bool ArrivalIsAfterDeparture(DateTime departure, DateTime arrival)
    {
        if (arrival < departure)
        {
            return false;
        }

        return true;
    }
}
