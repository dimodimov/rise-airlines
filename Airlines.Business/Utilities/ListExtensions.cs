﻿namespace Airlines.Business.Utilities;
public static class ListExtensions
{
    internal static void BubbleSort(this List<string> list, string sortingOrder = "ascending")
    {
        if (!string.Equals(sortingOrder.ToLower(), "ascending") && !string.Equals(sortingOrder.ToLower(), "descending"))
        {
            Console.WriteLine("Invalid sort order. Choose between ascending and descending");
            return;
        }

        for (var i = 0; i < list.Count - 1; i++)
            for (var j = 0; j < list.Count - i - 1; j++)
            {
                if (string.Equals(sortingOrder.ToLower(), "ascending"))
                    if (list[j].CompareTo(list[j + 1]) > 0)
                        (list[j], list[j + 1]) = (list[j + 1], list[j]);

                if (string.Equals(sortingOrder.ToLower(), "descending"))
                    if (list[j].CompareTo(list[j + 1]) < 0)
                        (list[j], list[j + 1]) = (list[j + 1], list[j]);
            }
    }

    internal static void SelectionSort(this List<string> list, string sortingOrder = "ascending")
    {
        if (!string.Equals(sortingOrder.ToLower(), "ascending") && !string.Equals(sortingOrder.ToLower(), "descending"))
        {
            Console.WriteLine("Invalid sort order. Choose between ascending for ascending and desc for descending");
            return;
        }

        for (var i = 0; i < list.Count - 1; i++)
        {
            var sortIndex = i;

            for (var j = i + 1; j < list.Count; j++)
            {
                if (string.Equals(sortingOrder.ToLower(), "ascending"))
                    if (list[j].CompareTo(list[sortIndex]) < 0)
                        sortIndex = j;

                if (string.Equals(sortingOrder.ToLower(), "descending"))
                    if (list[j].CompareTo(list[sortIndex]) > 0)
                        sortIndex = j;
            }

            if (sortIndex != i)
                (list[i], list[sortIndex]) = (list[sortIndex], list[i]);
        }
    }

    internal static string? LinearSearch(this List<string> list, string target)
    {
        for (var i = 0; i < list.Count; i++)
            if (list[i] == target)
                return list[i];
        return null;
    }

    internal static string? BinarySearchCustom(this List<string> sortedList, string target)
    {
        var leftIndex = 0;
        var rightIndex = sortedList.Count - 1;

        while (leftIndex <= rightIndex)
        {
            var mid = (leftIndex + rightIndex) / 2;

            if (target.CompareTo(sortedList[mid]) == 0)
                return target;

            if (target.CompareTo(sortedList[mid]) < 0)
                rightIndex = mid - 1;
            else
                leftIndex = mid + 1;
        }

        return null;
    }
}
