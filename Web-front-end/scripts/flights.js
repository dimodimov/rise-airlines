import { fetchAll } from './api/httpRequests.js';
import { createFlightTableRow, showToast } from './Utilities/createDomElements.js';
import { handleAddFlight } from './Components/submitForm.js';
import { ENDPOINTS } from './Api/config.js';

const tbody = document.querySelector("#main-table tbody");

document.addEventListener("DOMContentLoaded", () => {
document.getElementById("submit-entity-form").addEventListener("submit", handleAddFlight);

  fetchAll(ENDPOINTS.FLIGHTS)
    .then((flights) => {
        flights.forEach((flight) => {
        const row = createFlightTableRow(flight);
        tbody.appendChild(row);
      });
    })
    .catch((e) => {
      showToast("Failed to get flights!");
      console.error(e.message)
    });
});
