import { fetchAll } from './api/httpRequests.js';
import { createAirlineTableRow, showToast } from './Utilities/createDomElements.js';
import { handleAddAirline } from './Components/submitForm.js';
import { ENDPOINTS } from './Api/config.js';

const tbody = document.querySelector("#main-table tbody");

document.addEventListener("DOMContentLoaded", () => {
document.getElementById("submit-entity-form").addEventListener("submit", handleAddAirline);

  fetchAll(ENDPOINTS.AIRLINES)
    .then((airlines) => {
      airlines.forEach((airline) => {
        const row = createAirlineTableRow(airline);
        tbody.appendChild(row);
      });
    })
    .catch((e) => {
      showToast("Failed to get airlines!");
      console.error(e.message)
    });
});
