export const API_BASE_URL = "https://localhost:7110/api";

export const ENDPOINTS = {
    AIRLINES: `${API_BASE_URL}/Airlines`,
    AIRPORTS: `${API_BASE_URL}/Airports`,
    FLIGHTS: `${API_BASE_URL}/Flights`,
};
