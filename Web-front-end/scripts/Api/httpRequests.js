export const fetchAll = async (endpoint) => {
  try {
    const response = await fetch(endpoint);

    if (!response.ok) {
      throw new Error("Failed to fetch data");
    }

    return await response.json();
  } catch (error) {
    console.error("Error fetching data:", error);
  }
};

export const fetchOne = async (endpoint, id) => {
  try {
    const response = await fetch(`${endpoint}/${id}`);

    if (!response.ok) {
      throw new Error("Failed to fetch data");
    }

    return await response.json();
  } catch (error) {
    console.error("Error fetching entity:", error);
  }
};

export const add = async (endpoint, entity) => {
  try {
    const response = await fetch(endpoint, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(entity),
    });

    if (!response.ok) {
      throw new Error("Failed to add");
    }

    return await response.json();
  } catch (error) {
    console.error("Error adding entity:", error);
  }
};

export const update = async (endpoint, entity) => {
  try {
    const response = await fetch(endpoint, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(entity),
    });

    if (!response.ok) {
      throw new Error("Failed to update");
    }

    return await response.json();
  } catch (error) {
    console.error("Error updating entity:", error);
  }
};

export const deleteOne = async (endpoint, id) => {
  try {
    const response = await fetch(`${endpoint}/${id}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    });

    if (!response.ok) {
      throw new Error("Failed to delete");
    }

    return response.json();
  } catch (error) {
    console.error("Error deleting entity:", error);
  }
};
