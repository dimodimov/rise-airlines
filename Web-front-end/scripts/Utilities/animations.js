document.addEventListener("DOMContentLoaded", () => {
    const tableContainer = document.querySelector(".table-container");
    const table = document.getElementById("main-table");
    const tableRows = table.getElementsByTagName("tr");
    const toggleTableButton = document.getElementById("toggle-table-button");
    const toggleTableItemsButton = document.getElementById("toggle-table-items-button");
    const goToTopButton = document.getElementById("go-to-top-button");
    const toggleFormButton = document.getElementById("show-form-button");
    const submitEntityForm = document.querySelector("#submit-entity-form");

    // Initial Fade in effect
    let contentOpacity = 0;

    const fadeIn = setInterval(() => {
        table.style.opacity = contentOpacity;
        contentOpacity += 0.01;
        if (contentOpacity >= 1) {
            clearInterval(fadeIn);
        }
    }, 10);

    // Toggle Table Show/Hide
    let isHiddenTable = false;

    toggleTableButton.addEventListener("click", () => {
        if (isHiddenTable) {
            table.style.display = "table";
            toggleTableButton.textContent = "Hide Table";
        } else {
            table.style.display = "none";
            toggleTableButton.textContent = "Show Table";
        }

        isHiddenTable = !isHiddenTable;
    });

    // Toggle Table Items Show/Hide
    let isPartiallyHiddenTable = false;

    toggleTableItemsButton.addEventListener("click", () => {
        for (let i = 4; i < tableRows.length; i++) {
            if (isPartiallyHiddenTable) {
                tableRows[i].style.display = "table-row";
            } else {
                tableRows[i].style.display = "none";
            }
        }

        if (isPartiallyHiddenTable) {
            toggleTableItemsButton.textContent = "Hide extra rows";
            goToTopButton.style.display = "block";
        } else {
            toggleTableItemsButton.textContent = "Show extra rows";
            goToTopButton.style.display = "none";
        }

        isPartiallyHiddenTable = !isPartiallyHiddenTable;
    })

    // Scroll To Top Button
    goToTopButton.addEventListener("click", function () {
        tableContainer.scrollTo({
            top: 0,
            behavior: "smooth",
        });
    });

    // if (table.offsetHeight > tableContainer.offsetHeight) {
    //     goToTopButton.style.display = "block";
    // } else {
    //     goToTopButton.style.display = "none";
    // }

    // Toggle Form Show/Hide
    let isHiddenForm = false;

    toggleFormButton.addEventListener("click", () => {
        if (isHiddenForm) {
            submitEntityForm.style.display = "block";
            toggleFormButton.textContent = "Hide Form";
        } else {
            submitEntityForm.style.display = "none";
            toggleFormButton.textContent = "Show Form";
        }

        isHiddenForm = !isHiddenForm;
    });
});