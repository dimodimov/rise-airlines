import { ENDPOINTS } from '../Api/config.js';
import { openDeleteModal } from './modals/deleteModal.js';
import { handleAirlineEditFormSubmit, handleAirportEditFormSubmit, openEditModal, populateAirlineEditForm, populateAirportEditForm, populateFlightEditForm, handleFlightEditFormSubmit } from './modals/editModal.js';

export const createAirlineTableRow = (airline) => {
  const row = document.createElement("tr");
  row.setAttribute('data-id', airline.id);
  appendTableCellWithText(row, airline.name);
  appendTableCellWithText(row, airline.founded);
  appendTableCellWithText(row, airline.fleetSize);
  appendTableCellWithText(row, airline.description);

  const editButton = createEditButton(airline.id, ENDPOINTS.AIRLINES, populateAirlineEditForm, handleAirlineEditFormSubmit);
  appendTableCellWithButton(row, editButton);

  const deleteButton = createDeleteButton(airline.id, ENDPOINTS.AIRLINES);
  appendTableCellWithButton(row, deleteButton);

  return row;
};

export const createAirportTableRow = (airport) => {
  const row = document.createElement("tr");
  row.setAttribute('data-id', airport.code);
  appendTableCellWithText(row, airport.name);
  appendTableCellWithText(row, airport.country);
  appendTableCellWithText(row, airport.city);
  appendTableCellWithText(row, airport.code);
  appendTableCellWithText(row, airport.runwaysCount);
  appendTableCellWithText(row, airport.founded);

  const editButton = createEditButton(airport.code, ENDPOINTS.AIRPORTS, populateAirportEditForm, handleAirportEditFormSubmit);
  appendTableCellWithButton(row, editButton);

  const deleteButton = createDeleteButton(airport.code, ENDPOINTS.AIRPORTS);
  appendTableCellWithButton(row, deleteButton);

  return row;
};

export const createFlightTableRow = (flight) => {
  const row = document.createElement("tr");
  row.setAttribute('data-id', flight.id);
  appendTableCellWithText(row, flight.flightNumber);
  appendTableCellWithText(row, flight.fromAirport);
  appendTableCellWithText(row, flight.toAirport);
  appendTableCellWithText(row, flight.departureDateTime);
  appendTableCellWithText(row, flight.arrivalDateTime);

  const editButton = createEditButton(flight.id, ENDPOINTS.FLIGHTS, populateFlightEditForm, handleFlightEditFormSubmit);
  appendTableCellWithButton(row, editButton);

  const deleteButton = createDeleteButton(flight.id, ENDPOINTS.FLIGHTS);
  appendTableCellWithButton(row, deleteButton);

  return row;
};

const createEditButton = (id, endpoint, populateEditForm, handleEditFormSubmit) => {
  const editButton = document.createElement("button");
  editButton.className = "orange-button";
  editButton.textContent = "Edit";
  editButton.addEventListener("click", () => openEditModal(id, endpoint, populateEditForm, handleEditFormSubmit));
  return editButton;
};

const createDeleteButton = (entryId, endpoint) => {
  const deleteButton = document.createElement("button");
  deleteButton.textContent = "Delete";
  deleteButton.addEventListener("click", () => openDeleteModal(entryId, endpoint));
  return deleteButton;
};

const appendTableCellWithText = (row, text) => {
  const cell = document.createElement("td");
  cell.textContent = text;
  row.appendChild(cell);
};

const appendTableCellWithButton = (row, button) => {
  const cell = document.createElement("td");
  cell.appendChild(button);
  row.appendChild(cell);
};

export const showToast = (message) => {
  const toast = document.getElementById('toast');
  const toastMessage = document.getElementById('toast-message');

  toastMessage.textContent = message;

  toast.className = 'show';

  setTimeout(() => {
      toast.className = toast.className.replace('show', '');
      toastMessage.textContent = '';
  }, 2000);
}