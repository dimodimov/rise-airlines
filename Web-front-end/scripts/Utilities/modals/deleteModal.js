import { openModal, closeModal } from "./modalFunctions.js";
import { deleteOne } from "../../api/httpRequests.js";
import { showToast } from "../createDomElements.js";

export const openDeleteModal = (id, endpoint) => {
  const deleteModal = document.getElementById("deleteModal");
  openModal(deleteModal);

  const deleteButton = deleteModal.querySelector("#delete-entry-button");
  deleteButton.addEventListener("click", () => handleDelete(id, endpoint));

  const cancelButton = deleteModal.querySelector("#close-modal-button");
  cancelButton.addEventListener("click", () => closeModal(deleteModal));
};

const handleDelete = (id, endpoint) => {
  deleteOne(endpoint, id)
    .then(() => {
      const row = document.querySelector(
        `#main-table tbody tr[data-id="${id}"]`
      );
      if (row) {
        row.remove();
      }
    })
    .catch((e) => {
      showToast("Failed to delete element");
      console.error(e.message);
    });

  closeModal(deleteModal);
};
