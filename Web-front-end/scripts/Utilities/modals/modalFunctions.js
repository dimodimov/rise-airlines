export const openModal = (modal) => {
  modal.style.display = "block";
  const closeButton = modal.querySelector(".close");
  closeButton.addEventListener("click", () => closeModal(modal));
  document.addEventListener("keydown", (event) =>
    handleEscapeKeyPress(event, modal)
  );
  document.addEventListener("mousedown", (event) =>
    handleOutsideClick(event, modal)
  );
  document.body.style.overflow = "hidden";
};

export const closeModal = (modal) => {
  modal.style.display = "none";
  document.body.style.overflow = "auto";

  document.removeEventListener("keydown", (event) =>
    handleEscapeKeyPress(event, modal)
  );
  document.removeEventListener("mousedown", (event) =>
    handleOutsideClick(event, modal)
  );
};

const handleEscapeKeyPress = (event, modal) => {
  if (event.key === "Escape") {
    closeModal(modal);
  }
};

const handleOutsideClick = (event, modal) => {
  if (event.target == modal) {
    closeModal(modal);
  }
};
