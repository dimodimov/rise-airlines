import { ENDPOINTS } from "../../Api/config.js";
import { fetchOne, update } from "../../api/httpRequests.js";
import { openModal, closeModal } from "./modalFunctions.js";
import { showToast } from "../createDomElements.js";

// Airlines
const editAirlineNameInput = document.getElementById("editName");
const editAirlineFoundedInput = document.getElementById("editFounded");
const editAirlineFleetSizeInput = document.getElementById("editFleetSize");
const editAirlineDescriptionInput = document.getElementById("editDescription");

// Airports
const editAirportNameInput = document.getElementById("editAirportName");
const editAirportCountryInput = document.getElementById("editCountry");
const editAirportCityInput = document.getElementById("editCity");
const editAirportCodeInput = document.getElementById("editCode");
const editAirportRunwaysInput = document.getElementById("editRunways");
const editAirportFoundedInput = document.getElementById("editAirportFounded");

// Flights
const editFlightNumberInput = document.getElementById("editFlightNumber");
const editFlightFromInput = document.getElementById("editFromAirport");
const editFlightToInput = document.getElementById("editToAirport");
const editFlightDepartureInput = document.getElementById(
  "editDepartureDatetime"
);
const editFlightArrivalInput = document.getElementById("editArrivalDatetime");

export const openEditModal = (
  id,
  endpoint,
  populateEditForm,
  handleEditFormSubmit
) => {
  fetchOne(endpoint, id)
    .then((entity) => populateEditForm(entity))
    .catch(console.error);

  const editModal = document.getElementById("editModal");
  openModal(editModal);

  const editForm = document.getElementById("editForm");
  editForm.addEventListener("submit", (event) =>
    handleEditFormSubmit(event, editModal, id)
  );
};

export const populateAirlineEditForm = (airline) => {
  editAirlineNameInput.value = airline.name;
  editAirlineFoundedInput.value = airline.founded;
  editAirlineFleetSizeInput.value = airline.fleetSize;
  editAirlineDescriptionInput.value = airline.description;
};

export const populateFlightEditForm = (flight) => {
  editFlightNumberInput.value = flight.flightNumber;
  editFlightFromInput.value = flight.fromAirport;
  editFlightToInput.value = flight.toAirport;
  editFlightDepartureInput.value = flight.departureDateTime;
  editFlightArrivalInput.value = flight.arrivalDateTime;
};

export const populateAirportEditForm = (airport) => {
  editAirportNameInput.value = airport.name;
  editAirportCountryInput.value = airport.country;
  editAirportCityInput.value = airport.city;
  editAirportCodeInput.value = airport.code;
  editAirportRunwaysInput.value = airport.runwaysCount;
  editAirportFoundedInput.value = airport.founded;
};

export const handleAirlineEditFormSubmit = (event, modal, airlineId) => {
  event.preventDefault();
  const editedAirline = {
    id: airlineId,
    name: editAirlineNameInput.value,
    founded: editAirlineFoundedInput.value,
    fleetSize: editAirlineFleetSizeInput.value,
    description: editAirlineDescriptionInput.value,
  };

  update(ENDPOINTS.AIRLINES, editedAirline)
    .then((airline) => {
      const row = document.querySelector(
        `#main-table tbody tr[data-id="${airlineId}"]`
      );
      if (row) {
        const [name, founded, fleetSize, description] = row.children;

        name.textContent = airline.name;
        founded.textContent = airline.founded;
        fleetSize.textContent = airline.fleetSize;
        description.textContent = airline.description;
      }

      showToast("Update successful!");
    })
    .catch((e) => {
      showToast("Update failed!");
      console.error(e.message);
    });

  closeModal(modal);
};

export const handleAirportEditFormSubmit = (event, modal, airportId) => {
  event.preventDefault();
  const editedAirport = {
    id: airportId,
    name: editAirportNameInput.value,
    country: editAirportCountryInput.value,
    city: editAirportCityInput.value,
    code: editAirportCodeInput.value,
    runwaysCount: editAirportRunwaysInput.value,
    founded: editAirportFoundedInput.value,
  };

  update(ENDPOINTS.AIRPORTS, editedAirport)
    .then((airport) => {
      const row = document.querySelector(
        `#main-table tbody tr[data-id="${airportId}"]`
      );
      if (row) {
        const [name, country, city, code, runways, founded] = row.children;

        name.textContent = airport.name;
        country.textContent = airport.country;
        city.textContent = airport.city;
        code.textContent = airport.code;
        runways.textContent = airport.runwaysCount;
        founded.textContent = airport.founded;
      }

      showToast("Update successful!");
    })
    .catch((e) => {
      showToast("Update failed!");
      console.error(e.message);
    });

  closeModal(modal);
};

export const handleFlightEditFormSubmit = (event, modal, flightId) => {
  event.preventDefault();
  const editedFlight = {
    id: flightId,
    flightNumber: editFlightNumberInput.value,
    fromAirport: editFlightFromInput.value,
    toAirport: editFlightToInput.value,
    departureDateTime: editFlightDepartureInput.value,
    arrivalDateTime: editFlightArrivalInput.value,
  };

  update(ENDPOINTS.FLIGHTS, editedFlight)
    .then((flight) => {
      const row = document.querySelector(
        `#main-table tbody tr[data-id="${flightId}"]`
      );
      if (row) {
        const [
          flightNumber,
          fromAirport,
          toAirport,
          departureDateTime,
          arrivalDateTime,
        ] = row.children;

        flightNumber.textContent = flight.flightNumber;
        fromAirport.textContent = flight.fromAirport;
        toAirport.textContent = flight.toAirport;
        departureDateTime.textContent = flight.departureDateTime;
        arrivalDateTime.textContent = flight.arrivalDateTime;
      }

      showToast("Update successful!");
    })
    .catch((e) => {
      showToast("Update failed!");
      console.error(e.message);
    });

  closeModal(modal);
};
