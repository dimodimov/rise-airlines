import { fetchAll } from './api/httpRequests.js';
import { createAirportTableRow, showToast } from './Utilities/createDomElements.js';
import { handleAddAirport } from './Components/submitForm.js';
import { ENDPOINTS } from './Api/config.js';

const tbody = document.querySelector("#main-table tbody");

document.addEventListener("DOMContentLoaded", () => {
document.getElementById("submit-entity-form").addEventListener("submit", handleAddAirport);

  fetchAll(ENDPOINTS.AIRPORTS)
    .then((airports) => {
        airports.forEach((airport) => {
        const row = createAirportTableRow(airport);
        tbody.appendChild(row);
      });
    })
    .catch((e) => {
      showToast("Failed to get airports!");
      console.error(e.message)
    });
});
