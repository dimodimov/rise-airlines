﻿using Airlines.Persistence.Basic;

namespace Airlines.UnitTest;
public class FileReaderTest
{
    [Fact]
    public void FileReader_ValidFile_ShouldRead()
    {
        // Arrange && Act
        var result = FileReader.StreamReader("../../../TestData/airports.txt");

        // Assert
        Assert.True(result.Count > 1);
    }

    [Fact]
    public void FileReader_NonexistentFile_Throws()
    {
        // Arrange
        var wrongFilePath = "../../../TestData/airportsssssssssssss.txt";

        // Act & Assert
        _ = Assert.Throws<FileNotFoundException>(() => FileReader.StreamReader(wrongFilePath));
    }
}
