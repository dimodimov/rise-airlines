﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Airport;
using Airlines.Business.Models.Flight;
using Airlines.Business.Models.Strategies.RouteSearchStrategy;

namespace Airlines.UnitTest;
public class RouteTest
{
    [Fact]
    public void Route_Creation_NewRoute()
    {
        // Arrange
        var flightManager = new FlightManager();
        var route = new Route();

        // Act
        flightManager.Route = route;

        // Assert
        Assert.Equal(route, flightManager.Route);
    }

    [Fact]
    public void RouteAddFlight_CorrectRoute_AddedRoute()
    {
        // Arrange
        var route = new Route();
        var flightManager = new FlightManager();
        var flight = new Flight("FL123", "JFK", "LAX", 300, 2, "Airbus A320");
        _ = flightManager.AddFlight(flight);

        // Act
        _ = route.AddFlight("FL123", flightManager);

        // Assert
        Assert.Contains(flight, route.RouteList);
    }

    [Fact]
    public void RemoveLastRoute_ExistingRoute_RemoveRoute()
    {
        // Arrange
        var route = new Route();
        var flightManager = new FlightManager();
        var flight = new Flight("FL123", "JFK", "LAX", 300, 2, "Airbus A320");
        _ = flightManager.AddFlight(flight);
        _ = route.AddFlight("FL123", flightManager);

        // Act
        _ = route.RemoveLastFlight();

        // Assert
        Assert.Empty(route.RouteList);
    }

    [Fact]
    public void RouteAddFlight_InvalidFlight_Throws()
    {
        // Arrange
        var route = new Route();
        var flightManager = new FlightManager();

        // Act & Assert
        _ = Assert.Throws<InvalidFlightCodeException>(() => route.AddFlight("Flight123", flightManager));
    }

    [Fact]
    public void RouteAddFlight_IllogicalConnectionFlight_Throws()
    {
        // Arrange
        var route = new Route();
        var flightManager = new FlightManager();
        var flight = new Flight("FL123", "JFK", "LAX", 300, 2, "Airbus A320");
        var flightTwo = new Flight("FL234", "RCA", "OAE", 300, 2, "Airbus A320");
        _ = flightManager.AddFlight(flight);
        _ = flightManager.AddFlight(flightTwo);
        _ = route.AddFlight("FL123", flightManager);

        // Act & Assert
        _ = Assert.Throws<InvalidRouteException>(() => route.AddFlight("FL234", flightManager));
    }

    [Fact]
    public void RouteRemoveLastFlight_NoFlights_Throws()
    {
        // Arrange
        var route = new Route();

        // Act & Assert
        _ = Assert.Throws<InvalidRouteException>(route.RemoveLastFlight);
    }

    [Theory]
    [InlineData("ATL")]
    [InlineData("JFK")]
    [InlineData("ORD")]
    public void FindRoute_ValidRoute_TreeNode(string route)
    {
        // Arrange
        var flightManager = new FlightManager();
        flightManager.LoadRepository("../../../TestData/flights.txt");
        flightManager.LoadRouteTreeData("../../../TestData/routesValid.txt");

        // Act
        var routeNode = flightManager.RouteTree?.FindRoute(route);

        // Assert
        Assert.Equal(route, routeNode!.Value.ArrivalAirport);
    }

    [Fact]
    public void GetFlightRoute_MultipleStopsRoute_RouteString()
    {
        // Arrange
        var flightManager = new FlightManager();
        flightManager.LoadRepository("../../../TestData/flights.txt");
        flightManager.LoadRouteTreeData("../../../TestData/routesValid.txt");
        var searchRoute = "ATL";

        // Act
        var foundRoute = flightManager.RouteTree?.GetFlightRoute(searchRoute);

        // Assert
        Assert.Equal("DFW->JFK->LAX->ORD->ATL", foundRoute);
    }

    [Fact]
    public void GetFlightRoute_DirectRoute_RouteString()
    {
        // Arrange
        var flightManager = new FlightManager();
        flightManager.LoadRepository("../../../TestData/flights.txt");
        flightManager.LoadRouteTreeData("../../../TestData/routesValid.txt");
        var searchRoute = "JFK";

        // Act
        var foundRoute = flightManager.RouteTree?.GetFlightRoute(searchRoute);

        // Assert
        Assert.Equal("DFW->JFK", foundRoute);
    }

    [Fact]
    public void LoadRouteTreeData_DuplicateFlight_Throws()
    {
        // Arrange
        var flightManager = new FlightManager();
        flightManager.LoadRepository("../../../TestData/flights.txt");

        // Act & Assert
        _ = Assert.Throws<InvalidRouteException>(() => flightManager.LoadRouteTreeData("../../../TestData/routesInvalid.txt"));
    }

    [Theory]
    [InlineData("DFW")] // Root Airport
    [InlineData("SOF")]
    public void FindRoute_NonexistentRoute_Null(string route)
    {
        // Arrange
        var flightManager = new FlightManager();
        flightManager.LoadRepository("../../../TestData/flights.txt");
        flightManager.LoadRouteTreeData("../../../TestData/routesValid.txt");
        var foundRoute = flightManager.RouteTree?.FindRoute(route);

        // Act & Assert
        Assert.Null(foundRoute);
    }

    [Theory]
    [InlineData("DFW")] // Root Airport
    [InlineData("SOF")]
    public void GetFlightRoute_NonexistentRoute_Null(string route)
    {
        // Arrange
        var flightManager = new FlightManager();
        flightManager.LoadRepository("../../../TestData/flights.txt");
        flightManager.LoadRouteTreeData("../../../TestData/routesValid.txt");
        var foundRoute = flightManager.RouteTree?.GetFlightRoute(route);

        // Act & Assert
        Assert.Null(foundRoute);
    }

    [Theory]
    [InlineData("JFK", "LAX", "JFK->LAX")]
    [InlineData("JFK", "ATL", "JFK->ATL")]
    [InlineData("DFW", "LAX", "DFW->LAX")]
    public void ShortestPathFewestStops_ExistingRoute_True(string departureAirport, string arrivalAirport, string expected)
    {
        // Assert
        var airportManager = new AirportManager();
        airportManager.LoadRepository("../../../TestData/airports.txt");
        airportManager.AirportGraph.LoadAirports(airportManager);
        var flightManager = new FlightManager();
        flightManager.LoadRepository("../../../TestData/flights.txt");
        airportManager.AirportGraph.LoadFlights(flightManager);
        airportManager.AirportGraph.SetStrategy(new ShortestPathFewestStops());

        // Act
        var result = airportManager.AirportGraph.GetShortestPath(departureAirport, arrivalAirport);

        // Assert
        Assert.Equal(expected, result);
    }

    [Theory]
    [InlineData("JFK", "LAX", "JFK->LAX")]
    [InlineData("JFK", "ATL", "JFK->LAX->ORD->ATL")]
    [InlineData("DFW", "LAX", "DFW->LAX")]
    public void ShortestPathPrice_ExistingRoute_True(string departureAirport, string arrivalAirport, string expected)
    {
        // Assert
        var airportManager = new AirportManager();
        airportManager.LoadRepository("../../../TestData/airports.txt");
        airportManager.AirportGraph.LoadAirports(airportManager);
        var flightManager = new FlightManager();
        flightManager.LoadRepository("../../../TestData/flights.txt");
        airportManager.AirportGraph.LoadFlights(flightManager);
        airportManager.AirportGraph.SetStrategy(new ShortestPathByPrice());

        // Act
        var result = airportManager.AirportGraph.GetShortestPath(departureAirport, arrivalAirport);

        // Assert
        Assert.Equal(expected, result);
    }

    [Theory]
    [InlineData("JFK", "LAX", "JFK->LAX")]
    [InlineData("JFK", "ATL", "JFK->LAX->ORD->ATL")]
    [InlineData("DFW", "LAX", "DFW->LAX")]
    public void ShortestPathDuration_ExistingRoute_True(string departureAirport, string arrivalAirport, string expected)
    {
        // Assert
        var airportManager = new AirportManager();
        airportManager.LoadRepository("../../../TestData/airports.txt");
        airportManager.AirportGraph.LoadAirports(airportManager);
        var flightManager = new FlightManager();
        flightManager.LoadRepository("../../../TestData/flights.txt");
        airportManager.AirportGraph.LoadFlights(flightManager);
        airportManager.AirportGraph.SetStrategy(new ShortestPathByDuration());

        // Act
        var result = airportManager.AirportGraph.GetShortestPath(departureAirport, arrivalAirport);

        // Assert
        Assert.Equal(expected, result);
    }

    [Theory]
    [InlineData("JFK", "JFK")]
    [InlineData("JFK", "OHM")]
    public void ShortestPath_InvalidRoute_Throws(string departureAirport, string arrivalAirport)
    {
        // Assert
        var airportManager = new AirportManager();
        airportManager.LoadRepository("../../../TestData/airports.txt");
        airportManager.AirportGraph.LoadAirports(airportManager);
        var flightManager = new FlightManager();
        flightManager.LoadRepository("../../../TestData/flights.txt");
        airportManager.AirportGraph.LoadFlights(flightManager);

        // Act & Assert
        _ = Assert.Throws<InvalidRouteException>(()
            => airportManager.AirportGraph.GetShortestPath(departureAirport, arrivalAirport));
    }

    [Theory]
    [InlineData("JFK", "JFK")]
    [InlineData("JFK", "OHM")]
    public void ShortestPath_InvalidAirportRoute_Throws(string departureAirport, string arrivalAirport)
    {
        // Assert
        var airportManager = new AirportManager();
        airportManager.LoadRepository("../../../TestData/airports.txt");
        airportManager.AirportGraph.LoadAirports(airportManager);
        var flightManager = new FlightManager();
        flightManager.LoadRepository("../../../TestData/flights.txt");
        airportManager.AirportGraph.LoadFlights(flightManager);

        // Act & Assert
        _ = Assert.Throws<InvalidRouteException>(()
            => airportManager.AirportGraph.GetShortestPath(departureAirport, arrivalAirport));
    }

    [Theory]
    [InlineData("JFK", "LAX")]
    [InlineData("JFK", "ATL")]
    public void IsConnected_ExistingRoute_True(string departureAirport, string arrivalAirport)
    {
        // Assert
        var airportManager = new AirportManager();
        airportManager.LoadRepository("../../../TestData/airports.txt");
        airportManager.AirportGraph.LoadAirports(airportManager);
        var flightManager = new FlightManager();
        flightManager.LoadRepository("../../../TestData/flights.txt");
        airportManager.AirportGraph.LoadFlights(flightManager);

        // Act
        var result = airportManager.AirportGraph.IsConnected(departureAirport, arrivalAirport);

        // Assert
        Assert.True(result);
    }

    [Theory]
    [InlineData("JFK", "ATR")]
    [InlineData("JFK", "OHM")]
    public void IsConnected_InvalidRoute_False(string departureAirport, string arrivalAirport)
    {
        // Assert
        var airportManager = new AirportManager();
        airportManager.LoadRepository("../../../TestData/airports.txt");
        airportManager.AirportGraph.LoadAirports(airportManager);
        var flightManager = new FlightManager();
        flightManager.LoadRepository("../../../TestData/flights.txt");
        airportManager.AirportGraph.LoadFlights(flightManager);
        var result = airportManager.AirportGraph.IsConnected(departureAirport, arrivalAirport);

        // Act & Assert
        Assert.False(result);
    }

    [Fact]
    public void IsConnected_DestinationMatchesArrival_Throws()
    {
        // Assert
        var airportManager = new AirportManager();
        airportManager.LoadRepository("../../../TestData/airports.txt");
        airportManager.AirportGraph.LoadAirports(airportManager);
        var flightManager = new FlightManager();
        flightManager.LoadRepository("../../../TestData/flights.txt");
        airportManager.AirportGraph.LoadFlights(flightManager);

        // Act & Assert
        _ = Assert.Throws<InvalidRouteException>(() => airportManager.AirportGraph.IsConnected("JFK", "JFK"));
    }
}
