﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Airline;

namespace Airlines.UnitTest;
public class AirlineTest
{

    [Theory]
    [InlineData("BGAir")]
    [InlineData("PLAir")]
    [InlineData("Air")]
    public void IsValidAirline_ValidAirline_True(string validAirline)
    {
        // Act
        var result = Airline.IsValidAirline(validAirline);

        // Assert
        Assert.True(result);
    }

    [Theory]
    [InlineData("BAir123")]
    [InlineData("BGAirlines")]
    [InlineData("VR@ir1")]

    public void IsValidAirline_InvalidAirline_False(string invalidAirline)
    {
        // Act
        var result = Airline.IsValidAirline(invalidAirline);

        // Assert
        Assert.False(result);
    }


    [Fact]
    public void AddAirline_Duplicate_Throws()
    {
        // Arrange
        var airlineManager = new AirlineManager();
        var airline = new Airline("BGAir");

        // Act
        _ = airlineManager.AddAirline(airline);

        // Assert
        _ = Assert.Throws<DuplicateEntryException>(() => airlineManager.AddAirline(airline));
    }

#pragma warning disable IDE0022 // Use expression body for method
    [Theory]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData("Airlineaaaa")]
    public void NewAirline_InvalidName_Throws(string airline)
    {
        // Assert
        _ = Assert.Throws<InvalidAirlineNameException>(() => new Airline(airline));
    }
#pragma warning disable IDE0022 // Use expression body for method


    [Fact]
    public void AirlineExists_CorrectAirline_True()
    {
        // Arrange
        var airlineManager = new AirlineManager();
        var airline = new Airline("BGAir");

        // Act
        _ = airlineManager.AddAirline(airline);
        var result = airlineManager.AirlineExists("BGAir");

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void AirlineExists_WrongAirline_False()
    {
        // Arrange
        var airlineManager = new AirlineManager();
        var airline = new Airline("BGAir");

        // Act
        _ = airlineManager.AddAirline(airline);
        var result = airlineManager.AirlineExists("BAir");

        // Assert
        Assert.False(result);
    }
}
