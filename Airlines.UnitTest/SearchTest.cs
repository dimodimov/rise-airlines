﻿using System.Diagnostics;
using Xunit.Abstractions;
using Airlines.Business.Utilities;

namespace Airlines.UnitTest;

public class SearchTest(ITestOutputHelper output)
{
    private readonly ITestOutputHelper _output = output;

    [Theory]
    [InlineData("AAA")]
    [InlineData("BBB")]
    [InlineData("CCC")]
    [InlineData("DDD")]
    public void BinarySearch_StringSearch_SearchTerm(string searchTerm)
    {
        // Arrange
        var testList = new List<string> { "AAA", "BBB", "CCC", "DDD" };
        var expectedResult = searchTerm;

        // Act
        var searchResult = testList.BinarySearchCustom(searchTerm);

        // Assert
        Assert.Equal(expectedResult, searchResult);
    }

    [Theory]
    [InlineData("FFF")]
    [InlineData("AAA ")]
    [InlineData(" ")]
    [InlineData("")]
    public void BinarySearch_StringSearch_Null(string searchTerm)
    {
        // Arrange
        var testList = new List<string> { "AAA", "BBB", "CCC", "DDD" };
        var emptyTestList = new List<string>();

        // Act
        var searchResult = testList.BinarySearchCustom(searchTerm);
        var searchResultEmptyList = emptyTestList.BinarySearchCustom(searchTerm);

        // Assert
        Assert.Null(searchResult);
        Assert.Null(searchResultEmptyList);
    }

    [Theory]
    [InlineData("AAA")]
    [InlineData("BBB")]
    [InlineData("CCC")]
    [InlineData("DDD")]
    public void LinearSearch_StringSearch_SearchTerm(string searchTerm)
    {
        // Arrange
        var testList = new List<string> { "AAA", "BBB", "CCC", "DDD" };
        var expectedResult = searchTerm;

        // Act
        var searchResult = testList.LinearSearch(searchTerm);

        // Assert
        Assert.Equal(expectedResult, searchResult);
    }

    [Theory]
    [InlineData("FFF")]
    [InlineData("AAA ")]
    [InlineData(" ")]
    [InlineData("")]
    public void LinearSearch_StringSearch_Null(string searchTerm)
    {
        // Arrange
        var testList = new List<string> { "AAA", "BBB", "CCC", "DDD" };
        var emptyTestList = new List<string>();

        // Act
        var searchResult = testList.LinearSearch(searchTerm);
        var searchResultEmptyList = emptyTestList.LinearSearch(searchTerm);

        // Assert
        Assert.Null(searchResult);
        Assert.Null(searchResultEmptyList);
    }

    [Fact]
    public void BinarySearch_PerformanceTest()
    {
        // Arrange
        var testList = new List<string>();
        for (var i = 0; i < 1000000; i++)
            testList.Add(i.ToString());
        var searchTerm = "999999";

        // Act
        var stopwatch = Stopwatch.StartNew();
        var searchesCompleted = 0;
        var timeFrame = TimeSpan.FromSeconds(1);

        while (stopwatch.Elapsed < timeFrame)
        {
            _ = testList.BinarySearchCustom(searchTerm);
            searchesCompleted++;
        }

        stopwatch.Stop();

        // Assert
        _output.WriteLine($"BinarySearch: Searches completed in {timeFrame.TotalSeconds} seconds: {searchesCompleted}");
    }

    [Fact]
    public void LinearSearch_PerformanceTest()
    {
        // Arrange
        var testList = new List<string>();
        for (var i = 0; i < 1000000; i++)
            testList.Add(i.ToString());
        var searchTerm = "999999";

        // Act
        var stopwatch = Stopwatch.StartNew();
        var searchesCompleted = 0;
        var timeFrame = TimeSpan.FromSeconds(1);

        while (stopwatch.Elapsed < timeFrame)
        {
            _ = testList.LinearSearch(searchTerm);
            searchesCompleted++;
        }

        stopwatch.Stop();

        // Assert
        _output.WriteLine($"LinearSearch: Searches completed in {timeFrame.TotalSeconds} seconds: {searchesCompleted}");
    }
}
