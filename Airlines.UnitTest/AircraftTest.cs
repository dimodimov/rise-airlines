﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Aircraft;
using Airlines.Business.Models.Flight;
using Airlines.Business.Models.Reservation;

namespace Airlines.UnitTest;
public class AircraftTest
{
    [Fact]
    public void CargoAircraft_CorrectParams_NewAircraft()
    {
        // Arrange
        var model = "Airbus";
        var cargoWeight = 22;
        var cargoVolume = 0.25;

        // Act
        var aircraft = new CargoAircraft(model, cargoWeight, cargoVolume);

        // Assert
        Assert.Equal(model, aircraft.Model);
        Assert.Equal(cargoWeight, aircraft.CargoWeight);
        Assert.Equal(cargoVolume, aircraft.CargoVolume);
    }

    [Fact]
    public void PassengerAircraft_CorrectParams_NewAircraft()
    {
        // Arrange
        var model = "Airbus";
        var cargoWeight = 22;
        var cargoVolume = 0.25;
        var seats = 50;

        // Act
        var aircraft = new PassengerAircraft(model, cargoWeight, cargoVolume, seats);

        // Assert
        Assert.Equal(model, aircraft.Model);
        Assert.Equal(cargoWeight, aircraft.CargoWeight);
        Assert.Equal(cargoVolume, aircraft.CargoVolume);
        Assert.Equal(seats, aircraft.Seats);
    }

    [Fact]
    public void PrivateAircraft_CorrectParams_NewAircraft()
    {
        // Arrange
        var model = "Airbus";
        var seats = 50;

        // Act
        var aircraft = new PrivateAircraft(model, seats);

        // Assert
        Assert.Equal(model, aircraft.Model);
        Assert.Equal(seats, aircraft.Seats);
    }

    [Fact]
    public void AircraftManager_ValidPassengerAircraft_AddsAircraft()
    {
        // Arrange
        var aircraftManager = new AircraftManager();
        var model = "Airbus";
        var cargoWeight = "20000";
        var cargoVolume = "37.4";
        var seats = "150";

        // Act
        aircraftManager.AddAircraft(model, cargoWeight, cargoVolume, seats);

        // Assert
        Assert.Equal(model, aircraftManager.GetPassengerAircraftByModel(model)?.Model);
        Assert.Equal(int.Parse(cargoWeight), aircraftManager.GetPassengerAircraftByModel(model)?.CargoWeight);
        Assert.Equal(double.Parse(cargoVolume), aircraftManager.GetPassengerAircraftByModel(model)?.CargoVolume);
        Assert.Equal(int.Parse(seats), aircraftManager.GetPassengerAircraftByModel(model)?.Seats);
    }

    [Fact]
    public void AircraftManager_ValidCargoAircraft_AddsAircraft()
    {
        // Arrange
        var aircraftManager = new AircraftManager();
        var model = "Boeing";
        var cargoWeight = "140000";
        var cargoVolume = "854.5";
        var seats = "-";

        // Act
        aircraftManager.AddAircraft(model, cargoWeight, cargoVolume, seats);

        // Assert
        Assert.Equal(model, aircraftManager.GetCargoAircraftByModel(model)?.Model);
        Assert.Equal(int.Parse(cargoWeight), aircraftManager.GetCargoAircraftByModel(model)?.CargoWeight);
        Assert.Equal(double.Parse(cargoVolume), aircraftManager.GetCargoAircraftByModel(model)?.CargoVolume);
    }

    [Fact]
    public void AircraftManager_ValidPrivateAircraft_AddsAircraft()
    {
        // Arrange
        var aircraftManager = new AircraftManager();
        var model = "Gulfstream";
        var cargoWeight = "-";
        var cargoVolume = "-";
        var seats = "18";

        // Act
        aircraftManager.AddAircraft(model, cargoWeight, cargoVolume, seats);

        // Assert
        Assert.Equal(model, aircraftManager.GetPrivateAircraftByModel(model)?.Model);
        Assert.Equal(int.Parse(seats), aircraftManager.GetPrivateAircraftByModel(model)?.Seats);
    }

    [Fact]
    public void CargoReservation_ValidParams_NewCargoReservation()
    {
        // Arrange
        var flightIdentifier = "FL123";
        var cargoWeight = 123;
        var cargoVolume = 22.5;

        // Act
        var reservation = new CargoReservation(flightIdentifier, cargoWeight, cargoVolume);

        // Assert
        Assert.Equal(flightIdentifier, reservation.FlightIdentifier);
        Assert.Equal(cargoWeight, reservation.CargoWeight);
        Assert.Equal(cargoVolume, reservation.CargoVolume);
    }

    [Fact]
    public void TicketReservation_ValidParams_NewTicketReservation()
    {
        // Arrange
        var flightIdentifier = "FL123";
        var smallBaggageCount = 2;
        var largeBaggageCount = 3;
        var seats = 2;

        // Act
        var reservation = new TicketReservation(flightIdentifier, seats, smallBaggageCount, largeBaggageCount);

        // Assert
        Assert.Equal(flightIdentifier, reservation.FlightIdentifier);
        Assert.Equal(seats, reservation.Seats);
        Assert.Equal(smallBaggageCount, reservation.SmallBaggageCount);
        Assert.Equal(largeBaggageCount, reservation.LargeBaggageCount);
    }

    [Fact]
    public void ReserveCargo_ValidParams_SubmitsReservation()
    {
        // Arrange
        var reservationManager = new ReservationManager();
        var flightManager = new FlightManager();
        var aircraftManager = new AircraftManager();
        var flightIdentifier = "FL123";
        var airCraftCargoWeight = "20000";
        var aircraftCargoVolume = "37.4";
        var aircraftSeats = "-";

        var reservationCargoWeight = 240;
        var reservationCargoVolume = 1.5;

        _ = flightManager.AddFlight(new Flight(flightIdentifier, "JFK", "LAX", 300, 2, "Airbus"));
        aircraftManager.AddAircraft("Airbus", airCraftCargoWeight, aircraftCargoVolume, aircraftSeats);

        // Act
        _ = reservationManager.ReserveCargo(flightIdentifier, reservationCargoWeight, reservationCargoVolume, flightManager, aircraftManager);
        var reservation = reservationManager.GetCargoReservationList()[0];

        // Assert
        _ = Assert.Single(reservationManager.GetCargoReservationList());
        Assert.Equal(flightIdentifier, reservation.FlightIdentifier);
        Assert.Equal(reservationCargoWeight, reservation.CargoWeight);
        Assert.Equal(reservationCargoVolume, reservation.CargoVolume);
    }

    [Fact]
    public void ReserveTicket_ValidParams_SubmitsReservation()
    {
        // Arrange
        var reservationManager = new ReservationManager();
        var flightManager = new FlightManager();
        var aircraftManager = new AircraftManager();
        var flightIdentifier = "FL123";
        var airCraftCargoWeight = "20000";
        var aircraftCargoVolume = "37.4";
        var aircraftSeats = "150";

        var smallBaggageCount = 2;
        var largeBaggageCount = 3;
        var reservationSeats = 2;

        _ = flightManager.AddFlight(new Flight(flightIdentifier, "JFK", "LAX", 300, 2, "Airbus"));
        aircraftManager.AddAircraft("Airbus", airCraftCargoWeight, aircraftCargoVolume, aircraftSeats);

        // Act
        _ = reservationManager.ReserveTicket(flightIdentifier, reservationSeats, smallBaggageCount, largeBaggageCount, flightManager, aircraftManager);
        var reservation = reservationManager.GetTicketReservationList().ToList()[0];

        // Assert
        _ = Assert.Single(reservationManager.GetTicketReservationList());
        Assert.Equal(flightIdentifier, reservation.FlightIdentifier);
        Assert.Equal(reservationSeats, reservation.Seats);
        Assert.Equal(smallBaggageCount, reservation.SmallBaggageCount);
        Assert.Equal(largeBaggageCount, reservation.LargeBaggageCount);
    }

    [Theory]
    [InlineData("Airbus", 20001, 1.5)]
    [InlineData("Airbus", 240, 37.5)]
    public void ReserveCargo_InvalidReservationParams_Throws(string aircraft, int cargoWeight, double cargoVolume)
    {
        // Arrange
        var reservationManager = new ReservationManager();
        var flightManager = new FlightManager();
        var aircraftManager = new AircraftManager();
        var flightIdentifier = "FL123";
        var airCraftCargoWeight = "20000";
        var aircraftCargoVolume = "37.4";
        var aircraftSeats = "-";

        _ = flightManager.AddFlight(new Flight(flightIdentifier, "JFK", "LAX", 300, 2, "Airbus"));
        aircraftManager.AddAircraft(aircraft, airCraftCargoWeight, aircraftCargoVolume, aircraftSeats);

        // Act & Assert
        _ = Assert.Throws<InvalidReservationParameterException>(() => reservationManager.ReserveCargo(flightIdentifier, cargoWeight, cargoVolume, flightManager, aircraftManager));
    }

    [Theory]
    [InlineData("Boeing", 240, 1.5)]
    [InlineData("Airbus 320", 240, 1.5)]
    public void ReserveCargo_InvalidAircraftParams_Throws(string aircraft, int cargoWeight, double cargoVolume)
    {
        // Arrange
        var reservationManager = new ReservationManager();
        var flightManager = new FlightManager();
        var aircraftManager = new AircraftManager();
        var flightIdentifier = "FL123";
        var airCraftCargoWeight = "20000";
        var aircraftCargoVolume = "37.4";
        var aircraftSeats = "-";

        _ = flightManager.AddFlight(new Flight(flightIdentifier, "JFK", "LAX", 300, 2, "Airbus"));
        aircraftManager.AddAircraft(aircraft, airCraftCargoWeight, aircraftCargoVolume, aircraftSeats);

        // Act & Assert
        _ = Assert.Throws<InvalidAircraftParameterException>(() => reservationManager.ReserveCargo(flightIdentifier, cargoWeight, cargoVolume, flightManager, aircraftManager));
    }

    [Theory]
    [InlineData("Airbus", 1000, 1, 2)]
    [InlineData("Airbus", 2, 500, 2)]
    [InlineData("Airbus", 2, 1, 151)]
    public void ReserveTicket_InvalidReservationParams_Throws(string aircraft, int smallBaggageCount, int largeBaggageCount, int seats)
    {
        // Arrange
        var reservationManager = new ReservationManager();
        var flightManager = new FlightManager();
        var aircraftManager = new AircraftManager();
        var flightIdentifier = "FL123";
        var airCraftCargoWeight = "20000";
        var aircraftCargoVolume = "37.4";
        var aircraftSeats = "150";

        _ = flightManager.AddFlight(new Flight(flightIdentifier, "JFK", "LAX", 300, 2, "Airbus"));
        aircraftManager.AddAircraft(aircraft, airCraftCargoWeight, aircraftCargoVolume, aircraftSeats);

        // Assert
        _ = Assert.Throws<InvalidReservationParameterException>(() => reservationManager.ReserveTicket(flightIdentifier, seats, smallBaggageCount, largeBaggageCount, flightManager, aircraftManager));
    }

    [Theory]
    [InlineData("Airbus 320", 240, 1.5, 2)]
    [InlineData("Boeing", 240, 1.5, 2)]
    public void ReserveTicket_InvalidAircraft_Throws(string aircraft, int smallBaggageCount, int largeBaggageCount, int seats)
    {
        // Arrange
        var reservationManager = new ReservationManager();
        var flightManager = new FlightManager();
        var aircraftManager = new AircraftManager();
        var flightIdentifier = "FL123";
        var airCraftCargoWeight = "20000";
        var aircraftCargoVolume = "37.4";
        var aircraftSeats = "150";

        _ = flightManager.AddFlight(new Flight(flightIdentifier, "JFK", "LAX", 300, 2, "Airbus"));
        aircraftManager.AddAircraft(aircraft, airCraftCargoWeight, aircraftCargoVolume, aircraftSeats);

        // Assert
        _ = Assert.Throws<InvalidAircraftParameterException>(() => reservationManager.ReserveTicket(flightIdentifier, seats, smallBaggageCount, largeBaggageCount, flightManager, aircraftManager));
    }
}
