﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Airport;

namespace Airlines.UnitTest;
public class AirportTest
{
    [Theory]
    [InlineData("SOF")]
    [InlineData("S23")]
    [InlineData("VARN")]
    [InlineData("PD")]
    public void AirportIsValidIdentifier_ValidId_True(string validIdentifier)
    {
        // Act
        var result = Airport.IsValidIdentifier(validIdentifier);

        // Assert
        Assert.True(result);
    }

    [Theory]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData("S")]
    [InlineData("S!23")]
    [InlineData("VARNA")]
    [InlineData("#PLDV")]
    public void AirportIsValidIdentifier_InvalidId_False(string invalidIdentifier)
    {
        // Act
        var result = Airport.IsValidIdentifier(invalidIdentifier);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void AddAirport_ValidAirport_AddAirport()
    {
        // Arrange
        var airportManager = new AirportManager();
        var airport = new Airport("SOF", "Sofia Airport", "Sofia", "BG");

        // Act
        _ = airportManager.AddAirport(airport);

        // Assert
        Assert.Contains(airport, airportManager.GetAirports());
    }

    [Fact]
    public void AddAirport_Duplicate_Throws()
    {
        // Arrange
        var airportManager = new AirportManager();
        var airport = new Airport("SOF", "Sofia Airport", "Sofia", "BG");

        // Act
        _ = airportManager.AddAirport(airport);

        // Assert
        _ = Assert.Throws<DuplicateEntryException>(() => airportManager.AddAirport(airport));
    }

#pragma warning disable IDE0022 // Use expression body for method
    [Theory]
    [InlineData("S", "Sofia Airport", "Sofia", "BG")]
    [InlineData("SOFIA", "Sofia Airport", "Sofia", "BG")]
    [InlineData("", "Sofia Airport", "Sofia", "BG")]
    [InlineData("SOF", "Sofia Airport!", "Sofia", "BG")]
    [InlineData("SOF", "Sofia Airport1", "Sofia", "BG")]
    [InlineData("SOF", "Sofia Airport", "Sofia1", "BG1")]
    public void NewAirport_InvalidParams_Throws(string identifier, string name, string city, string country)
    {
        // Assert
        _ = Assert.Throws<InvalidAirportException>(() => new Airport(identifier, name, city, country));
    }
#pragma warning restore IDE0022 // Use expression body for method

    [Fact]
    public void ListAirportsBy_FilterByCity_ListByCity()
    {
        // Arrange
        var airportManager = new AirportManager();
        var airport = new Airport("SOF", "Sofia Airport", "Sofia", "BG");

        // Act
        _ = airportManager.AddAirport(airport);
        var result = airportManager.ListAirportsBy("Sofia", "city");

        // Assert
        Assert.Equal("SOF", result);
    }

    [Fact]
    public void ListAirportsBy_FilterByCountry_ListByCountry()
    {
        // Arrange
        var airportManager = new AirportManager();
        var airport = new Airport("SOF", "Sofia Airport", "Sofia", "BG");

        // Act
        _ = airportManager.AddAirport(airport);
        var result = airportManager.ListAirportsBy("BG", "country");

        // Assert
        Assert.Equal("SOF", result);
    }

    [Fact]
    public void ListAirportsBy_FilterByCity_NotFound()
    {
        // Arrange
        var airportManager = new AirportManager();
        var airport = new Airport("SOF", "Sofia Airport", "Sofia", "BG");

        // Act
        _ = airportManager.AddAirport(airport);
        var result = airportManager.ListAirportsBy("SF", "city");

        // Assert
        Assert.Equal($"No airports in found in city SF.", result);
    }

    [Fact]
    public void ListAirportsBy_FilterByCountry_NotFound()
    {
        // Arrange
        var airportManager = new AirportManager();
        var airport = new Airport("SOF", "Sofia Airport", "Sofia", "BG");

        // Act
        _ = airportManager.AddAirport(airport);
        var result = airportManager.ListAirportsBy("GB", "country");

        // Assert
        Assert.Equal($"No airports in found in country GB.", result);
    }

    [Fact]
    public void ListAirportsBy_WrongType_InvalidType()
    {
        // Arrange
        var airportManager = new AirportManager();
        var airport = new Airport("SOF", "Sofia Airport", "Sofia", "BG");

        // Act
        _ = airportManager.AddAirport(airport);
        var result = airportManager.ListAirportsBy("BG", "countryy");

        // Assert
        Assert.Equal("Invalid type. Please select city or country.", result);
    }
}
