﻿using System.Diagnostics;
using Xunit.Abstractions;
using Airlines.Business.Utilities;

namespace Airlines.UnitTest;

public class SortingTest(ITestOutputHelper output)
{
    private readonly ITestOutputHelper _output = output;

    [Fact]
    public void BubbleSort_StringList_SortAscending()
    {
        // Arrange
        var expectedResultOne = new List<string> { "ARC", "BRB", "CAD", "DFG" };
        var testCaseOne = new List<string> { "CAD", "BRB", "DFG", "ARC" };

        var expectedResultTwo = new List<string> { "Flight1", "Flight2", "Flight3", "Flight4", "Flight5" };
        var testCaseTwo = new List<string> { "Flight3", "Flight5", "Flight1", "Flight2", "Flight4" };

        // Act
        testCaseOne.BubbleSort();
        testCaseTwo.BubbleSort("ascending");

        // Assert
        Assert.Equal(expectedResultOne, testCaseOne);
        Assert.Equal(expectedResultTwo, testCaseTwo);
    }

    [Fact]
    public void BubbleSort_StringList_SortDescending()
    {
        // Arrange
        var expectedResultOne = new List<string> { "DFG", "CAD", "BRB", "ARC" };
        var testCaseOne = new List<string> { "CAD", "BRB", "DFG", "ARC" };

        var expectedResultTwo = new List<string> { "Flight5", "Flight4", "Flight3", "Flight2", "Flight1" };
        var testCaseTwo = new List<string> { "Flight3", "Flight5", "Flight1", "Flight2", "Flight4" };

        // Act
        testCaseOne.BubbleSort("descending");
        testCaseTwo.BubbleSort("descending");

        // Assert
        Assert.Equal(expectedResultOne, testCaseOne);
        Assert.Equal(expectedResultTwo, testCaseTwo);
    }

    [Fact]
    public void SelectionSort_StringList_SortAscending()
    {
        // Arrange
        var expectedResultOne = new List<string> { "ARC", "BRB", "CAD", "DFG" };
        var testCaseOne = new List<string> { "CAD", "BRB", "DFG", "ARC" };

        var expectedResultTwo = new List<string> { "Flight1", "Flight2", "Flight3", "Flight4", "Flight5" };
        var testCaseTwo = new List<string> { "Flight3", "Flight5", "Flight1", "Flight2", "Flight4" };

        // Act
        testCaseOne.SelectionSort();
        testCaseTwo.SelectionSort("ascending");

        // Assert
        Assert.Equal(expectedResultOne, testCaseOne);
        Assert.Equal(expectedResultTwo, testCaseTwo);
    }

    [Fact]
    public void SelectionSort_StringList_SortDescending()
    {
        // Arrange
        var expectedResultOne = new List<string> { "DFG", "CAD", "BRB", "ARC" };
        var testCaseOne = new List<string> { "CAD", "BRB", "DFG", "ARC" };

        var expectedResultTwo = new List<string> { "Flight5", "Flight4", "Flight3", "Flight2", "Flight1" };
        var testCaseTwo = new List<string> { "Flight3", "Flight5", "Flight1", "Flight2", "Flight4" };

        // Act
        testCaseOne.SelectionSort("descending");
        testCaseTwo.SelectionSort("descending");

        // Assert
        Assert.Equal(expectedResultOne, testCaseOne);
        Assert.Equal(expectedResultTwo, testCaseTwo);
    }

    [Fact]
    public void BubbleSort_PerformanceTest()
    {
        // Arrange
        var testList = new List<string>();
        var random = new Random();

        for (var i = 0; i < 1000; i++)
        {
            testList.Add(random.Next(1000).ToString());
        }

        var sortCount = 0;
        var timeFrame = TimeSpan.FromSeconds(1);

        // Act
        var stopwatch = Stopwatch.StartNew();

        while (stopwatch.Elapsed < timeFrame)
        {
            // Caveat: not 100% accurate as BubbleSort mutates the list and a copy has to be passed every time.
            new List<string>(testList).BubbleSort();
            sortCount++;
        }

        stopwatch.Stop();

        // Assert
        _output.WriteLine($"BubbleSort: Sorted {sortCount} times in {timeFrame.TotalSeconds} seconds.");
    }

    [Fact]
    public void SelectionSort_PerformanceTest()
    {
        // Arrange
        var testList = new List<string>();
        var random = new Random();

        for (var i = 0; i < 1000; i++)
        {
            testList.Add(random.Next(1000).ToString());
        }

        var sortCount = 0;
        var timeFrame = TimeSpan.FromSeconds(1);

        // Act
        var stopwatch = Stopwatch.StartNew();

        while (stopwatch.Elapsed < timeFrame)
        {
            // Caveat: not 100% accurate as SelectionSort mutates the list and a copy has to be passed every time.
            new List<string>(testList).SelectionSort();
            sortCount++;
        }

        stopwatch.Stop();

        // Assert
        _output.WriteLine($"SelectionSort: Sorted {sortCount} times in {timeFrame.TotalSeconds} seconds.");
    }
}
