﻿using Airlines.Business.Exceptions;
using Airlines.Business.Models.Flight;

namespace Airlines.UnitTest;
public class FlightTest
{

#pragma warning disable IDE0022 // Use expression body for method
    [Theory]
    [InlineData("Flight@", "LAX", "JFK", 300, 2, "Airbus A320")]
    [InlineData("#Flight12", "LAX", "JFK", 300, 2, "Airbus A320")]
    [InlineData("Flight!23", "LAX", "JFK", 300, 2, "Airbus A320")]
    public void NewFlight_InvalidFlightParams_Throws(string flight, string arrivalAirport, string departureAirport, decimal price, double duration, string aircraft)
    {
        // Assert
        _ = Assert.Throws<InvalidFlightCodeException>(() => new Flight(flight, arrivalAirport, departureAirport, price, duration, aircraft));
    }


    [Theory]
    [InlineData("Flight12", "L", "JFK", 300, 2, "Airbus A320")]
    [InlineData("Flight23", "LAX", "JFKAA", 300, 2, "Airbus A320")]
    public void NewFlight_InvalidAirportParams_Throws(string flight, string arrivalAirport, string departureAirport, decimal price, double duration, string aircraft)
    {
        // Assert
        _ = Assert.Throws<InvalidAirportException>(() => new Flight(flight, arrivalAirport, departureAirport, price, duration, aircraft));
    }

    [Theory]
    [InlineData("Flight23", "LAX", "JFK", 300, 2, "")]
    [InlineData("Flight23", "LAX", "JFK", 300, 2, " ")]
    public void NewFlight_InvalidAircraftParam_Throws(string flight, string arrivalAirport, string departureAirport, decimal price, double duration, string aircraft)
    {
        // Assert
        _ = Assert.Throws<InvalidAircraftParameterException>(() => new Flight(flight, arrivalAirport, departureAirport, price, duration, aircraft));
    }

#pragma warning restore IDE0022 // Use expression body for method

    [Fact]
    public void GetFlightByIdentifier_ValidFlight_Flight()
    {
        // Arrange
        var flightManager = new FlightManager();
        var flight = new Flight("Flight123", "JFK", "SOF", 300, 2, "Airbus A320");
        _ = flightManager.AddFlight(flight);

        // Act
        var result = flightManager.GetFlightById("Flight123");

        // Assert
        Assert.Equal(result, flight);
    }

    [Fact]
    public void GetFlightByIdentifier_NonexistentFlight_Null()
    {
        // Arrange
        var flightManager = new FlightManager();

        // Act
        var result = flightManager.GetFlightById("Flight123");

        // Assert
        Assert.Null(result);
    }

    [Fact]
    public void AddFlight_ValidFlight_AddsFlight()
    {
        // Arrange
        var flightManager = new FlightManager();
        var flight = new Flight("Flight123", "JFK", "SOF", 300, 2, "Airbus A320");

        // Act
        _ = flightManager.AddFlight(flight);

        // Assert
        Assert.Contains(flight, flightManager.GetFlights());
    }

    [Fact]
    public void AddFlight_Duplicate_Throws()
    {
        // Arrange
        var flightManager = new FlightManager();
        var flight = new Flight("Flight123", "JFK", "SOF", 300, 2, "Airbus A320");

        // Act
        _ = flightManager.AddFlight(flight);

        // Assert
        _ = Assert.Throws<DuplicateEntryException>(() => flightManager.AddFlight(flight));
    }
}
