using Airlines.Business.Utilities;

namespace Airlines.UnitTest;

public class ValidationTest
{
    [Theory]
    [InlineData("JFK")]
    [InlineData("SOF")]
    [InlineData("NYC")]
    public void IsLettersOnly_CorrectInput_True(string airport)
    {
        // Act
        var result = DataValidator.IsLettersOnly(airport);

        // Assert
        Assert.True(result);
    }

    [Theory]
    [InlineData("JFK1")]
    [InlineData("1")]
    [InlineData("@NC")]
    [InlineData(" ")]
    public void IsLettersOnly_SymbolsAndNumbers_False(string airport)
    {
        // Act
        var result = DataValidator.IsLettersOnly(airport);

        // Assert
        Assert.False(result);
    }

    [Theory]
    [InlineData("JFK123")]
    [InlineData("123")]
    [InlineData("123JFK")]
    public void IsAlphanumeric_CorrectInput_True(string airline)
    {
        // Act
        var result = DataValidator.IsAlphanumeric(airline);

        // Assert
        Assert.True(result);
    }

    [Theory]
    [InlineData("JFK@")]
    [InlineData("1-2-3")]
    [InlineData("JFK20%")]
    [InlineData(" ")]
    public void IsAlphanumeric_Symbols_False(string airline)
    {
        // Act
        var result = DataValidator.IsAlphanumeric(airline);

        // Assert
        Assert.False(result);
    }

    [Theory]
    [InlineData("JF K")]
    [InlineData("S OF")]
    [InlineData("W A S")]
    public void IsLettersOrSpaceOnly_CorrectInput_True(string airport)
    {
        // Act
        var result = DataValidator.IsLettersOrSpaceOnly(airport);

        // Assert
        Assert.True(result);
    }

    [Theory]
    [InlineData("JF#K")]
    [InlineData("5 OF")]
    [InlineData("W @ S")]
    public void IsLettersOrSpaceOnly_Symbols_False(string airport)
    {
        // Act
        var result = DataValidator.IsLettersOrSpaceOnly(airport);

        // Assert
        Assert.False(result);
    }
}