﻿document.addEventListener("DOMContentLoaded", () => {
    // Form Fields - id naming a bit different because ASP attributes are used directly
    const name = document.getElementById("Name");
    const founded = document.getElementById("Founded");
    const fleetSize = document.getElementById("FleetSize");
    const description = document.getElementById("Description");

    // Error Message Fields
    const nameError = document.getElementById("name-error");
    const foundedError = document.getElementById("founded-error");
    const fleetSizeError = document.getElementById("fleet-size-error");
    const descriptionError = document.getElementById("description-error");

    // Button Disable/Enable Logic
    const errors = {};
    document.getElementById("submit-form-button").disabled = false;
    const enableSubmitButton = () =>
      (document.getElementById("submit-form-button").disabled = false);
    const disableSubmitButton = () =>
      (document.getElementById("submit-form-button").disabled = true);
    const updateSubmitButtonStatus = () => {
      if (Object.values(errors).some(error=> error != null)) {
        disableSubmitButton();
      } else {
        enableSubmitButton();
      }
    } 

    // Define validation functions
    const validateName = () => {
        const value = name.value.trim();

        if (value === "") {
            errors.name = "Please enter a name.";
            nameError.textContent = errors.name;
            updateSubmitButtonStatus();
            
            return false;
        } else if (value.length < 1 || value.length >= 20) {
            errors.name = "Name must be between 1 and 20 symbols.";
            nameError.textContent = errors.name;
            updateSubmitButtonStatus();

            return false;
        } else {
            errors.name = null;
            nameError.textContent = "";
            updateSubmitButtonStatus();

            return true;
        }
    }

    const validateFounded = () => {
        const value = founded.value.trim();

        if (value === "") {
            errors.founded = "Please enter the date of foundation.";
            foundedError.textContent = errors.founded;
            updateSubmitButtonStatus();

            return false;
        } else {
            errors.founded = null;
            foundedError.textContent = "";
            updateSubmitButtonStatus();

            return true;
        }
    }

    const validateFleetSize = () => {
        const value = fleetSize.value.trim();

        if (value === "") {
            errors.fleetSize = "Please enter the fleet size.";
            fleetSizeError.textContent = errors.fleetSize;
            updateSubmitButtonStatus();

            return false;
        } else if (value < 1 || isNaN(value)) {
            errors.fleetSize = "Fleet size must be a positive number.";
            fleetSizeError.textContent = errors.fleetSize;
            updateSubmitButtonStatus();

            return false;
        } else {
            errors.fleetSize = null;
            fleetSizeError.textContent = "";
            updateSubmitButtonStatus();

            return true;
        }
    }

    const validateDescription = () => {
        const value = description.value.trim();

        if (value === "") {
            errors.description = "Please enter a description.";
            descriptionError.textContent = errors.description;
            updateSubmitButtonStatus();

            return false;
        } else if (value.length < 1 || value.length >= 100) {
            errors.description = "Description must be between 1 and 100 symbols.";
            descriptionError.textContent = errors.description;
            updateSubmitButtonStatus();

            return false;
        } else {
            errors.description = null;
            descriptionError.textContent = "";
            updateSubmitButtonStatus();

            return true;
        }
    }

    // Add event listeners
    name.addEventListener("blur", validateName);
    founded.addEventListener("blur", validateFounded);
    fleetSize.addEventListener("blur", validateFleetSize);
    description.addEventListener("blur", validateDescription);
});