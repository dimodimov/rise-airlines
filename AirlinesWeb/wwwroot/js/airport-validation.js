﻿document.addEventListener("DOMContentLoaded", () => {
  // Form Fields - id naming a bit different because ASP attributes are used directly
  const name = document.getElementById("Name");
  const country = document.getElementById("Country");
  const city = document.getElementById("City");
  const code = document.getElementById("Code");
  const runways = document.getElementById("RunwaysCount");
  const founded = document.getElementById("Founded");

  // Error Message Fields
  const nameError = document.getElementById("name-error");
  const countryError = document.getElementById("country-error");
  const cityError = document.getElementById("city-error");
  const codeError = document.getElementById("code-error");
  const runwaysError = document.getElementById("runways-error");
  const foundedError = document.getElementById("founded-error");

  // Button Disable/Enable Logic
  const errors = {};
  document.getElementById("submit-form-button").disabled = false;
  const enableSubmitButton = () =>
    (document.getElementById("submit-form-button").disabled = false);
  const disableSubmitButton = () =>
    (document.getElementById("submit-form-button").disabled = true);
  const updateSubmitButtonStatus = () => {
    if (Object.values(errors).some(error=> error != null)) {
      disableSubmitButton();
    } else {
      enableSubmitButton();
    }
  }  

  // Define validation functions
  const validateName = () => {
    const value = name.value.trim();
    
    if (value === "") {
      errors.name = "Please enter a name.";
      nameError.textContent = errors.name;
      updateSubmitButtonStatus();
      
      return false;
    } else if (value.length < 1 || value.length >= 20) {
      errors.name = "Name must be between 1 and 20 symbols.";
      nameError.textContent = errors.name;
      updateSubmitButtonStatus();

      return false;
    } else {
      errors.name = null;
      nameError.textContent = "";
      updateSubmitButtonStatus();

      return true;
    }
  };

  const validateCountry = () => {
    const value = country.value.trim();

    if (value === "") {
      errors.country = "Please enter a country.";
      countryError.textContent = errors.country;
      updateSubmitButtonStatus();

      return false;
    } else if (value.length < 1 || value.length >= 20) {
      errors.country = "Country must be between 1 and 20 symbols.";
      countryError.textContent = errors.country;
      updateSubmitButtonStatus();

      return false;
    } else {
      errors.country = null;
      countryError.textContent = "";
      updateSubmitButtonStatus();

      return true;
    }
  };

  const validateCity = () => {
    const value = city.value.trim();

    if (value === "") {
      errors.city = "Please enter a city.";
      cityError.textContent = errors.city;
      updateSubmitButtonStatus();

      return false;
    } else if (value.length < 1 || value.length >= 20) {
      errors.city = "City must be between 1 and 20 symbols.";
      cityError.textContent = errors.city;
      updateSubmitButtonStatus();

      return false;
    } else {
      errors.city = null;
      cityError.textContent = "";
      updateSubmitButtonStatus();

      return true;
    }
  };

  const validateCode = () => {
    const value = code.value.trim();

    if (value === "") {
      errors.code = "Please enter a code.";
      codeError.textContent = errors.code;
      updateSubmitButtonStatus();

      return false;
    } else if (value.length != 3) {
      errors.code = "Code must be exactly 3 letters.";
      codeError.textContent = errors.code;
      updateSubmitButtonStatus();

      return false;
    } else {
      errors.code = null;
      codeError.textContent = "";
      updateSubmitButtonStatus();

      return true;
    }
  };

  const validateFounded = () => {
    const value = founded.value.trim();

    if (value === "") {
      errors.founded = "Please enter the date of foundation.";
      foundedError.textContent = errors.founded;
      updateSubmitButtonStatus();

      return false;
    } else {
      errors.founded = null;
      foundedError.textContent = "";
      updateSubmitButtonStatus();

      return true;
    }
  };

  const validateRunways = () => {
    const value = runways.value.trim();

    if (value === "") {
      errors.runways = "Please enter the runways.";
      runwaysError.textContent = errors.runways;
      updateSubmitButtonStatus();

      return false;
    } else if (value < 1 || isNaN(value)) {
      errors.runways = "Runways count must be a positive number.";
      runwaysError.textContent = errors.runways;
      updateSubmitButtonStatus();

      return false;
    } else {
      errors.runways = null;
      runwaysError.textContent = "";
      updateSubmitButtonStatus();

      return true;
    }
  };

  // Add event listeners
  name.addEventListener("blur", validateName);
  country.addEventListener("blur", validateCountry);
  city.addEventListener("blur", validateCity);
  code.addEventListener("blur", validateCode);
  runways.addEventListener("blur", validateRunways);
  founded.addEventListener("blur", validateFounded);
});
