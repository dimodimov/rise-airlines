﻿document.addEventListener("DOMContentLoaded", () => {
  // Form Fields - id naming a bit different because ASP attributes are used directly
  const flightNumber = document.getElementById("FlightNumber");
  const fromAirport = document.getElementById("FromAirport");
  const toAirport = document.getElementById("ToAirport");
  const departureDateTime = document.getElementById("DepartureDateTime");
  const arrivalDateTime = document.getElementById("ArrivalDateTime");

  // Error Message Fields
  const flightNumberError = document.getElementById("flight-number-error");
  const fromAirportError = document.getElementById("from-airport-error");
  const toAirportError = document.getElementById("to-airport-error");
  const departureDateTimeError = document.getElementById("departure-datetime-error");
  const arrivalDateTimeError = document.getElementById("arrival-datetime-error");

  // Button Disable/Enable Logic
  const errors = {};
  document.getElementById("submit-form-button").disabled = false;
  const enableSubmitButton = () =>
    (document.getElementById("submit-form-button").disabled = false);
  const disableSubmitButton = () =>
    (document.getElementById("submit-form-button").disabled = true);
  const updateSubmitButtonStatus = () => {   
    if (Object.values(errors).some((error) => error != null)) {
      disableSubmitButton();
    } else {
      enableSubmitButton();
    }
  };

  // Define validation functions
  const validateFlightNumber = () => {
    const value = flightNumber.value.trim();

    if (value === "") {
      errors.flightNumber = "Please enter a flight number.";
      flightNumberError.textContent = errors.flightNumber;
      updateSubmitButtonStatus();

      return false;
    } else if (value.length < 1 || value.length > 5) {
      errors.flightNumber = "Flight number must be between 1 and 6 symbols.";
      flightNumberError.textContent = errors.flightNumber;
      updateSubmitButtonStatus();

      return false;
    } else {
      errors.flightNumber = null;
      flightNumberError.textContent = "";
      updateSubmitButtonStatus();

      return true;
    }
  };

  const validateFromAirport = () => {
    const value = fromAirport.value.trim();

    if (value === "") {
      errors.fromAirport = "Please enter a your 'from' airport.";
      fromAirportError.textContent = errors.fromAirport;
      updateSubmitButtonStatus();

      return false;
    } else if (value.length != 3) {
      errors.fromAirport = "'From' airport code must be exactly 3 letters long.";
      fromAirportError.textContent = errors.fromAirport;
      updateSubmitButtonStatus();

      return false;
    } else {
      errors.fromAirport = null;
      fromAirportError.textContent = "";
      updateSubmitButtonStatus();

      return true;
    }
  };

  const validateToAirport = () => {
    const value = toAirport.value.trim();

    if (value === "") {
      errors.toAirport = "Please enter a your 'to' airport.";
      toAirportError.textContent = errors.toAirport;
      updateSubmitButtonStatus();

      return false;
    } else if (value.length != 3) {
      errors.toAirport = "'To' airport code must be exactly 3 letters long.";
      toAirportError.textContent = errors.toAirport;
      updateSubmitButtonStatus();

      return false;
    } else {
      errors.toAirport = null;
      toAirportError.textContent = "";
      updateSubmitButtonStatus();

      return true;
    }
  };

  const validateDepartureDateTime = () => {
    const value = new Date(departureDateTime.value);

    if (departureDateTime.value === "") {
      errors.departureDateTime = "Departure date and time can't be empty.";
      departureDateTimeError.textContent = errors.departureDateTime;
      updateSubmitButtonStatus();

      return false;
    } else if (value < Date.now()) {
      errors.departureDateTime =
        "Departure date and time can't be in the past.";
      departureDateTimeError.textContent = errors.departureDateTime;
      updateSubmitButtonStatus();

      return false;
    } else {
      errors.departureDateTime = null;
      departureDateTimeError.textContent = "";
      updateSubmitButtonStatus();

      return true;
    }
  };

  const validateArrivalDateTime = () => {
    const value = new Date(arrivalDateTime.value);

    if (arrivalDateTime.value === "") {
      errors.arrivalDateTime = "Arrival date and time can't be empty.";
      arrivalDateTimeError.textContent = errors.arrivalDateTime;
      updateSubmitButtonStatus();

      return false;
    } else if (value < Date.now()) {
      errors.arrivalDateTime = "Arrival date and time can't be in the past.";
      arrivalDateTimeError.textContent = errors.arrivalDateTime;
      updateSubmitButtonStatus();

      return false;
    } else if (departureDateTime.value &&
        new Date(departureDateTime.value) > value
    ) {
      errors.arrivalDateTime = "Arrival date and time can't be before the departure date.";
      arrivalDateTimeError.textContent = errors.arrivalDateTime;
      updateSubmitButtonStatus();

      return false;
    } else {
      errors.arrivalDateTime = null;
      arrivalDateTimeError.textContent = "";
      updateSubmitButtonStatus();

      return true;
    }
  };

  // Add event listeners
  flightNumber.addEventListener("blur", validateFlightNumber);
  fromAirport.addEventListener("blur", validateFromAirport);
  toAirport.addEventListener("blur", validateToAirport);
  departureDateTime.addEventListener("blur", validateDepartureDateTime);
  arrivalDateTime.addEventListener("blur", validateArrivalDateTime);
});
