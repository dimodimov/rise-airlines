﻿using Airlines.Business.DTOs;
using Airlines.Business.Services.FlightService;
using AirlinesWeb.Models;
using AirlinesWeb.Models.Flights;
using Microsoft.AspNetCore.Mvc;

namespace AirlinesWeb.Controllers;
public class FlightsController(IFlightService flightService) : Controller
{
    private const int ITEMS_PER_PAGE = 15;

    private readonly IFlightService _flightService = flightService;

    public async Task<IActionResult> Index(string searchString, int departureFilter, int pageNumber = 1)
    {
        try
        {
            var flightViewModel = CreateFlightsViewModel();
            var flights = new List<FlightDTO>();
            var itemsToSkip = (pageNumber - 1) * ITEMS_PER_PAGE;

            if (string.IsNullOrEmpty(searchString) && departureFilter == 0)
            {
                flights = await _flightService.GetFlightsByPageAsync(itemsToSkip, ITEMS_PER_PAGE);
                flightViewModel.Pagination.TotalItems = await _flightService.GetFlightCountAsync();
            }

            if (!string.IsNullOrEmpty(searchString) && departureFilter == 0)
            {
                flights = await SearchFlights(searchString, flightViewModel, itemsToSkip);
            }

            if (departureFilter != 0 && string.IsNullOrEmpty(searchString))
            {
                flights = await FilterFlights(departureFilter, flightViewModel, itemsToSkip);
            }

            if (departureFilter != 0 && !string.IsNullOrEmpty(searchString))
            {
                flights = await SearchAndFilterFlights(searchString, departureFilter, flightViewModel, itemsToSkip);
            }

            flightViewModel.Flights = flights;

            return View(flightViewModel);
        }
        catch (Exception ex)
        {
            await Console.Out.WriteLineAsync(ex.Message);
            return View();
        }
    }

    private async Task<List<FlightDTO>> SearchFlights(string searchString, FlightsViewModel flightViewModel, int itemsToSkip)
    {
        flightViewModel.SearchString = searchString;
        var (itemCount, foundFlights) = await _flightService.SearchFlightsAsync(searchString, itemsToSkip, ITEMS_PER_PAGE);
        flightViewModel.Pagination.TotalItems = itemCount;

        return foundFlights;
    }

    private async Task<List<FlightDTO>> FilterFlights(int departureFilter, FlightsViewModel flightViewModel, int itemsToSkip)
    {
        flightViewModel.DepartureFilter = departureFilter;

        var today = DateTime.Today;
        var endDate = today.AddDays(departureFilter - 1);
        var (itemCount, filteredFlights) = await _flightService.GetFlightsByDateRangeAsync(today, endDate, itemsToSkip, ITEMS_PER_PAGE);
        flightViewModel.Pagination.TotalItems = itemCount;

        return filteredFlights;
    }

    private async Task<List<FlightDTO>> SearchAndFilterFlights(string searchString, int departureFilter, FlightsViewModel flightViewModel, int itemsToSkip)
    {
        flightViewModel.SearchString = searchString;
        flightViewModel.DepartureFilter = departureFilter;

        var today = DateTime.Today;
        var endDate = today.AddDays(departureFilter - 1);
        var (itemCount, filteredFlights) = await _flightService.SearchAndFilterFlightsAsync(searchString, today, endDate, itemsToSkip, ITEMS_PER_PAGE);
        flightViewModel.Pagination.TotalItems = itemCount;

        return filteredFlights;
    }

    [HttpPost]
    public async Task<IActionResult> AddFlight(FlightDTO model)
    {
        try
        {
            if (ModelState.IsValid)
            {
                _ = await _flightService.AddFlightAsync(model);

                return RedirectToAction(nameof(Index));
            }

            var viewModel = CreateFlightsViewModel();
            viewModel.Flight = model;
            viewModel.Pagination.TotalItems = await _flightService.GetFlightCountAsync();
            viewModel.Flights = await _flightService.GetFlightsByPageAsync(0, ITEMS_PER_PAGE);

            return View("Index", viewModel);

        }
        catch (Exception ex)
        {
            var viewModel = CreateFlightsViewModel();
            viewModel.Flight = model;
            viewModel.Pagination.TotalItems = await _flightService.GetFlightCountAsync();
            viewModel.Flights = await _flightService.GetFlightsByPageAsync(0, ITEMS_PER_PAGE);

            await Console.Out.WriteLineAsync(ex.Message);
            return View("Index", viewModel);
        }
    }

    private FlightsViewModel CreateFlightsViewModel()
    {
        var paginationViewModel = new PaginationViewModel
        {
            ItemsPerPage = ITEMS_PER_PAGE
        };
        var flightsViewModel = new FlightsViewModel
        {
            Pagination = paginationViewModel
        };

        return flightsViewModel;
    }
}
