﻿using Airlines.Business.DTOs;
using Airlines.Business.Services.AirportService;
using AirlinesWeb.Models;
using Microsoft.AspNetCore.Mvc;
using AirlinesWeb.Models.Airports;

namespace AirlinesWeb.Controllers;
public class AirportsController(IAirportService airportService) : Controller
{
    private const int ITEMS_PER_PAGE = 15;

    private readonly IAirportService _airportService = airportService;

    public async Task<IActionResult> Index(string searchString, int pageNumber = 1)
    {
        try
        {
            var airportViewModel = CreateAirportViewModel();
            var airports = new List<AirportDTO>();
            var itemsToSkip = (pageNumber - 1) * ITEMS_PER_PAGE;

            if (string.IsNullOrEmpty(searchString))
            {
                airports = await _airportService.GetAirportsByPageAsync(itemsToSkip, ITEMS_PER_PAGE);
                airportViewModel.Pagination.TotalItems = await _airportService.GetAirportsCountAsync();
            }

            if (!string.IsNullOrEmpty(searchString))
            {
                airports = await SearchAirports(searchString, airportViewModel, itemsToSkip);
            }

            airportViewModel.Airports = airports;

            return View(airportViewModel);
        }
        catch (Exception ex)
        {
            await Console.Out.WriteLineAsync(ex.Message);
            return View();
        }
    }

    private async Task<List<AirportDTO>> SearchAirports(string searchString, AirportsViewModel airportViewModel, int itemsToSkip)
    {
        airportViewModel.SearchString = searchString;
        var (itemCount, foundAirports) = await _airportService.SearchAirportsByPageAsync(searchString, itemsToSkip, ITEMS_PER_PAGE);
        airportViewModel.Pagination.TotalItems = itemCount;

        return foundAirports;
    }

    [HttpPost]
    public async Task<IActionResult> AddAirport(AirportDTO model)
    {
        try
        {
            if (ModelState.IsValid)
            {
                _ = await _airportService.AddAirportAsync(model);
                return RedirectToAction(nameof(Index));
            }

            var viewModel = CreateAirportViewModel();
            viewModel.Airport = model;
            viewModel.Airports = await _airportService.GetAirportsByPageAsync(0, ITEMS_PER_PAGE);
            viewModel.Pagination.TotalItems = await _airportService.GetAirportsCountAsync();

            return View("Index", viewModel);
        }
        catch (Exception ex)
        {
            var viewModel = CreateAirportViewModel();
            viewModel.Airport = model;
            viewModel.Airports = await _airportService.GetAirportsByPageAsync(0, ITEMS_PER_PAGE);
            viewModel.Pagination.TotalItems = await _airportService.GetAirportsCountAsync();

            await Console.Out.WriteLineAsync(ex.Message);
            return View("Index", viewModel);
        }
    }

    private AirportsViewModel CreateAirportViewModel()
    {
        var paginationViewModel = new PaginationViewModel
        {
            ItemsPerPage = ITEMS_PER_PAGE
        };
        var airportViewModel = new AirportsViewModel
        {
            Pagination = paginationViewModel
        };

        return airportViewModel;
    }
}
