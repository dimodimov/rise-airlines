﻿using Microsoft.AspNetCore.Mvc;
using Airlines.Business.DTOs;
using Airlines.Business.Services.AirlineService;
using AirlinesWeb.Models;
using AirlinesWeb.Models.Airlines;

namespace AirlinesWeb.Controllers;
public class AirlinesController(IAirlineService airlineService) : Controller
{
    private const int ITEMS_PER_PAGE = 15;

    private readonly IAirlineService _airlineService = airlineService;

    public async Task<IActionResult> Index(string searchString, int pageNumber = 1)
    {
        try
        {
            var airlineViewModel = CreateAirlineViewModel();
            var airlines = new List<AirlineDTO>();
            var itemsToSkip = (pageNumber - 1) * ITEMS_PER_PAGE;

            if (string.IsNullOrEmpty(searchString))
            {
                airlines = await _airlineService.GetAirlinesByPageAsync(itemsToSkip, ITEMS_PER_PAGE);
                airlineViewModel.Pagination.TotalItems = await _airlineService.GetAirlinesCountAsync();
            }

            if (!string.IsNullOrEmpty(searchString))
            {
                airlines = await SearchAirlines(searchString, airlineViewModel, itemsToSkip);
            }

            airlineViewModel.Airlines = airlines;

            return View(airlineViewModel);
        }
        catch (Exception ex)
        {
            await Console.Out.WriteLineAsync(ex.Message);
            return View();
        }
    }

    private async Task<List<AirlineDTO>> SearchAirlines(string searchString, AirlineViewModel airlineViewModel, int itemsToSkip)
    {
        airlineViewModel.SearchString = searchString;
        var (itemCount, foundAirlines) = await _airlineService.SearchAirlinesByPageAsync(searchString, itemsToSkip, ITEMS_PER_PAGE);
        airlineViewModel.Pagination.TotalItems = itemCount;

        return foundAirlines;
    }

    [HttpPost]
    public async Task<IActionResult> AddAirline(AirlineDTO model)
    {
        try
        {
            if (ModelState.IsValid)
            {
                _ = await _airlineService.AddAirlineAsync(model);
                return RedirectToAction(nameof(Index));
            }

            var viewModel = CreateAirlineViewModel();
            viewModel.Airline = model;
            viewModel.Airlines = await _airlineService.GetAirlinesByPageAsync(0, ITEMS_PER_PAGE);
            viewModel.Pagination.TotalItems = await _airlineService.GetAirlinesCountAsync();

            return View("Index", viewModel);
        }
        catch (Exception ex)
        {
            var viewModel = CreateAirlineViewModel();
            viewModel.Airline = model;
            viewModel.Airlines = await _airlineService.GetAirlinesByPageAsync(0, ITEMS_PER_PAGE);
            viewModel.Pagination.TotalItems = await _airlineService.GetAirlinesCountAsync();

            await Console.Out.WriteLineAsync(ex.Message);
            return View("Index", viewModel);
        }
    }

    private AirlineViewModel CreateAirlineViewModel()
    {
        var paginationViewModel = new PaginationViewModel
        {
            ItemsPerPage = ITEMS_PER_PAGE
        };
        var airlineViewModel = new AirlineViewModel
        {
            Pagination = paginationViewModel
        };

        return airlineViewModel;
    }
}
