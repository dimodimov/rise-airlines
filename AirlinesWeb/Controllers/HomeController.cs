using Airlines.Business.Services.AirlineService;
using Airlines.Business.Services.AirportService;
using Airlines.Business.Services.FlightService;
using AirlinesWeb.Models;
using AirlinesWeb.Models.Home;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace AirlinesWeb.Controllers;
public class HomeController(ILogger<HomeController> logger, IAirportService airportService, IAirlineService airlineService, IFlightService flightService) : Controller
{
    private readonly ILogger<HomeController> _logger = logger;
    private readonly IAirportService _airportService = airportService;
    private readonly IAirlineService _airlineService = airlineService;
    private readonly IFlightService _flightService = flightService;

    public async Task<IActionResult> Index()
    {
        try
        {
            var homeViewModel = new HomeViewModel
            {
                AirlinesCount = await _airlineService.GetAirlinesCountAsync(),
                AirportsCount = await _airportService.GetAirportsCountAsync(),
                FlightsCount = await _flightService.GetFlightCountAsync()
            };

            return View(homeViewModel);
        }
        catch (Exception ex)
        {
            await Console.Out.WriteLineAsync(ex.Message);
            return View();
        }
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
