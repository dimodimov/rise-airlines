﻿using Airlines.Business.DTOs;

namespace AirlinesWeb.Models.Airports;

public class AirportsViewModel
{
    public AirportDTO? Airport { get; set; }
    public IEnumerable<AirportDTO>? Airports { get; set; }
    public string? SearchString { get; set; }
    public PaginationViewModel Pagination { get; set; }
}
