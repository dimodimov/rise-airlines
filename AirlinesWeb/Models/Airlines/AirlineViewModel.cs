﻿using Airlines.Business.DTOs;

namespace AirlinesWeb.Models.Airlines;

public class AirlineViewModel
{
    public AirlineDTO? Airline { get; set; }
    public IEnumerable<AirlineDTO>? Airlines { get; set; }
    public string? SearchString { get; set; }
    public PaginationViewModel Pagination { get; set; }
}
