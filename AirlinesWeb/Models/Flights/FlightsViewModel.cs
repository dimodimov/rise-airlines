﻿using Airlines.Business.DTOs;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AirlinesWeb.Models.Flights;

public class FlightsViewModel
{
    public FlightDTO Flight { get; set; }
    public IEnumerable<FlightDTO>? Flights { get; set; }

    public string? SearchString { get; set; }

    public int? DepartureFilter { get; set; }

    public List<SelectListItem> DepartureFilters { get; } = new List<SelectListItem>
    {
        new SelectListItem {Value = "1", Text="Today" },
        new SelectListItem {Value = "3", Text="Next 3 Days" },
        new SelectListItem {Value = "7", Text="Next 7 Days" },
        new SelectListItem {Value = "30", Text="Next 30 Days" }
    };
    public PaginationViewModel Pagination { get; set; }
}