﻿namespace AirlinesWeb.Models.Home;

public class HomeViewModel
{
    public int AirlinesCount { get; set; }
    public int AirportsCount { get; set; }
    public int FlightsCount { get; set; }
}
