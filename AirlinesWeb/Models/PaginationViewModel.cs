﻿namespace AirlinesWeb.Models;

public class PaginationViewModel
{
    public int CurrentPage { get; set; } = 1;
    public int ItemsPerPage { get; set; }
    public int TotalItems { get; set; }
    public int TotalPages => (int)Math.Ceiling((double)TotalItems / ItemsPerPage);
}
