﻿using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Persistence.Basic.Repositories;

public class AircraftRepository : IRepositoryBasic<IAircraft>
{
    private readonly List<ICargoAircraft> _cargoAircraftList;
    private readonly List<IPassengerAircraft> _passengerAircraftList;
    private readonly List<IPrivateAircraft> _privateAircraftList;

    public AircraftRepository()
    {
        _cargoAircraftList = [];
        _passengerAircraftList = [];
        _privateAircraftList = [];
    }

    public ICargoAircraft? GetCargoAircraftByModel(string model)
        => _cargoAircraftList.FirstOrDefault(aircraft => aircraft.Model == model);

    public IPassengerAircraft? GetPassengerAircraftByModel(string model)
         => _passengerAircraftList.FirstOrDefault(aircraft => aircraft.Model == model);

    public IPrivateAircraft? GetPrivateAircraftByModel(string model)
        => _privateAircraftList.FirstOrDefault(aircraft => aircraft.Model == model);

    public void Add(IAircraft item)
    {
        if (item is IPassengerAircraft passengerAircraft)
        {
            _passengerAircraftList.Add(passengerAircraft);
        }

        if (item is ICargoAircraft cargoAircraft)
        {
            _cargoAircraftList.Add(cargoAircraft);
        }

        if (item is IPrivateAircraft privateAircraft)
        {
            _privateAircraftList.Add(privateAircraft);
        }
    }

    public static List<string[]> GetData(string filePath)
        => FileReader.StreamReader(filePath);
}
