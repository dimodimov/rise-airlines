﻿using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Persistence.Basic.Repositories;
public class FlightRepository : IRepository<IFlight>
{
    private readonly List<IFlight> _flights = [];

    public IReadOnlyList<IFlight> FlightList => _flights;

    public bool Exists(IFlight flight) => _flights.Any(el => el.Identifier == flight.Identifier);

    public IFlight? GetById(string identifier)
        => _flights.FirstOrDefault(f => f.Identifier == identifier);

    public void Add(IFlight item) => _flights.Add(item);

    public IReadOnlyList<IFlight> Get() => _flights;

    public static List<string[]> GetData(string filePath)
        => FileReader.StreamReader(filePath);

    public void LoadData(IEnumerable<IFlight> data)
    {
        foreach (var flight in data)
        {
            Add(flight);
        }
    }
}
