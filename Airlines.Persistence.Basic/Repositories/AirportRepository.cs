﻿using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Persistence.Basic.Repositories;
public class AirportRepository : IRepository<IAirport>
{
    private readonly List<IAirport> _airportList = [];
    private readonly Dictionary<string, List<IAirport>> _airportByCityDictionary = [];
    private readonly Dictionary<string, List<IAirport>> _airportByCountryDictionary = [];

    public bool Exists(IAirport airport) => _airportList.Any(el => el.Identifier == airport.Identifier);

    public List<IAirport>? GetAirportsByCountry(string name)
        => _airportByCountryDictionary.TryGetValue(name.ToLower(), out var value) ? value : null;

    public List<IAirport>? GetAirportsByCity(string name)
        => _airportByCityDictionary.TryGetValue(name.ToLower(), out var value) ? value : null;


    public void Add(IAirport item)
    {
        _airportList.Add(item);

        if (!_airportByCityDictionary.TryGetValue(item.City.ToLower(), out var cityValue))
        {
            cityValue = [];
            _airportByCityDictionary[item.City.ToLower()] = cityValue;
        }

        cityValue.Add(item);

        if (!_airportByCountryDictionary.TryGetValue(item.Country.ToLower(), out var countryValue))
        {
            countryValue = [];
            _airportByCountryDictionary[item.Country.ToLower()] = countryValue;
        }

        countryValue.Add(item);
    }

    public IReadOnlyList<IAirport> Get() => _airportList;

    public static List<string[]> GetData(string filePath)
        => FileReader.StreamReader(filePath);

    public void LoadData(IEnumerable<IAirport> data)
    {
        foreach (var airport in data)
        {
            Add(airport);
        }
    }
}
