﻿using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Persistence.Basic.Repositories;
public class ReservationRepository : IRepositoryBasic<ICargoReservation>, IRepositoryBasic<ITicketReservation>
{
    private readonly List<ICargoReservation> _cargoReservationList = [];
    private readonly List<ITicketReservation> _ticketReservationList = [];

    public IReadOnlyList<ICargoReservation> GetCargoReservationList() => _cargoReservationList;
    public IReadOnlyList<ITicketReservation> GetTicketReservationList() => _ticketReservationList;

    public void Add(ICargoReservation item) => _cargoReservationList.Add(item);

    public void Add(ITicketReservation item) => _ticketReservationList.Add(item);
}
