﻿namespace Airlines.Persistence.Basic.Repositories;

public interface IRepositoryBasic<T>
{
    void Add(T item);
}

public interface IRepository<T> : IRepositoryBasic<T>
{
    void LoadData(IEnumerable<T> data);
}
