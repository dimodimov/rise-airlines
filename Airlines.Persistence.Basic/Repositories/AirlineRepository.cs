﻿using Airlines.Persistence.Basic.EntityInterfaces;

namespace Airlines.Persistence.Basic.Repositories;
public class AirlineRepository : IRepository<IAirline>
{
    private readonly Dictionary<string, IAirline> _airlineDictionary;

    public AirlineRepository() => _airlineDictionary = [];

    public IReadOnlyDictionary<string, IAirline> AirlineDictionary => _airlineDictionary;

    public bool Exists(string airlineName) => AirlineDictionary.ContainsKey(airlineName);

    public void Add(IAirline item) => _airlineDictionary.Add(item.Name, item);

    public IReadOnlyDictionary<string, IAirline> Get() => _airlineDictionary;

    public static List<string[]> GetData(string filePath)
        => FileReader.StreamReader(filePath);

    public void LoadData(IEnumerable<IAirline> data)
    {
        foreach (var airline in data)
        {
            Add(airline);
        }
    }
}
