﻿namespace Airlines.Persistence.Basic.EntityInterfaces;
public interface IReservation
{
    public string FlightIdentifier { get; set; }
}

public interface ICargoReservation : IReservation
{
    public int CargoWeight { get; set; }
    public double CargoVolume { get; set; }
}

public interface ITicketReservation : IReservation
{
    public int Seats { get; set; }
    public int SmallBaggageCount { get; set; }
    public int LargeBaggageCount { get; set; }
}