﻿namespace Airlines.Persistence.Basic.EntityInterfaces;
public interface IFlight
{
    string Identifier { get; set; }
    string DepartureAirport { get; set; }
    string ArrivalAirport { get; set; }
    decimal Price { get; set; }
    double Duration { get; set; }
    string AircraftModel { get; set; }
}
