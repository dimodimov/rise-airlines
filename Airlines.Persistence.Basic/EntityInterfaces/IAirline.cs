﻿namespace Airlines.Persistence.Basic.EntityInterfaces;
public interface IAirline
{
    string Name { get; set; }
}
