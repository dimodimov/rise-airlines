﻿namespace Airlines.Persistence.Basic.EntityInterfaces;
public interface IAircraft
{
    public string Model { get; set; }
}

public interface ICargoAircraft : IAircraft
{
    public int CargoWeight { get; set; }
    public double CargoVolume { get; set; }
}

public interface IPassengerAircraft : ICargoAircraft
{
    public int Seats { get; set; }
}

public interface IPrivateAircraft : IAircraft
{
    public int Seats { get; set; }
}