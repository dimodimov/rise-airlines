﻿namespace Airlines.Persistence.Basic.EntityInterfaces;
public interface IAirport
{
    string Name { get; set; }
    string Identifier { get; set; }
    string City { get; set; }
    string Country { get; set; }
}
