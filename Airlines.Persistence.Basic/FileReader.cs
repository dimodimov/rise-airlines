﻿namespace Airlines.Persistence.Basic;
public static class FileReader
{
    public static List<string[]> StreamReader(string filePath)
    {
        using var streamReader = new StreamReader(filePath);

        string? line;
        var results = new List<string[]>();

        while ((line = streamReader?.ReadLine()) != null)
        {
            var lineParams = line.Split(',');
            results.Add(lineParams);
        }

        return results;
    }
}
